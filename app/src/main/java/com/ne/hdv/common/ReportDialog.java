package com.ne.hdv.common;


import android.graphics.Paint;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

import java.util.regex.Pattern;

public class ReportDialog extends BaseDialogFragment {

    public static final String TAG = ReportDialog.class.getSimpleName();

    private TextView messageText, learnMoreText;
    private Button negativeButton;
    private Button doNotShowButton;
    private LinearLayout sendButton;
    private String url;

    public static ReportDialog newInstance(String tag, String url) {
        ReportDialog d = new ReportDialog();

        d.url = url;
        d.createArguments(tag);

        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_report;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() == null)
            return;

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (getDialog() == null || getDialog().getWindow() == null)
                return;

            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.85f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initializeViews() {
        messageText = fv(R.id.text_report_url);
        messageText.setText(url);

        negativeButton = fv(R.id.button_negative);
        sendButton = fv(R.id.button_send);

        negativeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                dismiss();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (positiveListener != null)
                    positiveListener.onDialogPositive(ReportDialog.this, tag);

                dismiss();
            }
        });

        learnMoreText = fv(R.id.text_message);
        learnMoreText.setText(r.s(R.string.dlg_report_description) + "  " + r.s(R.string.dlg_trend_button_learn_more));

        Pattern pattern = Pattern.compile(r.s(R.string.dlg_trend_button_learn_more));
        Linkify.addLinks(learnMoreText, pattern, "learn-more-activity://trend?what=report");

//        messageText.setText(Html.fromHtml(r.s(R.string.dlg_first_noti_message)));
//        messageText.setText(r.s(R.string.dlg_first_noti_message));

        doNotShowButton = fv(R.id.button_do_not_show);
        doNotShowButton.setPaintFlags(doNotShowButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        doNotShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setUseReport(false);
                dismiss();
            }
        });
    }
}
