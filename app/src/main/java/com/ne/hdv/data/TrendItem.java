package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import com.ne.hdv.common.Common;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.Util;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TrendItem extends TableObject {
    public static final String TABLE_NAME = "trend";

    public static final String ROW_ID = "id";

    public static final String TREND_URL = "trend_url";
    public static final String TREND_SITE = "trend_site";
    public static final String TREND_TITLE = "trend_title";
    public static final String TREND_THUMBNAIL = "trend_thumbnail";
    public static final String TREND_THUMBNAIL_URL = "trend_thumbnail_url";
    public static final String TREND_NUMBER = "trend_number";
    public static final String TREND_DOWN_COUNT = "trend_down_count";
    public static final String TREND_FILE_SIZE = "trend_file_size";
    public static final String TREND_FILE_TIME = "trend_file_time";
    public static final String TREND_LANGUAGE = "trend_language";
    public static final String TREND_REGION = "trend_region";
    public static final String TREND_DOWNLOAD_STATUS = "trend_download_status";
    public static final String TREND_REPORTED = "trend_reported";
    public static final String TREND_CONFIRM_STATUS = "trend_confirm_status";

    public static final int TREND_STATE_NONE = 1;
    public static final int TREND_STATE_WAITING = 2;
    public static final int TREND_STATE_CONFIRMED = 3;
    public static final int TREND_STATE_DELETED = 9;
    public static final Creator<TrendItem> CREATOR = new Creator<TrendItem>() {
        @Override
        public TrendItem createFromParcel(Parcel source) {
            return new TrendItem(source);
        }

        @Override
        public TrendItem[] newArray(int size) {
            return new TrendItem[size];
        }
    };
    private long id;
    private String url;
    private String site;
    private String title;
    private byte[] thumbnail;
    private String thumbnailUrl;
    private int number;
    private int count;
    private long fileTime;
    private long fileSize;
    private String language;
    private String region;
    private boolean status = false;
    private boolean reported = false;
    private int confirmStatus = TREND_STATE_NONE;

    public TrendItem() {
        super();
    }


    public TrendItem(JSONObject o) {
        this.url = Util.optString(o, Common.TAG_URL);
//        if (!TextUtils.isEmpty(url)) {
//            Uri uri = Uri.parse(this.url);
//            if (uri != null)
//                this.site = uri.getHost();
//        }
        this.site = Util.optString(o, Common.TAG_PAGE_URL);
        this.number = Integer.valueOf(Util.optString(o, Common.TAG_NUMBER));
        this.count = Integer.valueOf(Util.optString(o, Common.TAG_COUNT));
        this.fileSize = Long.valueOf(Util.optString(o, Common.TAG_SIZE));
        this.fileTime = Long.valueOf(Util.optString(o, Common.TAG_TIME));
//        if (fileTime > 30000)
//            this.confirmStatus = TREND_STATE_CONFIRMED;
//        else
//            this.confirmStatus = TREND_STATE_NONE;
        this.thumbnailUrl = Util.optString(o, Common.TAG_THUMBNAIL_URL);
//        this.language = Util.optString(o, Common.TAG_LANGUAGE);
        this.region = Util.optString(o, Common.TAG_REGION);
        this.confirmStatus = Integer.valueOf(Util.optString(o, Common.TAG_STATUS));
    }

    public TrendItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.url = in.readString();
        this.site = in.readString();
        this.title = in.readString();
        this.fileTime = in.readLong();
        this.fileSize = in.readLong();
        in.readByteArray(this.thumbnail);
        this.thumbnailUrl = in.readString();
        this.status = in.readInt() == 1;
        this.reported = in.readInt() == 1;
        this.confirmStatus = in.readInt();
        this.number = in.readInt();
        this.count = in.readInt();
        this.language = in.readString();
        this.region = in.readString();
    }

    public static void createTableTrends(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(TREND_URL + " text not null, ")
                .append(TREND_SITE + " text, ")
                .append(TREND_TITLE + " text, ")
                .append(TREND_FILE_SIZE + " integer, ")
                .append(TREND_FILE_TIME + " integer, ")
                .append(TREND_THUMBNAIL + " BLOB, ")
                .append(TREND_THUMBNAIL_URL + " text, ")
                .append(TREND_DOWNLOAD_STATUS + " integer not null, ")
                .append(TREND_REPORTED + " integer not null, ")
                .append(TREND_CONFIRM_STATUS + " integer, ")
                .append(TREND_NUMBER + " integer, ")
                .append(TREND_LANGUAGE + " text, ")
                .append(TREND_REGION + " text, ")
                .append(TREND_DOWN_COUNT + " integer) ")
                .toString());

    }

    public static TrendItem getTrendItem(SQLiteDatabase db, String url) {
        List<TrendItem> list = getTrendItems(db, null, TREND_URL + " = ?", new String[]{url}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<TrendItem> getTrendItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<TrendItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                TrendItem item = new TrendItem()
                        .setRowId(c)
                        .setTrendUrl(c)
                        .setTrendSite(c)
                        .setTrendTitle(c)
                        .setTrendFileSize(c)
                        .setTrendFileTime(c)
                        .setTrendThumbnail(c)
                        .setTrendThumbnailUrl(c)
                        .setTrendDownloadStatus(c)
                        .setTrendReported(c)
                        .setTrendConfirmedStatus(c)
                        .setTrendNumber(c)
                        .setTrendLanguage(c)
                        .setTrendRegion(c)
                        .setTrendCount(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateTrendItem(SQLiteDatabase db, TrendItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putTrendUrl(v)
                    .putTrendSite(v)
                    .putTrendTitle(v)
                    .putTrendFileSize(v)
                    .putTrendFileTime(v)
                    .putTrendThumbnail(v)
                    .putTrendThumbnailUrl(v)
                    .putTrendDownloadStatus(v)
                    .putTrendReported(v)
                    .putTrendConfirmedStatus(v)
                    .putTrendNumber(v)
                    .putTrendLanguage(v)
                    .putTrendRegion(v)
                    .putTrendCount(v);

            int rowAffected = db.update(TABLE_NAME, v, ROW_ID + " =? ", new String[]{String.valueOf(item.getRowId())});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getTrendUrl());
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateTrendDownloadStatus(SQLiteDatabase db, TrendItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putTrendDownloadStatus(v);

            db.update(TABLE_NAME, v, TREND_URL + " = ?", new String[]{item.getTrendUrl()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateTrendReportedStatus(SQLiteDatabase db, TrendItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putTrendReported(v);

            db.update(TABLE_NAME, v, TREND_URL + " = ?", new String[]{item.getTrendUrl()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteTrendItem(SQLiteDatabase db, TrendItem item) {
        try {
            db.delete(TABLE_NAME, TREND_URL + " = ? ", new String[]{item.getTrendUrl()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllTrenItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(url);
        out.writeString(site);
        out.writeString(title);
        out.writeLong(fileSize);
        out.writeLong(fileTime);
        out.writeByteArray(thumbnail);
        out.writeString(thumbnailUrl);
        out.writeInt(status ? 1 : 0);
        out.writeInt(reported ? 1 : 0);
        out.writeInt(confirmStatus);
        out.writeInt(number);
        out.writeInt(count);
        out.writeString(region);
        out.writeString(language);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof TrendItem && url.equalsIgnoreCase(((TrendItem) obj).url);
    }

    public long getRowId() {
        return this.id;
    }

    public TrendItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public TrendItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public TrendItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getTrendUrl() {
        return this.url;
    }

    public TrendItem setTrendUrl(String url) {
        this.url = url;

        return this;
    }

    public TrendItem setTrendUrl(Cursor c) {
        this.url = s(c, TREND_URL);

        return this;
    }

    public TrendItem putTrendUrl(ContentValues v) {
        v.put(TREND_URL, url);

        return this;
    }

    public String getTrendSite() {
        return this.site;
    }

    public TrendItem setTrendSite(String site) {
        this.site = site;

        return this;
    }

    public TrendItem setTrendSite(Cursor c) {
        this.site = s(c, TREND_SITE);

        return this;
    }

    public TrendItem putTrendSite(ContentValues v) {
        v.put(TREND_SITE, site);

        return this;
    }

    public String getTrendTitle() {
        return this.title;
    }

    public TrendItem setTrendTitle(String title) {
        this.title = title;

        return this;
    }

    public TrendItem setTrendTitle(Cursor c) {
        this.title = s(c, TREND_TITLE);

        return this;
    }

    public TrendItem putTrendTitle(ContentValues v) {
        v.put(TREND_TITLE, title);

        return this;
    }

    public long getTrenFileSize() {
        return this.fileSize;
    }

    public TrendItem setTrendFileSize(long fileSize) {
        this.fileSize = fileSize;

        return this;
    }

    public TrendItem setTrendFileSize(Cursor c) {
        this.fileSize = l(c, TREND_FILE_SIZE);

        return this;
    }

    public TrendItem putTrendFileSize(ContentValues v) {
        v.put(TREND_FILE_SIZE, fileSize);

        return this;
    }

    public long getTrendFIleTime() {
        return this.fileTime;
    }

    public TrendItem setTrendFileTime(long fileTime) {
        this.fileTime = fileTime;

        return this;
    }

    public TrendItem setTrendFileTime(Cursor c) {
        this.fileTime = l(c, TREND_FILE_TIME);


        return this;
    }

    public TrendItem putTrendFileTime(ContentValues v) {
        v.put(TREND_FILE_TIME, fileTime);

        return this;
    }


    public byte[] getTrendThumbnail() {
        return this.thumbnail;
    }

    public TrendItem setTrendThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public TrendItem setTrendThumbnail(Cursor c) {
        this.thumbnail = blob(c, TREND_THUMBNAIL);

        return this;
    }

    public TrendItem putTrendThumbnail(ContentValues v) {
        v.put(TREND_THUMBNAIL, thumbnail);

        return this;
    }

    public String getTrendThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public TrendItem setTrendThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;

        return this;
    }

    public TrendItem setTrendThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, TREND_THUMBNAIL_URL);

        return this;
    }

    public TrendItem putTrendThumbnailUrl(ContentValues v) {
        v.put(TREND_THUMBNAIL_URL, thumbnailUrl);

        return this;
    }

    public boolean isTrendDownloaded() {
        return this.status;
    }

    public TrendItem setTrendDownloadStatus(boolean status) {
        this.status = status;

        return this;
    }

    public TrendItem setTrendDownloadStatus(Cursor c) {
        this.status = i(c, TREND_DOWNLOAD_STATUS) == 1;

        return this;
    }

    public TrendItem putTrendDownloadStatus(ContentValues v) {
        v.put(TREND_DOWNLOAD_STATUS, status ? 1 : 0);

        return this;
    }

    public boolean isTrendReported() {
        return this.reported;
    }

    public TrendItem setTrendReported(boolean reported) {
        this.reported = reported;

        return this;
    }

    public TrendItem setTrendReported(Cursor c) {
        this.reported = i(c, TREND_REPORTED) == 1;

        return this;
    }

    public TrendItem putTrendReported(ContentValues v) {
        v.put(TREND_REPORTED, reported ? 1 : 0);

        return this;
    }

    public int getTrendConfirmedStatus() {
        return this.confirmStatus;
    }

    public TrendItem setTrendConfirmedStatus(int status) {
        this.confirmStatus = status;

        return this;
    }

    public TrendItem setTrendConfirmedStatus(Cursor c) {
        this.confirmStatus = i(c, TREND_CONFIRM_STATUS);

        return this;
    }

    public TrendItem putTrendConfirmedStatus(ContentValues v) {
        v.put(TREND_CONFIRM_STATUS, confirmStatus);

        return this;
    }

    public int getTrendNumber() {
        return this.number;
    }

    public TrendItem setTrendNumber(int number) {
        this.number = number;

        return this;
    }

    public TrendItem setTrendNumber(Cursor c) {
        this.number = i(c, TREND_NUMBER);

        return this;
    }

    public TrendItem putTrendNumber(ContentValues v) {
        v.put(TREND_NUMBER, number);

        return this;
    }

    public int getTrendCount() {
        return this.count;
    }

    public TrendItem setTrendCount(int count) {
        this.count = count;

        return this;
    }

    public TrendItem setTrendCount(Cursor c) {
        this.count = i(c, TREND_DOWN_COUNT);

        return this;
    }

    public TrendItem putTrendCount(ContentValues v) {
        v.put(TREND_DOWN_COUNT, count);

        return this;
    }

    public String getTrendRegion() {
        return this.region;
    }

    public TrendItem setTrendRegion(String region) {
        this.region = region;

        return this;
    }

    public TrendItem setTrendRegion(Cursor c) {
        this.region = s(c, TREND_REGION);

        return this;
    }

    public TrendItem putTrendRegion(ContentValues v) {
        v.put(TREND_REGION, region);

        return this;
    }

    public String getTrendLanguage() {
        return this.language;
    }

    public TrendItem setTrendLanguage(String language) {
        this.language = language;

        return this;
    }

    public TrendItem setTrendLanguage(Cursor c) {
        this.language = s(c, TREND_LANGUAGE);

        return this;
    }

    public TrendItem putTrendLanguage(ContentValues v) {
        v.put(TREND_LANGUAGE, language);

        return this;
    }
}
