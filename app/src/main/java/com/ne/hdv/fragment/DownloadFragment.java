package com.ne.hdv.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.base.BaseViewPager;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.data.DownItem;
import com.ne.hdv.download.Api;

import java.util.ArrayList;
import java.util.List;

//import android.support.design.widget.FloatingActionButton;

public class DownloadFragment extends BaseFragment
        //implements
        //DownManager.DownListener,
//		TrendsFragment.DownloadReadyListener,
//        DownloadingFragment.DownloadingListener,
        //JNetworkMonitor.OnChangeNetworkStatusListener
{

    public static final String TAG = DownloadFragment.class.getSimpleName();

    ImageButton delButton, closeButton, allCheckButton, allUncheckButton;
    // ImageButton hdfmButton, menuButton, menuCloseButton, , delButton, listModeButton, gridModeButton;
//    ExpandLayout menuExpand;
//    View closeMenuView;
//    Button allDownButton, allCancelButton, allPauseButton, allClearButton, selDelButton, allDelButton;
    FloatingActionButton allDownButton, allCancelButton, allPauseButton, selDelButton, allDelButton, listModeButton, gridModeButton;
    FloatingActionMenu floatingActionMenu;
    TextView delCountText;

    Api api;

    ViewPagerAdapter adapter;
    //	TrendsFragment trendsFragment;
    DownloadingFragment ingFragment;
    DownloadCompleteFragment completeFragment;
    LinearLayout normalLayout, delLayout;
    private TabLayout tabLayout;
    private BaseViewPager viewPager;
    //  private DownloadFragmentListener listener;

    public DownloadFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        if (api == null)
//            api = new Api(app, this);
//        else
//            api.setTarget(this);
//
//        listModeButton = fv(R.id.sub_floating_menu_list_view);
//        listModeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listModeButton.setVisibility(View.GONE);
//                gridModeButton.setVisibility(View.VISIBLE);
//                floatingActionMenu.close(false);
////                if (completeFragment != null)
////                    completeFragment.setViewMode(false);
//            }
//        });
//        listModeButton.setVisibility(View.GONE);
//
//        gridModeButton = fv(R.id.sub_floating_menu_grid_view);
//        gridModeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gridModeButton.setVisibility(View.GONE);
//                listModeButton.setVisibility(View.VISIBLE);
//                floatingActionMenu.close(false);
////                if (completeFragment != null)
////                    completeFragment.setViewMode(true);
//            }
//        });
//        gridModeButton.setVisibility(View.GONE);
//
//        floatingActionMenu = fv(R.id.button_floating_menu);
//
//        allDownButton = fv(R.id.sub_floating_menu_download_all);
//        allDownButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                visibleMenu(false);
//
//                if (ingFragment != null) {
//                    ArrayList<DownItem> items = ingFragment.getDownloadingItems();
//                    if (items != null && items.size() > 0) {
//                        for (DownItem item : items) {
//                            item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
//                            db.updateDownloadState(item);
//                            downloadManager.addToRequestList(item.getDownloadId());
//                        }
//                        ingFragment.refreshList();
//                        if (listener != null)
//                            listener.onRefreshDownloadCount();
//                    }
//                }
//            }
//        });
//
//        allPauseButton = fv(R.id.sub_floating_menu_pause_all);
//        allPauseButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                visibleMenu(false);
//
//                downloadManager.cancelAll(null);
//                db.restateDownloadState();
//                if (ingFragment != null)
//                    ingFragment.refreshList();
//
//                if (listener != null)
//                    listener.onRefreshDownloadCount();
//            }
//        });
//
//        allCancelButton = fv(R.id.sub_floating_menu_cancel_all);
//        allCancelButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                visibleMenu(false);
//
//                downloadManager.cancelAll(null);
//                if (ingFragment != null) {
//                    ArrayList<DownItem> list = ingFragment.getDownloadingItems();
//                    if (list != null && list.size() > 0)
//                        db.deleteDownloadItems(list);
//                    ingFragment.refreshList();
//                }
//
//                if (listener != null)
//                    listener.onRefreshDownloadCount();
//            }
//        });

        selDelButton = fv(R.id.sub_floating_menu_sel_delete);
        selDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeleteMode(true);
            }
        });

        allDelButton = fv(R.id.sub_floating_menu_delete_all);
        allDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                if (fdf(Common.TAG_DIALOG_DELETE_ALL_ITEMS) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_DELETE_ALL_ITEMS);
                    dlg.setTitle(r.s(R.string.dlg_delete_title));
                    dlg.setMessage(r.s(R.string.dlg_msg_delete_all_downloaded_list));
                    dlg.setNegativeLabel(r.s(R.string.str_cancel));
                    dlg.setPositiveLabel(r.s(R.string.str_delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
//                            if (completeFragment != null) {
//                                completeFragment.deleteItems(true);
//                                completeFragment.refreshLIst();
//                            }
                        }
                    });
                    sdf(dlg);
                }

            }
        });

        closeButton = fv(R.id.btn_top_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delButton.getVisibility() == View.VISIBLE)
                    setDeleteMode(false);
            }
        });

        delCountText = fv(R.id.text_top_del_count);

        delButton = fv(R.id.btn_top_delete);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItems();
            }
        });

        allCheckButton = fv(R.id.btn_top_all_check);
        allCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allCheckButton.setVisibility(View.GONE);
                allUncheckButton.setVisibility(View.VISIBLE);

                if (completeFragment != null)
                    completeFragment.setDeleteCheckAll(true);
            }
        });

        allUncheckButton = fv(R.id.btn_top_all_uncheck);
        allUncheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allCheckButton.setVisibility(View.VISIBLE);
                allUncheckButton.setVisibility(View.GONE);

                if (completeFragment != null)
                    completeFragment.setDeleteCheckAll(false);
            }
        });

//        normalLayout = fv(R.id.layout_normal);
        delLayout = fv(R.id.layout_delete);

        viewPager = fv(R.id.viewpager);
        viewPager.setEnableSwipe(false);
        setupViewPager(viewPager);

        tabLayout = fv(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.setCustomView(null);
                setmenuVisible(adapter.getPageTitle(tab.getPosition()).toString());
                if (floatingActionMenu != null && floatingActionMenu.isOpened())
                    floatingActionMenu.close(false);
                setDeleteMode(false);

                View layout = LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab, null);
                TextView text = (TextView) layout.findViewById(R.id.text_title);
                text.setTextColor(r.c(getActivity(), R.color.primary1_a100));
                text.setText(r.s(tabLayout.getSelectedTabPosition() == 0 ? R.string.down_text_status : R.string.down_text_files));
                ImageView image = (ImageView) layout.findViewById(R.id.image_icon);
                image.setImageResource(tabLayout.getSelectedTabPosition() == 0 ? R.drawable.ic_downloading_color_on_24 : R.drawable.ic_files_color_on_24);
                tab.setCustomView(layout);

//                TextView text = new TextView(getActivity());
//                text.setTextSize(Util.convertDpToPixel(3.5f, getActivity()));
//                text.setLayoutParams(new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
//                text.setTextColor(r.c(getActivity(), R.color.primary1_a100));
//                text.setGravity(Gravity.CENTER);
//                switch (tabLayout.getSelectedTabPosition()) {
//                    case 0:
//                        text.setText(r.s(R.string.down_text_status));
//                        text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_downloading_color_on_24, 0, 0, 0);
//                        break;
//
//                    case 1:
//                        text.setText(r.s(R.string.down_text_files));
//                        text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_files_color_on_24, 0, 0, 0);
//                        break;
//                }
//                tab.setCustomView(text);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setCustomView(null);

                View layout = LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab, null);
                TextView text = (TextView) layout.findViewById(R.id.text_title);
                text.setText(r.s(tabLayout.getSelectedTabPosition() == 0 ? R.string.down_text_status : R.string.down_text_files));
                text.setTextColor(r.c(getActivity(), R.color.gray5_a100));
                ImageView image = (ImageView) layout.findViewById(R.id.image_icon);
                image.setImageResource(tabLayout.getSelectedTabPosition() == 0 ? R.drawable.ic_downloading_color_off_24 : R.drawable.ic_files_color_off_24);
                tab.setCustomView(layout);
//                TextView text = new TextView(getActivity());
//                text.setTextSize(Util.convertDpToPixel(3.5f, getActivity()));
//                text.setLayoutParams(new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
//                text.setTextColor(r.c(getActivity(), R.color.gray5_a100));
//                text.setGravity(Gravity.CENTER);
//                switch (tabLayout.getSelectedTabPosition()) {
//                    case 0:
//                        text.setText(r.s(R.string.down_text_status));
//                        text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_downloading_color_off_24, 0, 0, 0);
//                        break;
//
//                    case 1:
//                        text.setText(r.s(R.string.down_text_files));
//                        text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_files_color_off_24, 0, 0, 0);
//                        break;
//                }
//                tab.setCustomView(text);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        View statusLayout = LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab, null);
        TextView statusText = (TextView) statusLayout.findViewById(R.id.text_title);
        statusText.setText(r.s(R.string.down_text_status));
        ImageView statusImage = (ImageView) statusLayout.findViewById(R.id.image_icon);
        statusImage.setImageResource(R.drawable.ic_downloading_color_on_24);
        tabLayout.getTabAt(0).setCustomView(statusLayout);

//        View layout = LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab, null);
//        TextView text = (TextView) layout.findViewById(R.id.text_title);
//        text.setText(r.s(R.string.down_text_files));
//        ImageView image = (ImageView) layout.findViewById(R.id.image_icon);
//        image.setImageResource(R.drawable.ic_files_color_off_24);
//        tabLayout.getTabAt(1).setCustomView(layout);
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        setmenuVisible(adapter.getPageTitle(0).toString());
//        downloadManager.addListener(this);
//        networkMonitor.setListener(this);
    }

    @Override
    public void handleApiMessage(Message m) {
        super.handleApiMessage(m);
        if (m.what == Common.API_CODE_SEND_TREND) {
            if (!m.getData().getBoolean("update"))
                return;

            DownItem item = m.getData().getParcelable("download_item");
            if (item != null && ingFragment != null)
                ingFragment.updateProgress(item);
        }
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {


    }

//    @Override
//    public void onDownloadError(DownItem item, int errorCode) {
//        if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status))) {
//            ingFragment.updateProgress(item);
//        }
//
//        if (listener != null)
//            listener.onRefreshDownloadCount();
//    }
//
//    @Override
//    public void onDownloadProgress(DownItem item, long progress, long total) {
//        if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status))) {
//            ingFragment.updateProgress(item);
//            ingFragment.updateDownloadProgressUI();
//        }
//    }
//
//    @Override
//    public void onDownloadStart(DownItem item) {
//        if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status))) {
//            ingFragment.insertItem(item);
//            ingFragment.updateDownloadProgressUI();
//        }
//    }
//
//    @Override
//    public void onDownloadSuccess(DownItem item) {
//        if (item == null)
//            return;
//
//        if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status))) {
//            ingFragment.removeItem(item.getDownloadId());
//            ingFragment.updateDownloadProgressUI();
//        }
//
//        if (listener != null)
//            listener.onRefreshDownloadCount();
//
////        if (completeFragment != null && tabLayout.getSelectedTabPosition() == 2)
////            completeFragment.refreshList();
//    }

//	@Override
//	public void onTrendItemClicked(TrendItem _item) {
//		DownItem item = new DownItem();
//		String fileName = Common.PREFIX_DOWNLOAD_FILE + System.currentTimeMillis() + Common.checkVideoExtension(_item.getTrendUrl());
//		item.setDownloadId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
//		item.setDownloadUrl(_item.getTrendUrl());
//		item.setDownloadSite(_item.getTrendSite());
//		item.setDownloadFileName(fileName);
//		item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
//		item.setDownloadFileTotalSize(_item.getTrenFileSize());
//		item.setDownloadFileTime(_item.getTrendFIleTime());
//		item.setDownloadThumbnail(_item.getTrendThumbnail());
//		item.setDownloadFileRemainSize(0);
//		item.setDownloadFilePath(Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION);
//		item.setDownloadFileThumbnailPath(Common.getVideoDir(app.getApplicationContext()) + "." + FileUtil.getFileName(item.getDownloadFileName()) +  FileUtil.TEMP_THUMBNAIL_EXTENSION);
//
//		db.insertOrUpdateDownloadItem(item);
//		downloadManager.addToRequestList(item.getDownloadId());
//
//		if (listener != null)
//			listener.sendTrendUrl(item);
//	}

//    @Override
//    public void onShowAlertDialog(String message) {
//        showAlertMessageDialog(message);
//    }
//
//    @Override
//    public void onDownloadingItemClicked(DownItem item) {
//
//        if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DOWNLOADING || item.getDownloadState() == DownItem.DOWNLOAD_STATE_WAITING) {
//            downloadManager.cancelDownload(item.getDownloadId(), item);
//            item.setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
//        } else {
//            downloadManager.addToRequestList(item.getDownloadId());
//            item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
////            if (listener != null)
////                listener.onRefreshDownloadCount();
//        }
//        db.updateDownloadState(item);
//        if (listener != null)
//            listener.onRefreshDownloadCount();
//    }

//	@Override
//	public void joinedTrend() {
//		setupViewPager(viewPager);
//		viewPager.setCurrentItem(1);
//
//		if (trendsFragment != null) {
//			trendsFragment.joinedTrend();
//		}
//
//		if (ingFragment != null) {
//			ArrayList<DownItem> tempItems = ingFragment.getDownloadingItems();
//			for (DownItem item : tempItems) {
//				api.sendTrendUrl(item, false, false);
//			}
//			ingFragment.refreshList();
//		}
//
//
//
////		trendsFragment = new TrendsFragment();
////		trendsFragment.setListener(this);
////		adapter.addFrag(trendsFragment, r.s(R.string.down_text_trends), 1);
//	}
//
//    @Override
//    public void onDownloadedItemClicked(DownItem item) {
////        Intent intent = new Intent(getActivity(), ViewActivity.class);
////        intent.putExtra("id", item.getDownloadId());
////        startActivityForResult(intent, 0);
//        String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
//        MimeTypeMap myMime = MimeTypeMap.getSingleton();
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
//
//        Uri uri = null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            uri = FileProvider.getUriForFile(getActivity(), "com.ne.hdv.fileprovider", new File(path));
//        else
//            uri = Uri.fromFile(new File(path));
//        intent.setDataAndType(uri, "video/mp4");
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//
//        try {
//            startActivity(intent);
//        } catch (android.content.ActivityNotFoundException e) {
//            Toast.makeText(getActivity(), "Failed...", Toast.LENGTH_SHORT).show();
//        }
//
//    }

//	@Override
//	public void notiEmptyTrendList(boolean empty) {
//		if (!adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_trends)))
//			return;
//
//		menuButton.setVisibility(View.GONE);
//		hdfmButton.setVisibility(View.GONE);
//		gridModeButton.setVisibility(View.GONE);
//		listModeButton.setVisibility(View.GONE);
//	}
//
//    @Override
//    public void notiEmptyDownloadingList(boolean empty) {
//        if (!adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status)))
//            return;
//
//        floatingActionMenu.setVisibility(empty ? View.GONE : View.VISIBLE);
////        hdfmButton.setVisibility(View.GONE);
////        gridModeButton.setVisibility(View.GONE);
////        listModeButton.setVisibility(View.GONE);
//    }
////
//    @Override
//    public void notiEmptyCompleteList(boolean empty) {
//        if (!adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_files)))
//            return;
//
//        if (empty) {
//            setDeleteMode(false);
//        }
//        floatingActionMenu.setVisibility(empty ? View.GONE : View.VISIBLE);
////        listModeButton.setVisibility(!empty && !completeFragment.isListMode()? View.VISIBLE : View.GONE);
////        gridModeButton.setVisibility(!empty && completeFragment.isListMode()? View.VISIBLE : View.GONE);
////        listModeButton.setVisibility(!empty && !sp.isDownloadListMode() ? View.VISIBLE : View.GONE);
////        gridModeButton.setVisibility(!empty && sp.isDownloadListMode() ? View.VISIBLE : View.GONE);
////        floatingActionMenu.close(false);
////        hdfmButton.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void onDeleteMode(boolean on) {
//        setDeleteMode(on);
//    }
//
//    @Override
//    public void onCheckChanged(int count, boolean isSelectAll) {
//        if (count <= 0)
//            delCountText.setText("");
//        else
//            delCountText.setText(count + " " + r.s(R.string.del_selected_title));
//
//        allCheckButton.setVisibility(isSelectAll ? View.GONE : View.VISIBLE);
//        allUncheckButton.setVisibility(isSelectAll ? View.VISIBLE : View.GONE);
//    }

//    @Override
//    public void onNetworkChanged(int status) {
//        if (downloadManager == null || downloadManager.isEmpty())
//            return;
//
//        if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
//            showAlertMessageDialog(r.s(R.string.dlg_msg_check_network));
//            if (downloadManager != null)
//                downloadManager.cancelAll(null);
//            downloadManager.cancelAll(null);
//            db.restateDownloadState();
//
//            if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status)))
//                ingFragment.refreshList();
//        } else if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE && !sp.isUseCellularData()) {
//            showAlertMessageDialog(r.s(R.string.dlg_msg_check_wifi));
//            if (downloadManager != null)
//                downloadManager.cancelAll(null);
//            downloadManager.cancelAll(null);
//            db.restateDownloadState();
//
//            if (ingFragment != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status)))
//                ingFragment.refreshList();
//        }
//    }

//    @Override
//    public void onResume() {
////		if (tabLayout != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_trends)) && trendsFragment != null)
////			trendsFragment.onResume();
////		else
//        if (tabLayout != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_status)) && ingFragment != null)
//            ingFragment.onResume();
//        else if (tabLayout != null && adapter.getPageTitle(tabLayout.getSelectedTabPosition()).equals(r.s(R.string.down_text_files)) && completeFragment != null)
//            completeFragment.onResume();
//        super.onResume();
//
////		sp.setTotalProgressShowing(true);
////		Common.NOW_LOADING = false;
////		refreshList();
//    }

    private void setupViewPager(ViewPager viewPager) {

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        ingFragment = new DownloadingFragment();
//        ingFragment.setListener(this);
//		trendsFragment = new TrendsFragment();
//		trendsFragment.setListener(this);
//        completeFragment = new DownloadCompleteFragment();
//        completeFragment.setListener(this);

        adapter.addFrag(ingFragment, r.s(R.string.down_text_status));
//        adapter.addFrag(trendsFragment, r.s(R.string.down_text_ready));
//		if (sp.isJoinedTrends())
//			adapter.addFrag(trendsFragment, r.s(R.string.down_text_trends));
//        adapter.addFrag(completeFragment, r.s(R.string.down_text_files));
        viewPager.setAdapter(adapter);
    }

    private void deleteItems() {
        if (delCountText.getText().toString().isEmpty()) {
            if (fdf("no_select") == null) {
                MessageDialog dlg = MessageDialog.newInstance("no_select");
                dlg.setTitle(r.s(R.string.dlg_delete_title));
                dlg.setMessage(r.s(R.string.dlg_msg_no_selected_item));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }

            return;
        }

        if (fdf("delete_selected_items") == null) {
            MessageDialog dlg = MessageDialog.newInstance("delete_selected_items");
            dlg.setTitle(r.s(R.string.dlg_delete_title));
            dlg.setMessage((r.s(R.string.dlg_msg_delete_item)));
            dlg.setNegativeLabel(r.s(R.string.str_cancel));
            dlg.setPositiveLabel(r.s(R.string.str_delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    completeFragment.deleteItems(false);
                }
            });
            sdf(dlg);
        }
    }

    private void setmenuVisible(String tag) {
        if (allDownButton == null)
            return;

        boolean isTrends = tag.equalsIgnoreCase(r.s(R.string.down_text_trends));
        boolean isIng = tag.equalsIgnoreCase(r.s(R.string.down_text_status));
        boolean isComplete = tag.equalsIgnoreCase(r.s(R.string.down_text_files));

        allDownButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
//        allClearButton.setVisibility(View.GONE);
        allPauseButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
        allCancelButton.setVisibility(isIng ? View.VISIBLE : View.GONE);
        selDelButton.setVisibility(isComplete ? View.VISIBLE : View.GONE);
        allDelButton.setVisibility(isComplete ? View.VISIBLE : View.GONE);
//        hdfmButton.setVisibility(isComplete ? View.GONE : View.GONE);
//        listModeButton.setVisibility(isComplete && !completeFragment.isListMode() ? View.VISIBLE : View.GONE);
//        gridModeButton.setVisibility(isComplete && completeFragment.isListMode() ? View.VISIBLE : View.GONE);
//		if (isTrends) trendsFragment.refreshList();
        if (isIng) ingFragment.refreshList();
        if (isComplete) completeFragment.refreshLIst();
    }

    @SuppressLint("NewApi")
    private void setDeleteMode(boolean isMode) {
//        normalLayout.setVisibility(isMode ? View.GONE : View.VISIBLE);
        tabLayout.setVisibility(isMode ? View.GONE : View.VISIBLE);
        delLayout.setVisibility(isMode ? View.VISIBLE : View.GONE);
//        delLayout.setBackgroundColor(r.c(getActivity(), R.color.url_keyword));
        delCountText.setText("");
        allCheckButton.setVisibility(View.VISIBLE);
        allUncheckButton.setVisibility(View.GONE);
        delButton.setVisibility(View.VISIBLE);

        if (isMode) {
            floatingActionMenu.close(true);
//            menuExpand.collapse(true);
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
//                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                getActivity().getWindow().setStatusBarColor(r.c(getActivity(), R.color.url_keyword));
//            }
//            tabLayout.setBackgroundColor(r.c(getActivity(), R.color.url_keyword));
//            tabLayout.setTabTextColors(r.c(getActivity(), R.color.download_del_tab_text), r.c(getActivity(), R.color.white));

            if (completeFragment != null)
                completeFragment.selectModeClick(true);
        } else {
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
//                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//                getActivity().getWindow().setStatusBarColor(r.c(getActivity(), R.color.main));
//            }
//            tabLayout.setBackgroundColor(r.c(getActivity(), R.color.main));
//            tabLayout.setTabTextColors(r.c(getActivity(), R.color.url_edit_hint_text), r.c(getActivity(), R.color.url_keyword));

            if (completeFragment != null)
                completeFragment.selectModeClick(false);
        }
    }

    public void showDownloadTab(int tabIndex) {
        if (tabLayout == null)
            return;
        TabLayout.Tab tab = tabLayout.getTabAt(tabIndex);
        tab.select();
//		setmenuVisible(tab);
    }

//    public void setListener(DownloadFragmentListener listener) {
//        this.listener = listener;
//    }

    private void visibleMenu(boolean show) {
//        if (show)
//            menuExpand.expand(true);
//        else
//            menuExpand.collapse(true);
//        closeMenuView.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show) ;
        else
            floatingActionMenu.close(true);
    }

    public boolean setBackPress() {
        if (floatingActionMenu != null && floatingActionMenu.isOpened()) {
            visibleMenu(false);
            return true;
        }

        if (delLayout != null && delLayout.getVisibility() == View.VISIBLE) {
            setDeleteMode(false);
            return true;
        }
        return false;
    }

    public void launchHDF() {
        try {
            if (getPackageList()) {
                Intent intent = getActivity().getPackageManager()
                        .getLaunchIntentForPackage(Common.PACKAGE_TM_FILEMANAGER);
                intent.putExtra("path", Common.getVideoDir(getActivity()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(Common.PLAYSTORE_URL + Common.PACKAGE_TM_FILEMANAGER);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(Common.PLAYSTORE_URL + Common.PACKAGE_TM_FILEMANAGER);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    public boolean getPackageList() {
        boolean isExist = false;

        PackageManager pkgMgr = getActivity().getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith(Common.PACKAGE_TM_FILEMANAGER)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    public void startPremiumMode() {
        if (completeFragment != null)
            completeFragment.startPremiumMode();
    }

    public void showAlertMessageDialog(String msg) {
        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle(r.s(R.string.download));
            dlg.setPositiveLabel(r.s(R.string.ok));
            dlg.setMessage(msg);

            sdf(dlg);
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void addFrag(Fragment fragment, String title, int index) {
            mFragmentList.add(index, fragment);
            mFragmentTitleList.add(index, title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
