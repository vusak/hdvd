package com.ne.hdv.download;

import com.ne.hdv.common.LogUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class DDownloadLinkProvider {

    public static final String DEFAULT_MOBILE_USER_AGENT_STRING
            = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
    private String userAgent = DEFAULT_MOBILE_USER_AGENT_STRING;

    private String source;
    private String videoId;

    public DDownloadLinkProvider(String source) {
        this.source = source;
    }

    public String getVideoLink() {
        String url = source;
        StringBuilder sb = new StringBuilder(url);
        sb.insert(source.indexOf(".com/") + 5, "json/");
        sb.append("?fields=title,stream_h264_url,stream_h264_ld_url,stream_h264_hq_url,stream_h264_hd_url,stream_h264_hd1080_url");
        String jsonUrl = new String(sb);
        String videoList = "";
        try {
            videoList = getRemoteContent(jsonUrl);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
        if ((videoList == null) || (videoList.isEmpty())) {
            return null;
        }

        JSONObject info;
        String videoURL;
        try {
            info = new JSONObject(videoList);
            videoURL = info.getString("stream_h264_hq_url");
            if ((videoURL == null) || (videoURL.isEmpty()) || (videoURL.equals("null"))) {
                videoURL = info.getString("stream_h264_ld_url");
            }
            if ((videoURL == null) || (videoURL.isEmpty()) || (videoURL.equals("null"))) {
                videoURL = info.getString("stream_h264_url");
            }
            if ((videoURL == null) || (videoURL.isEmpty()) || (videoURL.equals("null"))) {
                videoURL = info.getString("stream_h264_hd1080_url");
            }
            if ((videoURL == null) || (videoURL.isEmpty()) || (videoURL.equals("null"))) {
                videoURL = info.getString("stream_h264_hd_url");
            }
            if ((videoURL == null) || (videoURL.isEmpty()) || (videoURL.equals("null"))) {
                videoURL = null;
            }
        } catch (Exception e) {
            return null;
        }

        return videoURL;
    }

    private String getRemoteContent(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        connection.setRequestProperty("User-Agent", userAgent);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Referrer", url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

//    private String getRemoteContent(String url) {
//        String content = null;
//
//        try {
//
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet(url);
//            HttpResponse httpResponse = httpClient.execute(httpGet);
//            content = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
//
//        } catch (Exception e) {
//            content = null;
//        }
//
//        return content;
//    }
}