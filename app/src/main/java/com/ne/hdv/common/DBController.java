package com.ne.hdv.common;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ne.hdv.data.BookmarkItem;
import com.ne.hdv.data.DownItem;
import com.ne.hdv.data.FavoriteItem;
import com.ne.hdv.data.ReportItem;
import com.ne.hdv.data.TrendItem;

import java.util.ArrayList;
import java.util.List;

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "hdvd.db";
    //    private static final int DB_VERSION = 3;
    private static final int DB_VERSION = 4;
    private static DBHelper helper;
    private static SQLiteDatabase db;
    private Context context;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    public synchronized void deleteAllData() {

    }

    public synchronized List<DownItem> getAllDownloadItems() {
        return DownItem.getDownloadItems(db, null, null, null, null, null, DownItem.ROW_ID + " DESC", null);
    }

    public synchronized List<DownItem> getAllRDownloadItems() {
        return DownItem.getDownloadItems(db, null, null, null, null, null, DownItem.DOWNLOAD_START_AT + " DESC", null);
    }

    public synchronized List<DownItem> getAllCDownloadItems() {
        return DownItem.getDownloadItems(db, null, null, null, null, null, DownItem.DOWNLOAD_AT + " DESC", null);
    }

    public synchronized List<DownItem> getDownloadItems(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return DownItem.getDownloadItems(db, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public synchronized DownItem getDownloadItem(String id) {
        return DownItem.getDownloadItem(db, id);
    }

    public synchronized boolean isDownloadedItem(String url) {
        return DownItem.isDownloadedItem(db, url);
    }

    public synchronized boolean isDownloadFileName(String name) {
        return DownItem.isDownloadFileName(db, name);
    }

    public synchronized boolean restateDownloadState() {
        return DownItem.restateDownloadState(db);
    }

    public synchronized boolean insertOrUpdateDownloadItem(DownItem item) {
        return DownItem.insertOrUpdateDownloadItem(db, item);
    }

    public synchronized boolean updateDownloadState(DownItem item) {
        return DownItem.updateDownloadState(db, item);
    }

    public synchronized boolean cancelAllItems() {
        return DownItem.cancelAllDownloadItems(db);
    }

    public synchronized boolean deleteDownloadItem(DownItem item) {
        return DownItem.deleteDownloadItem(db, item);
    }

    public synchronized boolean deleteDownloadItems(ArrayList<DownItem> list) {
        return DownItem.deleteDownloadedItems(db, list);
    }

    public synchronized boolean deleteAllNonDownloadItems() {
        return DownItem.deleteAllNonDownloadItems(db);
    }

    public synchronized boolean deleteAllDownloadedItems() {
        return DownItem.deleteAllDownloadedItems(db);
    }

    public synchronized int getReadyDownloadItemCount() {
        return DownItem.readyDownloadItemCount(db);
    }

    public synchronized int getDownloadingItemCount() {
        return DownItem.downloadingItemCount(db);
    }

    public synchronized boolean insertOrUpdateTrendItem(TrendItem item) {
        return TrendItem.insertOrUpdateTrendItem(db, item);
    }

    public synchronized List<TrendItem> getTrendItems() {
        return TrendItem.getTrendItems(db, null, null, null, null, null, TrendItem.TREND_NUMBER + " ASC", null);
    }

    public synchronized boolean updateTrendDownloadStatus(TrendItem item) {
        return TrendItem.updateTrendDownloadStatus(db, item);
    }

    public synchronized boolean updateTrendReportedStatus(TrendItem item) {
        return TrendItem.updateTrendReportedStatus(db, item);
    }

    public synchronized boolean deleteTrendItem(TrendItem item) {
        return TrendItem.deleteTrendItem(db, item);
    }

    public synchronized boolean deleteAllTrendItems() {
        return TrendItem.deleteAllTrenItems(db);
    }

    public synchronized boolean isReportedItem(String site) {
        return ReportItem.getReportItem(db, site) != null;
    }

    public synchronized boolean insertOrUpdateReportItem(ReportItem item) {
        return ReportItem.insertOrUpdateReportItem(db, item);
    }

    public synchronized List<ReportItem> getReportItems() {
        return ReportItem.getReportItems(db, null, null, null, null, null, ReportItem.ROW_ID + " ASC", null);
    }

    public synchronized boolean deleteReportItem(ReportItem item) {
        return ReportItem.deleteReportItem(db, item);
    }

    public synchronized boolean deleteAllReportItems() {
        return ReportItem.deleteAllReportItems(db);
    }

    public synchronized BookmarkItem getBookmarkItem(String url) {
        return BookmarkItem.getBookmarkItem(db, url);
    }

    public synchronized BookmarkItem getBookmarkItem(BookmarkItem item) {
        return BookmarkItem.getBookmarkItem(db, item);
    }

    public synchronized List<BookmarkItem> getBookmarkItems() {
        return BookmarkItem.getBookmarkItems(db, null, null, null, null, null, BookmarkItem.BOOKMARK_ORDER + " DESC", null);
    }

    public synchronized boolean insertOrUpdateBookmarkItem(BookmarkItem item) {
        return BookmarkItem.insertOrUpdateBookmarkItem(db, item);
    }

    public synchronized boolean updateBookmark(BookmarkItem item) {
        return BookmarkItem.updateBookmark(db, item);
    }

    public synchronized boolean updateBookmarkOrder(BookmarkItem item) {
        return BookmarkItem.updateBookmarkOrder(db, item);
    }

    public synchronized boolean updateBookmarkShowing(BookmarkItem item) {
        return BookmarkItem.updateBookmarkShowing(db, item);
    }

    public synchronized boolean deleteBookmarkItem(String url) {
        return BookmarkItem.deleteBookmarkItem(db, url);
    }

    public synchronized boolean deleteBookmarkItem(BookmarkItem item) {
        return BookmarkItem.deleteBookmarkItem(db, item);
    }

    public synchronized boolean deleteAllBookmarkItems() {
        return BookmarkItem.deleteAllBookmarkItems(db);
    }

    public synchronized FavoriteItem getFavoriteItem(String url) {
        return FavoriteItem.getFavoriteItem(db, url);
    }

    public synchronized List<FavoriteItem> getFavoriteItems() {
        return FavoriteItem.getFavoriteItems(db, null, null, null, null, null, FavoriteItem.FAVORITE_COUNT + " DESC", null);
    }

    public synchronized boolean insertOrUpdateFavoriteItem(FavoriteItem item) {
        return FavoriteItem.insertOrUpdateFavoriteItem(db, item);
    }

    public synchronized boolean deleteFavoriteItem(FavoriteItem item) {
        return FavoriteItem.deleteFavoriteItem(db, item);
    }

    public synchronized boolean deleteAllFavoriteItem() {
        return FavoriteItem.deleteAllFavoriteItems(db);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (oldVersion) {
                case 1:
                    updateVer1ToVer2(db);
                case 2:
                    updateVer2ToVer3(db);
                case 3:
                    updateVer3ToVer4(db);
            }
        }

        private void updateVer1ToVer2(SQLiteDatabase db) {
            BookmarkItem.createTableBookmarks(db);
            FavoriteItem.createTableFavorite(db);
        }

        private void updateVer2ToVer3(SQLiteDatabase db) {
            DownItem.updateAddColumnVer2to3(db);
        }

        private void updateVer3ToVer4(SQLiteDatabase db) {
            DownItem.updateAddColumnVer3to4(db);
            TrendItem.createTableTrends(db);
            ReportItem.createReportTable(db);
        }

        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            DownItem.createTableDownload(db);
            BookmarkItem.createTableBookmarks(db);
            FavoriteItem.createTableFavorite(db);
            TrendItem.createTableTrends(db);
            ReportItem.createReportTable(db);
        }
    }
}
