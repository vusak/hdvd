package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

public class HistoryItem extends TableObject {
    public static final String TABLE_NAME = "history";

    public static final String ROW_ID = "_id";
    public static final String HISTORY_ID = "history_id";
    public static final String HISTORY_URL = "history_url";
    public static final String HISTORY_SITE = "history_site";
    public static final String HISTORY_TITLE = "history_title";
    public static final String HISTORY_ICON = "history_icon";
    public static final String HISTORY_AT = "history_at";
    public static final Creator<HistoryItem> CREATOR = new Creator<HistoryItem>() {
        @Override
        public HistoryItem createFromParcel(Parcel source) {
            return new HistoryItem(source);
        }

        @Override
        public HistoryItem[] newArray(int size) {
            return new HistoryItem[size];
        }
    };
    private long id;
    private String historyId;
    private String url;
    private String site;
    private String title;
    private byte[] icon;
    private long historyAt;
    private boolean isChecked = false;

    public HistoryItem() {
        super();
    }

    public HistoryItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.historyId = in.readString();
        this.url = in.readString();
        this.site = in.readString();
        this.title = in.readString();
        in.readByteArray(this.icon);
        this.historyAt = in.readLong();
    }

    public static void createTableHistory(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(HISTORY_ID + " text not null unique, ")
                .append(HISTORY_URL + " text not null, ")
                .append(HISTORY_SITE + " text, ")
                .append(HISTORY_TITLE + " text, ")
                .append(HISTORY_ICON + " BLOB, ")
                .append(HISTORY_AT + " integer not null) ")
                .toString());
    }

    public static HistoryItem getHistoryItem(SQLiteDatabase db, String id) {
        List<HistoryItem> list = getHistoryItems(db, null, HISTORY_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

//    public static boolean deleteHistory(SQLiteDatabase db, int past) {
//        long current = System.currentTimeMillis();
//        long compare = 0;
//
//        switch (past) {
//            case ClearHistoryDialog.CLEAR_PAST_HOUR :
//                compare = 1000 * 60 * 60;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_DAY:
//                compare = 1000 * 60 * 60 * 24;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_WEEK:
//                compare = 1000 * 60 * 60 * 24 * 7;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_MONTH:
//                compare = 1000 * 60 * 60 * 24 * 7 * 4;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_BEGINNING:
//                return deleteAllHistoryItems(db);
//        }
//
//        compare = current - compare;
//        try {
//            db.delete(TABLE_NAME, HISTORY_AT + " > " + compare, null);
//            return true;
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }

    public static List<HistoryItem> getHistoryItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<HistoryItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                HistoryItem item = new HistoryItem()
                        .setRowId(c)
                        .setHistoryId(c)
                        .setHistoryUrl(c)
                        .setHistorySite(c)
                        .setHistoryTitle(c)
                        .setHistoryIcon(c)
                        .setHistoryAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateHistoryItem(SQLiteDatabase db, HistoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putHistoryId(v)
                    .putHistoryUrl(v)
                    .putHistorySite(v)
                    .putHistoryIcon(v)
                    .putHistoryTitle(v)
                    .putHistoryAt(v);

            int rowAffected = db.update(TABLE_NAME, v, HISTORY_ID + " =? ", new String[]{item.getHistoryId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteHistoryItem(SQLiteDatabase db, HistoryItem item) {
        try {
            db.delete(TABLE_NAME, HISTORY_ID + " =? ", new String[]{item.getHistoryId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllHistoryItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(historyId);
        out.writeString(url);
        out.writeString(site);
        out.writeString(title);
        out.writeByteArray(icon);
        out.writeLong(historyAt);
    }

    public long getRowId() {
        return this.id;
    }

    public HistoryItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public HistoryItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public HistoryItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getHistoryId() {
        return this.historyId;
    }

    public HistoryItem setHistoryId(String id) {
        this.historyId = id;

        return this;
    }

    public HistoryItem setHistoryId(Cursor c) {
        this.historyId = s(c, HISTORY_ID);

        return this;
    }

    public HistoryItem putHistoryId(ContentValues v) {
        v.put(HISTORY_ID, historyId);

        return this;
    }

    public String getHistoryUrl() {
        return this.url;
    }

    public HistoryItem setHistoryUrl(String url) {
        this.url = url;

        return this;
    }

    public HistoryItem setHistoryUrl(Cursor c) {
        this.url = s(c, HISTORY_URL);

        return this;
    }

    public HistoryItem putHistoryUrl(ContentValues v) {
        v.put(HISTORY_URL, url);

        return this;
    }

    public String getHistorySite() {
        return this.site;
    }

    public HistoryItem setHistorySite(String site) {
        this.site = site;

        return this;
    }

    public HistoryItem setHistorySite(Cursor c) {
        this.site = s(c, HISTORY_SITE);

        return this;
    }

    public HistoryItem putHistorySite(ContentValues v) {
        v.put(HISTORY_SITE, site);

        return this;
    }

    public String getHistoryTitle() {
        return this.title;
    }

    public HistoryItem setHistoryTitle(String title) {
        this.title = title;

        return this;
    }

    public HistoryItem setHistoryTitle(Cursor c) {
        this.title = s(c, HISTORY_TITLE);

        return this;
    }

    public HistoryItem putHistoryTitle(ContentValues v) {
        v.put(HISTORY_TITLE, title);

        return this;
    }

    public long getHistoryAt() {
        return this.historyAt;
    }

    public HistoryItem setHistoryAt(long at) {
        this.historyAt = at;

        return this;
    }

    public HistoryItem setHistoryAt(Cursor c) {
        this.historyAt = l(c, HISTORY_AT);

        return this;
    }

    public HistoryItem putHistoryAt(ContentValues v) {
        v.put(HISTORY_AT, historyAt);

        return this;
    }

    public byte[] getHistoryIcon() {
        return this.icon;
    }

    public HistoryItem setHistoryIcon(byte[] icon) {
        this.icon = icon;

        return this;
    }

    public HistoryItem setHistoryIcon(Cursor c) {
        this.icon = blob(c, HISTORY_ICON);

        return this;
    }

    public HistoryItem putHistoryIcon(ContentValues v) {
        v.put(HISTORY_ICON, icon);

        return this;
    }


    public boolean isChecked() {
        return this.isChecked;
    }

    public HistoryItem setChecked(boolean isChecked) {
        this.isChecked = isChecked;

        return this;
    }

    public void setChecked() {
        this.isChecked = !this.isChecked;
    }
}
