package com.ne.hdv.common;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

public class GreetingDialog extends BaseDialogFragment {

    public static final String TAG = GreetingDialog.class.getSimpleName();

    private TextView messageText;
    private Button positiveButton;

    public static GreetingDialog newInstance(String tag) {
        GreetingDialog d = new GreetingDialog();

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_greeting;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        messageText = fv(R.id.text_message);
        positiveButton = fv(R.id.button_positive);
        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() == null)
            return;

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (getDialog() == null || getDialog().getWindow() == null)
                return;

            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.85f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initializeViews() {
        positiveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (positiveListener != null)
                    positiveListener.onDialogPositive(GreetingDialog.this, tag);

                dismiss();
            }
        });

//        messageText.setText(Html.fromHtml(r.s(R.string.dlg_first_noti_message)));
        messageText.setText(r.s(R.string.dlg_first_noti_message));
    }
}
