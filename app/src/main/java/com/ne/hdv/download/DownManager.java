package com.ne.hdv.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.ne.hdv.BaseApplication;
import com.ne.hdv.MainActivity;
import com.ne.hdv.R;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.DBController;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.SPController;
import com.ne.hdv.data.DownItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Cecil on 2017-05-16.
 */
public class DownManager {
//    private static int MAX_DOWNLOAD_COUNT = 2;

    private static Map<String, DownTask> downloadTasks = Collections.synchronizedMap(new HashMap<String, DownTask>());
    private ArrayList<String> downloadRequestList = new ArrayList<>();
//    private Set<String> downloadRequestList = new HashSet<String>();

    private BaseApplication app;
    private DBController db;
    private SPController sp;
    private BaseApplication.ResourceWrapper r;
    private NotificationManager notiManager;
    private List<DownListener> listeners = new ArrayList<DownListener>();

    public DownManager(BaseApplication baseApplication) {
        app = baseApplication;
        db = app.getDBController();
        sp = app.getSPController();
        r = app.getResourceWrapper();

        notiManager = (NotificationManager) app.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notiManager.cancelAll();
    }

    public void addListener(DownListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void removeListener(DownListener listener) {
        listeners.remove(listener);
    }

    public boolean addToRequestList(String uuid) {
        boolean result = false;
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(uuid))
                result = downloadRequestList.add(uuid);
        }

        if (downloadTasks.size() < sp.getMaxDownloads() + 1) {
            DownItem item = db.getDownloadItem(uuid);
            if (item != null)
                startDownload(uuid, item, item.getDownloadUrl(), item.getDownloadStartAt() != 0);
        }
        return result;
    }

    public void startDownload(String id, DownItem item, String url, boolean continuing) {
        DownTask t = new DownTask(this, id, item);
        synchronized (downloadTasks) {
            downloadTasks.put(id, t);
        }

        if (item != null) {
            t.download(url, Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName(), continuing, null);

            if (item.getDownloadStartAt() == 0)
                item.setDownloadStartAt((int) (System.currentTimeMillis() / 1000));
            item.setDownloadState(DownItem.DOWNLOAD_STATE_DOWNLOADING);
            db.updateDownloadState(item);

            setNotification(item);

            for (DownListener listener : listeners) {
                listener.onDownloadStart(item);
            }
        }
    }

    public void cancelAll(List<DownItem> list) {
        synchronized (downloadTasks) {
            Set<String> keySet = downloadTasks.keySet();
            for (String key : keySet) {
                DownTask task = downloadTasks.get(key);
                task.cancel(true);

                if (task.getId() != null) {
                    task.getItem().setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
                    db.updateDownloadState(task.getItem());

                    if (list == null)
                        setNotification(task.getItem());
                }
            }

            downloadTasks.clear();
        }

        synchronized (downloadRequestList) {
            downloadRequestList.clear();
        }

        notiManager.cancel(Common.NOTI_ID);
        if (list == null)
            return;

        for (DownItem item : list) {
            notiManager.cancel((int) item.getRowId());
        }
    }

    public boolean contains(String id) {
        synchronized (downloadRequestList) {
            return downloadRequestList.contains(id);
        }
    }

    public boolean isEmpty() {
        synchronized (downloadRequestList) {
            return downloadRequestList.isEmpty();
        }
    }

    public void removeFromRequestList(String id) {
        synchronized (downloadRequestList) {
            downloadRequestList.remove(id);
        }
    }

    public void cancelDownload(String id, DownItem item) {
        synchronized (downloadTasks) {
            DownTask t = downloadTasks.remove(id);
            if (t != null)
                t.cancel(true);
        }

        removeFromRequestList(id);

        if (item != null) {
            item.setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
            db.updateDownloadState(item);
            setNotification(item);
        }
    }

    public void onDownloadProgress(String id, DownItem item, long progress, long total) {
        synchronized (downloadRequestList) {
            if (!downloadRequestList.contains(id))
                return;
        }

        if (item != null) {
            float downloadProgress = progress / (float) total;
            item.setDownloadState(DownItem.DOWNLOAD_STATE_DOWNLOADING);
            item.setDownProgress(downloadProgress);
            item.setDownloadFileRemainSize(progress);
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
            setNotification(item);
        }

        for (DownListener listener : listeners) {
            listener.onDownloadProgress(item, progress, total);
        }
    }

    public void onDownloadSuccess(String id, DownItem item) {
        synchronized (downloadTasks) {
            downloadTasks.remove(id);
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "")));
                mIntent.setData(uri);
                app.getApplicationContext().sendBroadcast(mIntent);
            } else {
                app.getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        removeFromRequestList(id);

        if (item != null) {
            item.setDownloadState(DownItem.DOWNLOAD_STATE_DONE);
            item.setDownloadAt((int) System.currentTimeMillis() / 1000);
            item.setDownloadFileRemainSize(item.getDownloadFileTotalSize());
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
            setNotification(item);
        }

        for (DownListener listener : listeners) {
            listener.onDownloadSuccess(item);
        }

        int index = 0;
        while (downloadRequestList.size() > index && downloadTasks.size() < sp.getMaxDownloads() + 1) {
            DownItem temp = db.getDownloadItem(downloadRequestList.get(index));
            if (temp != null) {
                startDownload(temp.getDownloadId(), temp, temp.getDownloadUrl(), temp.getDownloadStartAt() != 0);
                index++;
            }
        }

    }

    public void onDownloadError(String id, DownItem item, int resultCode) {
        synchronized (downloadTasks) {
            downloadTasks.remove(id);
        }

        if (item != null) {
            item.setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
            db.updateDownloadState(item);
            item = db.getDownloadItem(item.getDownloadId());
            setNotification(item);
        }

        for (DownListener listener : listeners) {
            listener.onDownloadError(item, resultCode);
        }
    }

    public void setNotification(DownItem item) {
        String contentText = "";

        if (item == null)
            return;

        switch (item.getDownloadState()) {
            case DownItem.DOWNLOAD_STATE_DONE:
                contentText = "Completed";
                break;

            case DownItem.DOWNLOAD_STATE_DOWNLOADING:
                if (item.getDownloadFileRemainSize() <= 0)
                    contentText = "Ready";
                else
                    contentText = FileUtil.convertToStringRepresentation(item.getDownloadFileRemainSize()) + " / " + FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize());
                break;

            case DownItem.DOWNLOAD_STATE_WAITING:
                contentText = "Ready";
                break;

            case DownItem.DOWNLOAD_STATE_PAUSE:
                contentText = "Paused";
                break;

        }

        Bitmap thumbnail = null;
        if (item.getDownloadThumbnail() != null)
            thumbnail = ImageUtil.getImage(item.getDownloadThumbnail(), 100, 100);
//        if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DONE) {
//            String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
//            File file = new File(path);
//            if (file.exists()) {
//                thumbnail = ImageUtil.getThumbnailFromFilepath(path);
//            }
//        }
//        else if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_PAUSE) {
//            File file = new File(item.getDownloadFilePath());
//            if (file.exists()) {
//                thumbnail = ImageUtil.getThumbnailFromFilepath(item.getDownloadFilePath());
//            }
//        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(app.getBaseContext())
                .setSmallIcon(R.drawable.icon_download)
                .setContentTitle(item.getDownloadFileName())
                .setContentText(contentText)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);
        if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DOWNLOADING || item.getDownloadState() == DownItem.DOWNLOAD_STATE_WAITING) {
            int total = 0;
            int remain = 0;
            int count = 0;
            synchronized (downloadTasks) {
                Set<String> keySet = downloadTasks.keySet();
                for (String key : keySet) {
                    DownItem dItem = downloadTasks.get(key).getItem();

                    if (dItem.getDownloadState() != DownItem.DOWNLOAD_STATE_DONE) {
                        count++;
                        total += dItem.getDownloadFileTotalSize();
                        remain += dItem.getDownloadFileRemainSize();
                    }
                    notiManager.cancel((int) item.getRowId());
                }
            }
            builder.setProgress(total, remain, false)
                    .setContentTitle(count + " Downloading...");
            if (remain == 0 || total == 0)
                builder.setContentText("Ready");
            else
                builder.setContentText(FileUtil.convertToStringRepresentation(remain) + " / " + FileUtil.convertToStringRepresentation(total));
        }

        if (thumbnail != null)
            builder.setLargeIcon(thumbnail);
        else
            builder.setLargeIcon(BitmapFactory.decodeResource(app.getBaseContext().getResources(), R.drawable.ic_videocam_gray_36));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            builder.setChannelId(Common.NOTIFICATION_CHANNEL_ID);

        Intent resultIntent = new Intent(app.getApplicationContext(), MainActivity.class);
        resultIntent.putExtra(Common.FRAG_INTENT_DOWNLOAD_STATUS, item.getDownloadState());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(app.getBaseContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent((int) item.getRowId(), PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DONE || item.getDownloadState() == DownItem.DOWNLOAD_STATE_PAUSE) {
            notiManager.notify((int) item.getRowId(), builder.build());
            if (downloadTasks.size() <= 0)
                notiManager.cancel(Common.NOTI_ID);
        }
//        else
//            notiManager.notify(Common.NOTI_ID, builder.build());
    }

    public static interface DownListener {
        public void onDownloadStart(DownItem item);

        public void onDownloadProgress(DownItem item, long progress, long total);

        public void onDownloadSuccess(DownItem item);

        public void onDownloadError(DownItem item, int errorCode);
    }
}
