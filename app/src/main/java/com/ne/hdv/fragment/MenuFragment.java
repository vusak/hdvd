package com.ne.hdv.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.ne.hdv.LearnMoreActivity;
import com.ne.hdv.MainActivity;
import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.common.PremiumDialog;
import com.ne.hdv.common.SettingsDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ne.hdv.common.LogUtil.DEBUG_MODE;
import static com.ne.hdv.common.Util.showToast;


public class MenuFragment extends BaseFragment implements OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static final String TAG = MenuFragment.class.getSimpleName();
    MainActivity mFragActivity;
    String deleteDir;

    LinearLayout manageVideoLayout, delVideoLayout, delRecordLayout;
    LinearLayout searchEngineLayout, maxDownloadLayout, timeFilterLayout, moreLayout, suggestLayout, helpVideoLayout;
    LinearLayout termsLayout, premiumLayout;
    TextView premiumText;
    View premiumView;
    Button delVideoButton, delRecordButton;
    Switch cellularSwitch, notiSwitch, savePwdSwitch, reportSwitch;
    TextView textDirectory, textMax, textEngine, textTime, textVersion, textNotiSub;
    int countVideo;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == 0) { // 파일삭제
                //getFileCounts();
                try {
                    showToast(getActivity(), "Delete completed", true);
                    //mFragActivity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(deleteDir)));
                    //mFragActivity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(fileToAddInMediaStore)));


                } catch (Exception e) {
                }
            } else if (msg.what == 1) { // 갯수 표시
                textDirectory.setText(countVideo + " " + getActivity().getResources().getString(R.string.delete_video2));
            }
        }
    };
    Runnable deleteRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                File[] files = new File(deleteDir).listFiles();
                if (files == null) {
                    return;
                } else {
                    for (File f : files)
                        DeleteRecursive(f);
                }

                mHandler.sendEmptyMessage(0);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    };
    private MenuFragmentListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_menu;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

    }

    public void setListener(MenuFragmentListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_search_engine:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_search_engine)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.menu_search_engine), list, sp.getSearchEngine());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setSearchEngine(index);
                            textEngine.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.manage_video:
                launchHDF();
                break;

            case R.id.btn_del_down_folder:
            case R.id.del_video:
                deleteDir = Common.getVideoDir(getActivity());
                deleteVideo();
                break;

            case R.id.layout_max_download:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_max_download)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.menu_max_downloads), list, sp.getMaxDownloads());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setMaxDownloads(index);
                            textMax.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.layout_time_filter:
                if (fdf(SettingsDialog.TAG) == null) {
                    final ArrayList<String> list = new ArrayList<>();
                    list.addAll(Arrays.asList(r.sa(R.array.settings_time_filter)));
                    SettingsDialog dlg = SettingsDialog.newInstance(SettingsDialog.TAG,
                            r.s(R.string.menu_use_time_filter_main), list, sp.getTimeFiltering());
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            int index = list.indexOf(tag);
                            if (index > -1)
                                sp.setTimeFiltering(index);
                            textTime.setText(tag);
                        }
                    });
                    sdf(dlg);


                }
                break;

            case R.id.layout_clear_record:
            case R.id.btn_del_records:
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG)
                            .setMessage(r.s(R.string.msg_delete_records))
//                            .setTitle(r.s(R.string.dlg_delete_title))
                            .setNegativeLabel(r.s(R.string.str_cancel))
                            .setPositiveLabel(r.s(R.string.str_delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            new ClearRecordTask().execute();
                        }
                    });
                    sdf(dlg);
                }
                break;

            case R.id.layout_more:
                moreInfo();
                break;

            case R.id.layout_suggest:
                sendFeedback();
                break;

            case R.id.layout_help_video:
                showHelpVideo();
                break;

            case R.id.layout_terms:
                showLearnMore();
                break;

            case R.id.layout_get_premium:
                getPremium();

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch_use_cellular_data:
                sp.setUseCellularData(isChecked);
                break;

            case R.id.switch_use_notification:
                sp.setUseNotification(isChecked);
                break;

            case R.id.switch_use_report:
                sp.setUseReport(isChecked);
                break;

            case R.id.switch_save_password:
                sp.setSetUseSavePasswords(isChecked);
                break;
        }
    }

    public void startPremiumMode() {
        if (premiumLayout != null) {
            premiumLayout.setVisibility(sp.isPremiumMode() && !DEBUG_MODE ? View.GONE : View.VISIBLE);
            premiumView.setVisibility(sp.isPremiumMode() && !DEBUG_MODE ? View.GONE : View.VISIBLE);
            premiumText.setText(sp.isPremiumMode() ? "Reset Premium!" : r.s(R.string.message_premium));
            premiumText.setTextColor(ContextCompat.getColor(getActivity(), sp.isPremiumMode() ? R.color.main_red : R.color.primary1_a100));
        }
    }

    private void launchHDF() {
        try {
            if (getPackageList()) {
                Intent intent = mFragActivity.getPackageManager()
                        .getLaunchIntentForPackage(Common.PACKAGE_TM_FILEMANAGER);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("path", Common.getVideoDir(getActivity()));
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(Common.PLAYSTORE_URL + Common.PACKAGE_TM_FILEMANAGER);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(Common.PLAYSTORE_URL + Common.PACKAGE_TM_FILEMANAGER);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    private boolean getPackageList() {
        boolean isExist = false;

        PackageManager pkgMgr = mFragActivity.getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith(Common.PACKAGE_TM_FILEMANAGER)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    private void deleteVideo() {

        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setMessage(r.s(R.string.ru_del_video));
            dlg.setNegativeLabel(r.s(R.string.str_cancel));
            dlg.setPositiveLabel(r.s(R.string.str_delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    Thread thread = new Thread(deleteRunnable);
                    thread.start();
                }
            });
            sdf(dlg);
        }
    }

    private void moreInfo() {
        try {
            Uri uri = Uri.parse("https://play.google.com/store/apps/dev?id=6800756349887572109");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        } catch (Exception e) {
        }
    }

    private void sendFeedback() {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "dooyonce13@gmail.com"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Suggest to HDV Downloader"); //제목
            getActivity().startActivity(intent);
        } catch (Exception e) {
        }
    }

    private void showHelpVideo() {
        try {
//			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=b9yYkFJMUyg&feature=youtu.be"));
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Common.MAIN_TUTORIAL_URL));
            getActivity().startActivity(intent);
        } catch (Exception e) {
        }
    }

    private void showLearnMore() {
        try {
            Intent intent = new Intent(getActivity(), LearnMoreActivity.class);
            startActivity(intent);
        } catch (Exception e) {

        }
    }

    private void getPremium() {
        if (sp.isPremiumMode()) {
            if (listener != null)
                listener.onResetPremiumMode();
        } else {
            if (fdf(PremiumDialog.TAG) == null) {
                PremiumDialog dlg = PremiumDialog.newInstance(PremiumDialog.TAG, getActivity());
                dlg.setListener(new PremiumDialog.PremiumDialogListener() {
                    @Override
                    public void onPurchaseButtonClicked() {
                        if (listener != null)
                            listener.onStartPremiumMode();
                    }

                    @Override
                    public void onNotNowButtonClicked() {

                    }
                });
                sdf(dlg);
            }
        }
    }

    private void clearApplicationCache(File dir) {
        if (dir == null)
            dir = app.getCacheDir();

        if (dir == null)
            return;

        File[] child = dir.listFiles();
        try {
            for (int i = 0; i < child.length; i++) {
                if (child[i].isDirectory())
                    clearApplicationCache(child[i]);
                else
                    child[i].delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (reportSwitch != null)
            reportSwitch.setChecked(sp.isUseReport());
        Common.NOW_LOADING = false;
    }

    @Override
    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(activity, attrs, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        searchEngineLayout = fv(R.id.layout_search_engine);
        searchEngineLayout.setOnClickListener(this);
        textEngine = fv(R.id.text_search_engine);
        textEngine.setText(r.sa(R.array.settings_search_engine)[sp.getSearchEngine()]);

        manageVideoLayout = fv(R.id.manage_video);
        manageVideoLayout.setOnClickListener(this);

        delVideoLayout = fv(R.id.del_video);
        delVideoLayout.setOnClickListener(this);
        delVideoButton = fv(R.id.btn_del_down_folder);
        delVideoButton.setOnClickListener(this);
        textDirectory = fv(R.id.txt_video);

        maxDownloadLayout = fv(R.id.layout_max_download);
        maxDownloadLayout.setOnClickListener(this);
        textMax = fv(R.id.text_max_download);
        textMax.setText(r.sa(R.array.settings_max_download)[sp.getMaxDownloads()]);

        timeFilterLayout = fv(R.id.layout_time_filter);
        timeFilterLayout.setOnClickListener(this);
        textTime = fv(R.id.text_time_filter);
        textTime.setText(r.sa(R.array.settings_time_filter)[sp.getTimeFiltering()]);

        cellularSwitch = fv(R.id.switch_use_cellular_data);
        cellularSwitch.setChecked(sp.isUseCellularData());
        cellularSwitch.setOnCheckedChangeListener(this);

        textNotiSub = fv(R.id.text_noti_popup);
        textNotiSub.setVisibility(TextUtils.isEmpty(textNotiSub.getText().toString()) ? View.GONE : View.VISIBLE);
        notiSwitch = fv(R.id.switch_use_notification);
        notiSwitch.setChecked(sp.isUseNotification());
        notiSwitch.setOnCheckedChangeListener(this);

        reportSwitch = fv(R.id.switch_use_report);
        reportSwitch.setChecked(sp.isUseReport());
        reportSwitch.setOnCheckedChangeListener(this);

        savePwdSwitch = fv(R.id.switch_save_password);
        savePwdSwitch.setChecked(sp.isSavePasswords());
        savePwdSwitch.setOnCheckedChangeListener(this);

        delRecordLayout = fv(R.id.layout_clear_record);
        delRecordLayout.setOnClickListener(this);
        delRecordButton = fv(R.id.btn_del_records);
        delRecordButton.setOnClickListener(this);


        // ETC
        moreLayout = fv(R.id.layout_more);
        moreLayout.setOnClickListener(this);
        suggestLayout = fv(R.id.layout_suggest);
        suggestLayout.setOnClickListener(this);
        helpVideoLayout = fv(R.id.layout_help_video);
        helpVideoLayout.setOnClickListener(this);
        termsLayout = fv(R.id.layout_terms);
        termsLayout.setOnClickListener(this);
        premiumLayout = fv(R.id.layout_get_premium);
        premiumLayout.setOnClickListener(this);
        premiumView = fv(R.id.view_premium);
        premiumText = fv(R.id.text_premium);

        premiumLayout.setVisibility(sp.isPremiumMode() && !DEBUG_MODE ? View.GONE : View.VISIBLE);
        premiumView.setVisibility(sp.isPremiumMode() && !DEBUG_MODE ? View.GONE : View.VISIBLE);
        premiumText.setText(sp.isPremiumMode() ? "Reset Premium!" : r.s(R.string.message_premium));
        premiumText.setTextColor(ContextCompat.getColor(getActivity(), sp.isPremiumMode() ? R.color.main_red : R.color.primary1_a100));

        mFragActivity = (MainActivity) getActivity();
        textVersion = fv(R.id.version);
        String version;
        try {
            PackageInfo pi = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pi.versionName;
//            textVersion.setText("HDV Downloader Version : " + version);
            textVersion.setText(version);
        } catch (NameNotFoundException e) {
        }

        Common.NOW_LOADING = false; // 로딩 끝남
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public static interface MenuFragmentListener {
        public void onStartPremiumMode();

        public void onResetPremiumMode();
    }

    class ClearRecordTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            clearApplicationCache(null);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            showToast(mFragActivity, r.s(R.string.msg_delete_complete), false);
            super.onPostExecute(aVoid);

        }
    }

}
