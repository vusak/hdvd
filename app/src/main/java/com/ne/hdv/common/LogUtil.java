package com.ne.hdv.common;

import android.util.Log;

public final class LogUtil {
    public static boolean DEBUG_MODE = false;
    private static String TAG = "JMOBILE_HDVD";

    private LogUtil() {
    }

    public static void log(String msg) {
        log(TAG, msg);
    }

    public static void log(String tag, String msg) {
        if (DEBUG_MODE)
            Log.d(tag, msg == null ? "null" : msg);
    }

    public static void log(Throwable tr) {
        log(TAG, tr);
    }

    public static void log(String tag, Throwable tr) {
        if (DEBUG_MODE)
            Log.d(tag, Log.getStackTraceString(tr));
    }
}
