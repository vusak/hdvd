package com.ne.hdv.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.provider.MediaStore.Video.Thumbnails;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.ne.hdv.R;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.data.DownItem;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    final int stub_id = R.drawable.stub;
    Animation fadeanim;
    Context mContext;
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    ExecutorService executorService;
    int imgSize;
    String url;
    int bmpSample = 1;
    int THUMB_MODE;
    DownItem item = null;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
        mContext = context;
    }

    public static Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                // We have no memory to rotate. Return the original bitmap.
            }
        }
        return b;
    }

    public void displayImage(ImageView imageView, String path, int size, int bmp, int t) {
        imageViews.put(imageView, path);
        fadeanim = AnimationUtils.loadAnimation(mContext, R.anim.image_fade_in_100);
        imgSize = size;
        THUMB_MODE = t;
        bmpSample = bmp;


        Bitmap bitmap = memoryCache.get(path);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            queuePhoto(path, imageView);
        }
    }

    public void displayImage(ImageView imageView, DownItem item) {
        imageViews.put(imageView, item.getDownloadFilePath());
        fadeanim = AnimationUtils.loadAnimation(mContext, R.anim.image_fade_in_100);
        this.item = item;

        Bitmap bitmap = memoryCache.get(item.getDownloadFilePath());
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else {
            queuePhoto(item.getDownloadFilePath(), imageView);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmapByLocal(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = bmpSample;

        Bitmap bmp;
        if (THUMB_MODE == 0) { //비디오
            bmp = ThumbnailUtils.createVideoThumbnail(path, Thumbnails.MICRO_KIND);
        } else { //이미지
            bmp = BitmapFactory.decodeFile(path, options);
        }

//		Bitmap bmp;
//		bmp = ImageUtil.getThumbnailFromFilepath(path);

        return bmp;
    }

    private Bitmap getBitmapByUrl(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = bmpSample;

        Bitmap bmp = null;

        File file = new File(path);
        if (file.exists())
            bmp = ImageUtil.getThumbnailFromFilepath(path);

        return bmp;
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        return tag == null || !tag.equals(photoToLoad.url);
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {


        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }


        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            Bitmap bmp;
            if (item == null)
                bmp = getBitmapByLocal(photoToLoad.url);
            else
                bmp = getBitmapByUrl(photoToLoad.url);

            memoryCache.put(photoToLoad.url, bmp);

            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(mContext, bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        Context mmContext;

        public BitmapDisplayer(Context c, Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
            mmContext = c;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);
            } else {
                if (item != null)
                    photoToLoad.imageView.setImageResource(R.drawable.icon_down);
                else
                    photoToLoad.imageView.setImageResource(stub_id);
            }
        }
    }


}
