package com.ne.hdv.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.data.FavoriteItem;

import java.net.URL;
import java.util.List;

public class FavoriteAdapter extends ReSelectableAdapter<FavoriteItem, FavoriteAdapter.ReHistoryHolder> {

    private Context context;

    public FavoriteAdapter(Context context, int layoutId, List<FavoriteItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ReHistoryHolder holder, int position) {
        FavoriteItem item = items.get(position);
//        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "bebasneue.otf");
        String url = item.getFavoriteUrl().replace("http://", "");
        url = url.replace("https://", "");
        try {
            URL add = new URL(item.getFavoriteSite());
            url = add.getHost();
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
        holder.url.setText(url);

        String initial = url.replace("www.", "");
        initial = initial.replace("m.", "");
        initial = initial.replace("tv.", "");
        holder.initial.setText(initial.substring(0, 1));

        if (TextUtils.isEmpty(item.getFavoriteTitle())) {
            holder.title.setText(url);
        } else
            holder.title.setText(item.getFavoriteTitle());
        if (item.getFavoriteIcon() != null) {
            holder.icon.setBackgroundResource(R.color.transparent);
            holder.icon.setImageBitmap(ImageUtil.getImage(item.getFavoriteIcon(), 24, 24));
            holder.icon.setPadding(0, 0, 0, 0);
            holder.initial.setVisibility(View.GONE);
        } else {
            holder.icon.setBackgroundResource(R.drawable.app_background_s);
            holder.icon.setImageResource(R.color.transparent);
            holder.initial.setVisibility(View.VISIBLE);
//            holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_public_navy_18));
//            int padding = (int) Util.convertDpToPixel(3, context);
//            holder.icon.setPadding(padding, padding, padding, padding);
        }

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
    }

    @Override
    public ReHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReHistoryHolder holder = new ReHistoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.url = holder.fv(R.id.text_url);
        holder.mask = holder.fv(R.id.image_mask);
        holder.initial = holder.fv(R.id.text_initial);
        return holder;
    }

    public class ReHistoryHolder extends ReAbstractViewHolder {

        ImageView icon, mask;
        TextView title, url, initial;

        public ReHistoryHolder(View itemView) {
            super(itemView);
        }

    }
}
