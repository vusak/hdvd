package com.ne.hdv.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.ne.hdv.R;
import com.ne.hdv.adapter.BookmarkAdapter;
import com.ne.hdv.adapter.FavoriteAdapter;
import com.ne.hdv.adapter.ReAdapter;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.common.AddBookmarkDialog;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.common.PremiumDialog;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.BookmarkItem;
import com.ne.hdv.data.FavoriteItem;
import com.ne.hdv.data.ReportItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FrontFragment extends BaseFragment {
    public static final String TAG = FrontFragment.class.getSimpleName();
    TextView urlText;
    BrowserFragment browserFragment;
    URLEditFragment urlEditFragment;
    LinearLayout browserLayout, urlEditLayout;
    NestedScrollView layoutNotice;

    RecyclerView bookmarkLv, favoriteLv, featuredAppLv;
    TextView emptyFavoritText;
    LinearLayoutManager bookmarkManager, favoriteManager, featuredAppManager;
    BookmarkAdapter bookmarkAdapter;
    ArrayList<BookmarkItem> bookmarkList;
    FavoriteAdapter favoriteAdapter;
    ArrayList<FavoriteItem> favoriteList;
    ImageButton deleteButton;
    View editModeView1, editModeView2, editModeView3, editModeView4, editModeView5;

    FrameLayout nativeAdsLayout;
    NativeAd fanNativeAd;

    // premium
    CardView premiumCardView;
    LinearLayout premiumLayout;
    ImageButton premiumCloseButton;

    private FrontFragmentListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_front;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        layoutNotice = fv(R.id.layout_notice);

        urlText = fv(R.id.btn_input_url);
        urlText.setOnClickListener(v -> visibleUrlEdit(true));

        browserLayout = fv(R.id.layout_fragment_browser);
        browserLayout.setVisibility(View.GONE);
        browserFragment = new BrowserFragment();
        browserFragment.setListener(new BrowserFragment.BrowserFragmentListsner() {
            @Override
            public void onUrlEditClick(String url) {
                visibleUrlEdit(true);
                if (urlEditFragment != null)
                    urlEditFragment.refershList(url);
            }

            @Override
            public void onAccessYoutebe() {
                showYoutubeAlert();
            }

            @Override
            public void onAccessDownloadUrl(String site, String url) {
                if (listener != null)
                    listener.onShowDownloadDialog(true, site, url);
            }

            @Override
            public void onWebviewScrolled() {

            }

            @Override
            public void onSendReport(ReportItem item) {
                if (listener != null)
                    listener.onSendReportUrl(item);
            }

            @Override
            public void onShowToast(String message) {
                if (listener != null)
                    listener.onShowToastMessage(message);
            }

            @Override
            public void onReadyButtonClicked() {
                if (listener != null)
                    listener.onReadyButtonClicked();
            }

            @Override
            public void onClearReadyList() {
                if (listener != null)
                    listener.onReadyListClear();
            }
        });
        browserLayout.post(new Runnable() {
            @Override
            public void run() {
                if (getActivity() == null || getActivity().getSupportFragmentManager() == null)
                    return;

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                if (transaction == null)
                    return;
                if (browserFragment == null)
                    browserFragment = new BrowserFragment();
                transaction.replace(R.id.layout_fragment_browser, browserFragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_fragment_browser, browserFragment).commitAllowingStateLoss();
            }
        });

        urlEditLayout = fv(R.id.layout_fragment_url);
        urlEditLayout.setVisibility(View.GONE);
        urlEditFragment = new URLEditFragment();
        urlEditFragment.setListener(new URLEditFragment.URLEditFragmentListener() {
            @Override
            public void onGotoUrlButtonClick(String url) {
                gotoUrl(url);
                visibleUrlEdit(false);
            }
        });
        urlEditLayout.post(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                if (transaction == null)
                    return;
                if (urlEditFragment == null)
                    urlEditFragment = new URLEditFragment();
                transaction.add(R.id.layout_fragment_url, urlEditFragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
                Util.hideKeyBoard(getActivity());
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.layout_fragment_url, urlEditFragment).commitAllowingStateLoss();
            }
        });

//        giftAdsButton = fv(R.id.button_gift_ads);
//        giftAdsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (listener != null)
//                    listener.onGiftAdsButtonClicked();
//                giftAdsButton.clearAnimation();
//                giftAdsButton.setVisibility(View.GONE);
//            }
//        });
//        giftAdsButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake));
//        giftAdsButton.setVisibility(sp.isDelayedShowGiftAds() ? View.VISIBLE : View.GONE);

//        mContentView = fv(R.id.adView_native_main);
        nativeAdsLayout = fv(R.id.layout_native_ads);

        initPremium();
        initMainList();
        if (!sp.isPremiumMode())
            showNativeAd(); // 네이티브 광고 호출
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
    }

    @Override
    public void onResume() {
        refreshMainList();

        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE)
            browserFragment.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE)
            browserFragment.onPause();
        super.onPause();
    }

    public void setListener(FrontFragmentListener listener) {
        this.listener = listener;
    }

    void gotoUrl(String url) {
//        if (url.matches("(?i).*youtube.*")) { // URL에 youtube 입력시
//            showYoutubeAlert();
//        } else if (url.matches("(?i).*dailymotion.*")){ // URL에 dailymotion 입력시
//            showWarningAlert();
//        } else {
        String SCHEME = "http://";
        if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
            Common.SITE = getSearchEngine() + url;
        } else if (url.matches("http.*")) { // http로 시작
            Common.SITE = url;
        } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
            Common.SITE = SCHEME + url;
        } else {
            Common.SITE = getSearchEngine() + url;
        }
        goToBrowser(Common.SITE);
//        }

    }

    public void goToBrowser(String url) {
        visibleBrowser(true);
        if (browserFragment != null) {
            browserFragment.goUrl(url);
        }
    }

    private void visibleBrowser(boolean show) {
        if (browserLayout == null)
            return;

        browserLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (!show) {
            if (browserFragment != null) {
                browserFragment.stopWebViewLoading();
                browserFragment.clearCurrData();
            }
            refreshMainList();
        } else {
            if (browserFragment != null)
                browserFragment.onResume();
        }
    }

    private void visibleUrlEdit(boolean show) {
        if (nativeAdsLayout == null || urlEditLayout == null)
            return;

        nativeAdsLayout.setVisibility(View.VISIBLE);
        urlEditLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        if (show && urlEditFragment != null)
            urlEditFragment.showKeypad();
        else
            Util.hideKeyBoard(getActivity());

        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE && browserFragment != null) {
            if (show)
                browserFragment.onPause();
            else
                browserFragment.onResume();
        }
    }

    private String getSearchEngine() {
        String engine = Common.SEARCH_ENGINE_GOOGLE;
        switch (sp.getSearchEngine()) {
            case 0: // Google
                engine = Common.SEARCH_ENGINE_GOOGLE;
                break;

            case 1: // Naver
                engine = Common.SEARCH_ENGINE_NAVER;
                break;

            case 2: // bing
                engine = Common.SEARCH_ENGINE_BING;
                break;
        }

        return engine;
    }

    private void initPremium() {
        premiumCardView = fv(R.id.cardview_premium);
        premiumCardView.setVisibility(sp.isShowPremiumBanner() ? View.VISIBLE : View.GONE);
//        if (sp.isPremiumMode() || !sp.isShowPremiumBanner()) {
//            premiumCardView.setVisibility(View.GONE);
//            return;
//        }
        implementSwipeDismiss();

        premiumLayout = fv(R.id.layout_premium);
        premiumLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                premiumBannerClose();
                if (fdf(PremiumDialog.TAG) == null) {
                    PremiumDialog dlg = PremiumDialog.newInstance(PremiumDialog.TAG, getActivity());
                    dlg.setListener(new PremiumDialog.PremiumDialogListener() {
                        @Override
                        public void onPurchaseButtonClicked() {
                            if (listener != null)
                                listener.onStartPremiumMode();
                        }

                        @Override
                        public void onNotNowButtonClicked() {

                        }
                    });
                    sdf(dlg);
                }
            }
        });
        premiumCloseButton = fv(R.id.button_premium_close);
        premiumCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                premiumBannerClose();
            }
        });
    }

    void initMainList() {
        editModeView1 = fv(R.id.view_edit_mode1);
        editModeView2 = fv(R.id.view_edit_mode2);
        editModeView3 = fv(R.id.view_edit_mode3);
        editModeView4 = fv(R.id.view_edit_mode4);
        editModeView5 = fv(R.id.view_edit_mode5);
        editModeView1.setVisibility(View.GONE);
        editModeView2.setVisibility(View.GONE);
        editModeView3.setVisibility(View.GONE);
        editModeView4.setVisibility(View.GONE);
        editModeView5.setVisibility(View.GONE);

        deleteButton = fv(R.id.button_clear_favorite);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                    dlg.setTitle(r.s(R.string.dlg_del_visit_list_title));
                    dlg.setMessage(r.s(R.string.dlg_del_visit_list_msg));
                    dlg.setNegativeLabel(r.s(R.string.str_cancel));
                    dlg.setPositiveLabel(r.s(R.string.str_delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            db.deleteAllFavoriteItem();
                            refreshMainList();
                        }
                    });
                    sdf(dlg);
                }
            }
        });

        bookmarkLv = fv(R.id.lv_bookmark);
        bookmarkManager = new GridLayoutManager(getActivity(), 5);
        bookmarkLv.setLayoutManager(bookmarkManager);
        bookmarkList = new ArrayList<>();
        bookmarkAdapter = new BookmarkAdapter(getActivity(), R.layout.item_quick_dial, bookmarkList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, final Object item) {
                if (bookmarkAdapter.isDeleteMode()) {
                    if (fdf(MessageDialog.TAG) == null) {
                        MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                        dlg.setTitle(r.s(R.string.dlg_del_bookmark_title));
                        dlg.setMessage(r.s(R.string.dlg_del_bookmark_msg));
                        dlg.setNegativeLabel(r.s(R.string.str_cancel));
                        dlg.setPositiveLabel(r.s(R.string.str_delete));
                        dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                            @Override
                            public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                                db.deleteBookmarkItem((BookmarkItem) item);
                                refreshMainList();
                            }
                        });
                        sdf(dlg);
                    }
                } else {
                    BookmarkItem bItem = (BookmarkItem) item;
                    if (bItem.getBookmarkUrl() != null && bItem.getBookmarkUrl().equalsIgnoreCase(BookmarkItem.ID_HOW_TO_USE)) {
                        showHelp();
//                        Intent intent = new Intent(getActivity(), HelpActivity.class);
//                        getActivity().startActivity(intent);
                    } else if (bItem.getBookmarkUrl() != null && bItem.getBookmarkUrl().equalsIgnoreCase(BookmarkItem.ID_DO_IT_YOURSELF)) {
                        showVideo();
                    } else if (bItem.getRowId() == -1) {
                        if (bItem.getBookmarkId().equals(r.s(R.string.bookmark_vedio))) {
                            showVideo();
                        } else {
                            if (fdf(AddBookmarkDialog.TAG) == null) {
                                AddBookmarkDialog dlg = AddBookmarkDialog.newInstance(AddBookmarkDialog.TAG, getActivity(), null);
                                dlg.setListener(new AddBookmarkDialog.AddBookmarkDialogListener() {
                                    @Override
                                    public void onSaveButtonClick(BookmarkItem item) {
                                        new AddBookmarkItem().execute(item);
                                    }

                                    @Override
                                    public void onCancelButtonClick() {

                                    }
                                });
                                sdf(dlg);
                            }
                        }
                    } else if (bItem.getRowId() == -2) {
//                        if (bItem.getBookmarkId().equals(r.s(R.string.bookmark_mobvista)))
//                            showMobvista();
//                        else
                        if (bItem.getBookmarkId().equals(BookmarkItem.ID_AD_QR_SCANNER))
                            launchApp(Common.PACKAGE_QR_SCANNER);
                        else if (bItem.getBookmarkId().equals(BookmarkItem.ID_AD_INSTANT))
                            launchApp(Common.PACKAGE_JM_INSTANT);
                        else if (bItem.getBookmarkId().equals(BookmarkItem.ID_AD_JM_BROWSER))
                            launchApp(Common.PACKAGE_JM_BROWSER);
                        else if (bItem.getBookmarkId().equals(BookmarkItem.ID_AD_FILEMANAGMER))
                            launchApp(Common.PACKAGE_JM_FILEMANAGER);
                        else if (bItem.getBookmarkId().equals(BookmarkItem.ID_AD_EGG_STORY))
                            launchApp(Common.PACKAGE_EGG_STORY);
//                        else if (bItem.getBookmarkId().equals(r.s(R.string.bookmark_jmobile)))
//                            showJMobileApps();
                    } else {
                        String url = bItem.getBookmarkUrl();
                        visibleBrowser(true);
                        if (browserFragment != null)
                            browserFragment.goUrl(url);
                    }
                }
            }

            @Override
            public void OnItemLongClick(int position, final Object item) {
                if (((BookmarkItem) item).getRowId() == -1 || ((BookmarkItem) item).getRowId() == -2)
                    return;

                editModeView1.setVisibility(View.VISIBLE);
                editModeView2.setVisibility(View.VISIBLE);
                editModeView3.setVisibility(View.VISIBLE);
                editModeView4.setVisibility(View.VISIBLE);
                editModeView5.setVisibility(View.VISIBLE);

                if (listener != null)
                    listener.setEditMode(true);
            }
        });


        bookmarkLv.setAdapter(bookmarkAdapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(bookmarkLv);

        favoriteLv = fv(R.id.lv_mostly_visit);
        favoriteManager = new GridLayoutManager(getActivity(), 1);
        favoriteLv.setLayoutManager(favoriteManager);
        favoriteList = new ArrayList<>();
        favoriteAdapter = new FavoriteAdapter(getActivity(), R.layout.item_favorite, favoriteList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                String url = ((FavoriteItem) item).getFavoriteSite();
                if (url == null && TextUtils.isEmpty(url))
                    return;

                visibleBrowser(true);
                if (browserFragment != null)
                    browserFragment.goUrl(url);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        favoriteLv.setAdapter(favoriteAdapter);
        emptyFavoritText = fv(R.id.text_empty_mostly_visit);

    }

    public boolean setHomePress() {
        if (urlEditLayout != null && urlEditLayout.getVisibility() == View.VISIBLE) {
            visibleUrlEdit(false);
            return true;
        }

        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setMessage(r.s(R.string.dlg_message_move_home));
                dlg.setNegativeLabel(r.s(R.string.cancel));
                dlg.setPositiveLabel(r.s(R.string.move));
                dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                    @Override
                    public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                        visibleBrowser(false);
                    }
                });
                sdf(dlg);
            }
            return true;
        }
        return false;
    }

    public boolean setBackPress() {
        if (urlEditLayout != null && urlEditLayout.getVisibility() == View.VISIBLE) {
            visibleUrlEdit(false);
            return true;
        }

        if (browserLayout != null && browserLayout.getVisibility() == View.VISIBLE) {
            if (browserFragment != null && !browserFragment.onPrevButtonClicked())
                visibleBrowser(false);
            return true;
        }
        return false;
    }

    public void setEditMode(boolean edit) {
        if (bookmarkAdapter != null)
            bookmarkAdapter.setDeleteMode(edit);

        editModeView1.setVisibility(edit ? View.VISIBLE : View.GONE);
        editModeView2.setVisibility(edit ? View.VISIBLE : View.GONE);
        editModeView3.setVisibility(edit ? View.VISIBLE : View.GONE);
        editModeView4.setVisibility(edit ? View.VISIBLE : View.GONE);
        editModeView5.setVisibility(edit ? View.VISIBLE : View.GONE);
        refreshMainList();
    }

    void refreshMainList() {
        if (bookmarkList == null || favoriteList == null || bookmarkAdapter == null || favoriteAdapter == null)
            return;
        bookmarkList.clear();
        favoriteList.clear();

        if (sp.isFirstSetHowToUse()) {
            BookmarkItem vItem = new BookmarkItem();
            vItem.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            vItem.setBookmarkShowing(false);
            vItem.setBookmarkOrder((int) db.getNextDBId(BookmarkItem.TABLE_NAME));
            vItem.setBookmarkAt(System.currentTimeMillis());
            Bitmap vBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_diy_orange_50);
            vItem.setBookmarkIcon(ImageUtil.getImageBytes(vBitmap));
            vItem.setBookmarkName(getString(R.string.main_title_featured_app_video));
            vItem.setBookmarkUrl(BookmarkItem.ID_DO_IT_YOURSELF);
            db.insertOrUpdateBookmarkItem(vItem);

            BookmarkItem item = new BookmarkItem();
            item.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBookmarkShowing(false);
            item.setBookmarkOrder((int) db.getNextDBId(BookmarkItem.TABLE_NAME));
            item.setBookmarkAt(System.currentTimeMillis());
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_diy_blue_50);
            item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));
            item.setBookmarkName(getString(R.string.str_how_to_use));
            item.setBookmarkUrl(BookmarkItem.ID_HOW_TO_USE);
            db.insertOrUpdateBookmarkItem(item);
            sp.setFirstSetHowToUse(false);
        }

        ArrayList<BookmarkItem> bList = new ArrayList<>();
        bList.addAll(db.getBookmarkItems());
        if (bList != null && bList.size() > 0) {
            boolean delete = false;
            for (BookmarkItem item : bList) {
                if (!item.getBookmarkUrl().equalsIgnoreCase(BookmarkItem.ID_HOW_TO_USE)
                        && !item.getBookmarkUrl().equalsIgnoreCase(BookmarkItem.ID_DO_IT_YOURSELF)) {
                    delete = true;
                    break;
                }
//                bookmarkList.add(item);
            }
            if (delete) {
                db.deleteBookmarkItem(BookmarkItem.ID_HOW_TO_USE);
                db.deleteBookmarkItem(BookmarkItem.ID_DO_IT_YOURSELF);
            }
            bookmarkList.addAll(db.getBookmarkItems());
        }

        if (bookmarkAdapter != null && !bookmarkAdapter.isDeleteMode() && r.d(getActivity(), R.drawable.ic_add_navy_24) != null) {
            int cnt = 0;
            if (!sp.isPremiumMode() && !getPackageList(Common.PACKAGE_QR_SCANNER)) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.main_title_featured_app_qr_scanner))
                        .setRowId(-2)
                        .setBookmarkId(BookmarkItem.ID_AD_QR_SCANNER));
                cnt++;
            }
            if (false && !sp.isPremiumMode() && !getPackageList(Common.PACKAGE_JM_INSTANT)) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.main_title_featured_app_instant))
                        .setRowId(-2)
                        .setBookmarkId(BookmarkItem.ID_AD_INSTANT));
                cnt++;
            }
            if (false && !sp.isPremiumMode() && !getPackageList(Common.PACKAGE_JM_FILEMANAGER)) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.main_title_featured_app_jmfm))
                        .setRowId(-2)
                        .setBookmarkId(BookmarkItem.ID_AD_FILEMANAGMER));
                cnt++;
            }
            if (false && !sp.isPremiumMode() && !getPackageList(Common.PACKAGE_JM_BROWSER)) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.main_title_featured_app_jmbr))
                        .setRowId(-2)
                        .setBookmarkId(BookmarkItem.ID_AD_JM_BROWSER));
                cnt++;
            }
            if (false && !sp.isPremiumMode() && !getPackageList(Common.PACKAGE_EGG_STORY) && app.getLocale().equals("en")) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.str_egg_story))
                        .setRowId(-2)
                        .setBookmarkId(BookmarkItem.ID_AD_EGG_STORY));
                cnt++;
            }

            if (bookmarkList.size() < Common.MAX_BOOKMARK_COUNT) {
                bookmarkList.add(new BookmarkItem().setBookmarkName(r.s(R.string.str_add_bookmark))
                        .setRowId(-1)
                        .setBookmarkIcon(ImageUtil.getImageBytes(((BitmapDrawable) r.d(getActivity(), R.drawable.ic_add_navy_24)).getBitmap()))
                        .setBookmarkId(r.s(R.string.add_new)));
            }
        }

        ArrayList<FavoriteItem> fList = new ArrayList<>();
        fList.addAll(db.getFavoriteItems());
        if (fList != null && fList.size() > 0) {
            for (FavoriteItem item : fList)
                if (favoriteList.size() < 9)
                    favoriteList.add(item);
        }

        bookmarkAdapter.notifyDataSetChanged();
        favoriteAdapter.notifyDataSetChanged();

        if (deleteButton != null)
            deleteButton.setVisibility(favoriteList.size() > 0 ? View.VISIBLE : View.GONE);
        if (emptyFavoritText != null)
            emptyFavoritText.setVisibility(favoriteList.size() > 0 ? View.GONE : View.VISIBLE);
    }

    public void showVideo() {
        goToBrowser(Common.MAIN_JMOBILE_VIDEO_INSTAGRAM);
    }

    public void showHelp() {
        goToBrowser(Common.MAIN_JMOBILE_HELP_CENTER);
    }

    public void startPremiumMode() {
        if (nativeAdsLayout != null)
            nativeAdsLayout.setVisibility(View.GONE);
        if (premiumCardView != null)
            premiumCardView.setVisibility(View.GONE);
        if (browserFragment != null)
            browserFragment.startPremiumMode();

        refreshMainList();
    }

    public void showLottieButton(boolean show) {
        if (browserFragment != null)
            browserFragment.readyDownload(show);
    }

    private void showNativeAd() {
        if (nativeAdsLayout != null)
            nativeAdsLayout.removeAllViews();

        if (getActivity() == null)
            return;

        onLoadAdmobAdvanced();
    }

    private void onFANAdLoad() {
        if (getActivity() == null)
            return;

        fanNativeAd = new NativeAd(getActivity(), Common.FAN_MAIN_NATIVE);
        fanNativeAd.setAdListener(new com.facebook.ads.AdListener() {

            @Override
            public void onError(Ad ad, AdError error) {
                if (fanNativeAd != null)
                    fanNativeAd.destroy();

                if (nativeAdsLayout != null)
                    nativeAdsLayout.removeAllViews();

                if (getActivity() == null)
                    return;
            }

            @Override
            public void onAdLoaded(Ad ad) {

                if (fanNativeAd.isAdLoaded()) {
                    fanNativeAd.unregisterView();
                }

                // Add the Ad view into the ad container.
                if (getActivity() == null || nativeAdsLayout == null)
                    return;

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.fan_native_main, nativeAdsLayout, false);
                nativeAdsLayout.addView(adView);

                // Create native UI using the ad metadata.
                ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
                TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
                MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
                //TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
                Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);

                // Set the Text.
                nativeAdTitle.setText(fanNativeAd.getAdTitle());
                //nativeAdBody.setText(nativeAd.getAdBody());
                nativeAdCallToAction.setText(fanNativeAd.getAdCallToAction());

                // Download and display the ad icon.
                NativeAd.Image adIcon = fanNativeAd.getAdIcon();
                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);

                // Download and display the cover image.
                nativeAdMedia.setNativeAd(fanNativeAd);

                // Add the AdChoices icon
                LinearLayout adChoicesContainer = fv(R.id.ad_choices_container);
                AdChoicesView adChoicesView = new AdChoicesView(getActivity(), fanNativeAd, true);
                adChoicesContainer.addView(adChoicesView);

                // Register the Title and CTA button to listen for clicks.
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(nativeAdTitle);
                clickableViews.add(nativeAdCallToAction);
                fanNativeAd.registerViewForInteraction(nativeAdsLayout, clickableViews);
            }


            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        if (fanNativeAd != null)
            fanNativeAd.loadAd(NativeAd.MediaCacheFlag.ALL);
    }

    private void onLoadAdmobAdvanced() {
        AdLoader adLoader = new AdLoader.Builder(getActivity(), LogUtil.DEBUG_MODE ? Common.ADMOB_MAIN_NATIVE_ADVANCED_TEST : Common.ADMOB_MAIN_NATIVE_ADVANCED)
                .forAppInstallAd(nativeAppInstallAd -> {
                    if (nativeAppInstallAd == null || getActivity() == null || getActivity().getLayoutInflater() == null)
                        return;
                    NativeAppInstallAdView adView = (NativeAppInstallAdView) getActivity().getLayoutInflater().inflate(R.layout.admob_app_install_main, null);
                    populateAppInstallAdView(nativeAppInstallAd, adView);
                    if (nativeAdsLayout != null) {
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .forContentAd(nativeContentAd -> {
                    if (getActivity() == null || nativeContentAd == null || getActivity().getLayoutInflater() == null)
                        return;

                    NativeContentAdView adView = (NativeContentAdView) getActivity().getLayoutInflater()
                            .inflate(R.layout.admob_content_main, null);
                    populateContentAdView(nativeContentAd, adView);
                    if (nativeAdsLayout != null) {
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        if (nativeAdsLayout != null)
                            nativeAdsLayout.removeAllViews();

                        onFANAdLoad();
//                        super.onAdFailedToLoad(i);
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder().build())
                .build();

        if (adLoader != null)
            adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAppInstallAd.getVideoController();

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
//        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//            public void onVideoEnd() {
//                // Publishers should allow native ads to complete video playback before refreshing
//                // or replacing them with another ad in the same UI location.
//                mRefresh.setEnabled(true);
//                mVideoStatus.setText("Video status: Video playback has ended.");
//                super.onVideoEnd();
//            }
//        });

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        if (adView.getCallToActionView() != null && nativeAppInstallAd.getCallToAction() != null)
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
//        ((TextView) adView.getAdvertiserView()).setText(nativeContentAd.getAdvertiser());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.
        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    // Youtube 지원 안됨 알림
    private void showYoutubeAlert() {

        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle(r.s(R.string.warning));
            dlg.setMessage(r.s(R.string.alert_youtube));
            dlg.setPositiveLabel(r.s(R.string.ok));
            sdf(dlg);
        }

    }

    // 지원되지 않는 사이트 알림
    private void showWarningAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle(R.string.warning);
        alert.setMessage(R.string.alert_wanning);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }
        );
        alert.show();

    }

    private void launchApp(String packageName) {
        try {
            if (getPackageList(packageName)) {
                Intent intent = getActivity().getPackageManager()
                        .getLaunchIntentForPackage(packageName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    private boolean getPackageList(String packageName) {
        boolean isExist = false;

        PackageManager pkgMgr = getActivity().getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith(packageName)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    private boolean checkBookmarkLimit() {
        return db.getBookmarkItems() == null || db.getBookmarkItems().size() < Common.MAX_BOOKMARK_COUNT;
    }

    private void moveItem(int oldPos, int newPos) {
        BookmarkItem item = bookmarkList.get(oldPos);
        bookmarkList.remove(oldPos);
        bookmarkList.add(newPos, item);
        bookmarkAdapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < bookmarkList.size(); i++) {
            item = bookmarkList.get(i);
            item.setBookmarkOrder(bookmarkList.size() - i);
            db.updateBookmarkOrder(item);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return bookmarkAdapter.isDeleteMode();
            }
        };

        return simpleCallback;
    }

    private void implementSwipeDismiss() {
        SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END);//Swipe direction i.e any direction, here you can put any direction LEFT or RIGHT
        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view) {
                premiumCardView.setVisibility(View.GONE);
            }

            @Override
            public void onDragStateChanged(int state) {

            }
        });
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) premiumCardView.getLayoutParams();

        layoutParams.setBehavior(swipeDismissBehavior);//set swipe behaviour to Coordinator layout
    }

    private void premiumBannerClose() {
        sp.setDeletePremiumBannerTime(System.currentTimeMillis());
//        sp.setShowPremiumBanner(false);
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                premiumCardView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        premiumCardView.startAnimation(anim);
    }

    public static interface FrontFragmentListener {
        public void onShowToastMessage(String message);

        public void onShowDownloadDialog(boolean isBrowser, String site, String url);

        public void setEditMode(boolean edit);

        public void onSendReportUrl(ReportItem item);

        public void onGiftAdsButtonClicked();

        public void onStartPremiumMode();

        public void onReadyButtonClicked();

        public void onReadyListClear();
    }

    class AddBookmarkItem extends AsyncTask<BookmarkItem, Void, BookmarkItem> {
        @Override
        protected BookmarkItem doInBackground(BookmarkItem... params) {
            BookmarkItem item = params[0];
            String path = item.getBookmarkUrl();
            Bitmap bitmap = null;
            if (!TextUtils.isEmpty((path))) {
                String host = Uri.parse(path).getHost();
                String iconUrl = host + "/favicon.ico";
                if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                    iconUrl = "http://" + iconUrl;

                bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);
                if (TextUtils.isEmpty(item.getBookmarkName()))
                    item.setBookmarkName(host);
            }
            if (bitmap != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));

            return item;
        }

        @Override
        protected void onPostExecute(BookmarkItem item) {
            db.insertOrUpdateBookmarkItem(item);
            refreshMainList();
            if (listener != null)
                listener.onShowToastMessage(r.s(R.string.msg_toast_bookmarked));
        }
    }
}
