package com.ne.hdv;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ne.hdv.adapter.DownloadAdapter;
import com.ne.hdv.base.BaseVideoActivity;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.data.DownItem;

import java.io.File;
import java.util.ArrayList;

public class PlayerActivity extends BaseVideoActivity {
    private static final int CONTROL_VOLUME = 0;
    private static final int CONTROL_BRIGHTNESS = 1;
    private static final int CONTROL_SEEK = 2;

    // Saved instance state keys.
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private static final String KEY_VIDEO_URL = "video_url";
    private static final String KEY_VIDEO_ID = "video_id";
    private static final String KEY_VIDEO_TITLE = "video_title";

    SimpleExoPlayer player;
    SimpleExoPlayerView playerView;
    String id, title, url;
    Handler hideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            hideControlVisible();
        }
    };

    Handler controlHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (playbackLayout.getTag() != "IN_ANIMATION") {
                playbackLayout.setTag("IN_ANIMATION");
                playbackLayout.animate().alpha(0F).setDuration(450L).withEndAction(() -> {
                    playbackLayout.setVisibility(View.GONE);
                    playbackLayout.setTag("");
                }).start();
            }
        }
    };

    private MediaSource mediaSource;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;

    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    private ImageButton backButton;
    private LinearLayout titleLayout, volumeLayout, brightnessLayout, messageLayout;
    private TextView titleText, controlText, messageText;
    private ProgressBar volumeProgress, brightnessProgress;
    private ImageView volumeImage, brightnessImage;
    LinearLayout playbackLayout;

    RecyclerView lv;
    LinearLayoutManager manager;
    DownloadAdapter adapter;
    ArrayList<DownItem> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        playerView = fv(R.id.layout_playerview);


        if (savedInstanceState != null) {
            id = savedInstanceState.getString(KEY_VIDEO_ID);
            title = savedInstanceState.getString(KEY_VIDEO_TITLE);
            url = savedInstanceState.getString(KEY_VIDEO_URL);

            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            id = getIntent().getStringExtra(Common.INTENT_VIDEO_ID);
            title = getIntent().getStringExtra(Common.INTENT_VIDEO_TITLE);
            url = getIntent().getStringExtra(Common.INTENT_VIDEO_URL);
            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
            clearStartPosition();
        }
    }

    @Override
    public void onBackPressed() {
        releasePlayer();
        super.onBackPressed();
    }

    private void initializePlayer() {
//      1. Create a default TrackSelector
        final BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
//                new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        RenderersFactory renderersFactory = new DefaultRenderersFactory(this);
        trackSelector =
                new DefaultTrackSelector();
        trackSelector.setParameters(trackSelectorParameters);
        // 2. Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();
        // 3. Create the player
        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        player.setPlayWhenReady(startAutoPlay);
        playerView.setPlayer(player);
        playerView.setKeepScreenOn(true);
        playerView.setControllerVisibilityListener(visibility -> {
            LinearLayout layout = fv(R.id.layout_playback_control);
            if (layout.getTag() != "IN_ANIMATION") {
                switch (visibility) {
                    case View.GONE:
                        layout.setTag("IN_ANIMATION");
                            playerView.showController();
                            layout.animate().alpha(0F).setDuration(450L).withEndAction(() -> {
                                playerView.hideController();
                                layout.setTag("");
                            }).start();

                        break;

                    case View.VISIBLE:
                        hideControlVisible();
                        layout.animate().alpha(1F).setDuration(450L).start();
                        break;
                }
            }
        });
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                com.google.android.exoplayer2.util.Util.getUserAgent(this, "ExoPlayer"));

        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        Uri uri = Uri.parse(url); //Uri.fromFile(new File(url));
        // This is the MediaSource representing the media to be played.
        mediaSource = new ExtractorMediaSource(uri, //Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"),
                dataSourceFactory, extractorsFactory, null, null);
//        mediaSource = buildMediaSource(uri);
        // Prepare the player with the source.
        player.addListener(new Player.DefaultEventListener() {
            @Override
            public void onLoadingChanged(boolean isLoading) {
                super.onLoadingChanged(isLoading);
            }
        });

        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition)
            player.seekTo(startWindow, startPosition);
        player.prepare(mediaSource, !haveStartPosition, false);
        playerView.requestFocus();
        playerView.setControllerShowTimeoutMs(isLandscape() ? 3000 : 0);
        playerView.setControllerHideOnTouch(isLandscape());
        player.setPlayWhenReady(true);

        initView();
        setControl(this, player, !isLandscape());
        if (isLandscape()) {
            playerView.hideController();
        }
        else
            playbackLayout.setVisibility(View.GONE);
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(v -> {
            onBackPressed();
        });

        titleLayout = fv(R.id.layout_title);
        titleText = fv(R.id.text_title);
        titleText.setText(title);

        volumeLayout = fv(R.id.layout_volume);
        volumeProgress = fv(R.id.progress_volume);
        volumeProgress.setMax(15);
        volumeImage = fv(R.id.image_volume);

        brightnessLayout = fv(R.id.layout_brightness);
        brightnessProgress = fv(R.id.progress_brightness);
        brightnessProgress.setMax(15);
        brightnessImage = fv(R.id.image_brightness);

        messageLayout = fv(R.id.layout_message);
        controlText = fv(R.id.text_message_control);
        messageText = fv(R.id.text_message_per);
        hideControlVisible();

        playbackLayout = fv(R.id.layout_playback);

//        playbackButton = fv(R.id.button_playback);
//        playbackButton.setOnClickListener(v -> {
//            if (player != null) {
//                player.setPlayWhenReady(!player.getPlayWhenReady());
//                playbackButton.setImageResource(player.getPlayWhenReady() ? R.drawable.ic_pause_white_36 : R.drawable.ic_play_arrow_white_36);
//            }
//        });

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        ArrayList<DownItem> temp = new ArrayList<>();
        temp.addAll(db.getAllCDownloadItems());
        for (DownItem item : temp) {
            if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DONE) {
                File file = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (!file.exists())
                    db.deleteDownloadItem(item);
                else {
                    if (item.getDownloadFileTotalSize() <= 0) {
                        item.setDownloadFileTotalSize(file.length());
                        db.insertOrUpdateDownloadItem(item);
                    }
                    list.add(item);
                }
            }
        }
        manager = new GridLayoutManager(this, 1);
        lv.setLayoutManager(manager);
        adapter = new DownloadAdapter(this, R.layout.item_video_list, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownItem item) {
            }

            @Override
            public void onDeleteButtonClick(int position, DownItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownItem item) {

            }

            @Override
            public void OnItemClick(int position, DownItem item) {
                if (!item.getDownloadId().equalsIgnoreCase(id)) {
                    id = item.getDownloadId();
                    String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
                    Uri uri = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        uri = FileProvider.getUriForFile(PlayerActivity.this, "com.ne.hdv.fileprovider", new File(path));
                    else
                        uri = Uri.fromFile(new File(path));
                    url = uri.toString();
                    title = item.getDownloadFileName();
                    titleText.setText(title);
                    adapter.setEditIndex(position);

                    // Produces DataSource instances through which media data is loaded.
                    DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(PlayerActivity.this,
                            com.google.android.exoplayer2.util.Util.getUserAgent(PlayerActivity.this, "ExoPlayer"));

                    // Produces Extractor instances for parsing the media data.
                    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                    Uri temp = Uri.parse(url); //Uri.fromFile(new File(url));
                    // This is the MediaSource representing the media to be played.
                    mediaSource = new ExtractorMediaSource(temp, //Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"),
                            dataSourceFactory, extractorsFactory, null, null);

                    player.prepare(mediaSource);
                    player.setPlayWhenReady(true);
                }

            }

            @Override
            public void OnItemLongClick(int position, DownItem item) {
            }
        });
        adapter.setVideoMode(true);
        lv.setAdapter(adapter);
        adapter.setEditId(id);
        lv.scrollToPosition(adapter.getEcitIndex());
    }

    private void releasePlayer() {
        if (player != null) {
            updateTrackSelectorParameters();
            updateStartPosition();
            player.release();
            player = null;
            mediaSource = null;
            trackSelector = null;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            case MotionEvent.ACTION_UP:
                hideHandler.sendEmptyMessageDelayed(100, 2000);
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23)
            initializePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        } else {
            if (player != null)
                player.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        updateStartPosition();
        outState.putString(KEY_VIDEO_ID, id);
        outState.putString(KEY_VIDEO_URL, url);
        outState.putString(KEY_VIDEO_TITLE, title);

        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_WINDOW, startWindow);
        outState.putLong(KEY_POSITION, startPosition);
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }

    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            startPosition = Math.max(0, player.getContentPosition());
        }
    }

    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    @Override
    public void clicked() {
        super.clicked();

        controlHideHandler.removeCallbacksAndMessages(null);
        playbackLayout.setTag("");
        if (playbackLayout.getVisibility() == View.VISIBLE) {
            playbackLayout.setVisibility(View.GONE);
        }
        else {
            playbackLayout.setVisibility(View.VISIBLE);
            playbackLayout.animate().alpha(1F).setDuration(450L).start();
            controlHideHandler.sendEmptyMessageDelayed(100, 3000);
        }
    }

    @Override
    public void commonVolume(float distance) {
        super.commonVolume(distance);

        int vol = getVolumePer();
        if (vol > -2) {
            if (vol <= 0) {
                vol = 0;
            } else if (vol > 15)
                vol = 15;

            controlVisible(CONTROL_VOLUME);
            volumeProgress.setProgress(vol);
            messageText.setText(vol + "");
            volumeImage.setImageResource(volumeProgress.getProgress() == 0 ? R.drawable.ic_volume_off_white_18 : R.drawable.ic_volume_up_white_18);
        }
        hideHandler.sendEmptyMessageDelayed(0, 1000);
    }

    @Override
    public void commonVolume(boolean up) {
        super.commonVolume(up);

        controlVisible(CONTROL_VOLUME);
        int vol = getVolumePer();
        if (vol > -2) {
            if (vol <= 0) {
                vol = 0;
            } else if (vol > 15)
                vol = 15;

            volumeProgress.setProgress(vol);
            messageText.setText(vol + "");
            volumeImage.setImageResource(volumeProgress.getProgress() == 0 ? R.drawable.ic_volume_off_white_18 : R.drawable.ic_volume_up_white_18);
        }
        hideHandler.sendEmptyMessageDelayed(0, 1000);
    }

    @Override
    public void commonBrightness(float distance) {
        super.commonBrightness(distance);

        controlVisible(CONTROL_BRIGHTNESS);
        if ((int) ((getWindow().getAttributes().screenBrightness + distance) * 15) > 15) {
            brightnessProgress.setProgress(15);
            brightnessImage.setImageResource(R.drawable.ic_brightness_white_18);
            messageText.setText("10");
        } else if ((int) ((getWindow().getAttributes().screenBrightness + distance) * 15) < 0) {
            brightnessProgress.setProgress(0);
            brightnessImage.setImageResource(R.drawable.ic_brightness_low_white_18);
            messageText.setText("0");
        } else {
            brightnessProgress.setProgress((int) ((getWindow().getAttributes().screenBrightness + distance) * 15));
            int bright = brightnessProgress.getProgress();

            messageText.setText(bright + "");
            brightnessImage.setImageResource(bright == 0 ? R.drawable.ic_brightness_low_white_18 : R.drawable.ic_brightness_white_18);
        }
        hideHandler.sendEmptyMessageDelayed(0, 1000);
    }

    @Override
    public void seekCommon(float distance) {
        super.seekCommon(distance);

        int seek = (int) getSeekDistance();
        if (player.getCurrentPosition() + (int) (seek) >= 0 && player.getCurrentPosition() + (int) (seek) < player.getDuration() + 10) {
            controlVisible(CONTROL_SEEK);
            player.seekTo(player.getCurrentPosition() + (int) (distance * 60000));
            if (seek > 0)
                messageText.setText("+" + convertSecondsToHMmSs((long) getSeekDistance() / 1000) + "(" + convertSecondsToHMmSs(player.getCurrentPosition() / 1000) + ")");
            else
                messageText.setText("-" + convertSecondsToHMmSs((long) getSeekDistance() / 1000) + "(" + convertSecondsToHMmSs(player.getCurrentPosition() / 1000) + ")");
            hideHandler.sendEmptyMessageDelayed(0, 300);
        }

    }

    private void controlVisible(int control) {
        if (isLandscape())
            playerView.hideController();
        else
            playbackLayout.setVisibility(View.GONE);
        hideHandler.removeCallbacksAndMessages(null);
        brightnessLayout.setVisibility(control == CONTROL_BRIGHTNESS ? View.VISIBLE : View.GONE);
        volumeLayout.setVisibility(control == CONTROL_VOLUME && isLandscape() ? View.VISIBLE : View.GONE);
        messageLayout.setVisibility(View.VISIBLE);

        String ctrl = r.s(R.string.video_control_brightness);
        if (control == CONTROL_VOLUME)
            ctrl = r.s(R.string.video_control_volume);
        else if (control == CONTROL_SEEK)
            ctrl = r.s(R.string.video_control_seek);
        controlText.setText(ctrl);
    }

    private void hideControlVisible() {
        brightnessLayout.setVisibility(View.GONE);
        volumeLayout.setVisibility(View.GONE);
        messageLayout.setVisibility(View.GONE);
    }

    private String convertSecondsToHMmSs(long seconds) {
        long s = Math.abs(seconds) % 60;
        long m = (Math.abs(seconds) / 60) % 60;
        long h = (Math.abs(seconds) / (60 * 60)) % 24;
        if (h > 0)
            return String.format("%d:%02d:%02d", h, m, s);
        else
            return String.format("%d:%02d", m, s);
    }

    private boolean isLandscape() {
        int orientation = getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }
}
