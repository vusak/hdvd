package com.ne.hdv.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ne.hdv.R;

import java.util.ArrayList;

public class FeaturedAppAdapter extends RecyclerView.Adapter<FeaturedAppAdapter.ViewHolder> {

    public static String FEATURED_APP_JMFM = "JMFM";
    public static String FEATURED_APP_INSTANT = "INSTANT";
    public static String FEATURED_APP_JMBR = "JMBR";
    public static String FEATURED_APP_WEATHER = "JMWEATHER";
    //    public static String FEATURED_APP_VFF = "VFF";
    public static String FEATURED_APP_MOBVISTA = "MOBVISTA";
    public static String FEATURED_APP_JMOBILE = "JMOBILE";
    private Context context;
    private ArrayList<String> items;
    private int layoutId;
    private FeaturedAppAdapterListener listener;

    public FeaturedAppAdapter(Context context, int layoutId, ArrayList<String> items, FeaturedAppAdapterListener listener) {
        this.context = context;
        this.items = items;
        this.layoutId = layoutId;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String app = items.get(position);

        String sTitle = context.getString(R.string.main_title_featured_app_mobvista);
        Drawable dIcon = ContextCompat.getDrawable(context, R.drawable.ic_hdfm_app_50);
        if (app.equalsIgnoreCase(FEATURED_APP_JMFM)) {
            sTitle = context.getString(R.string.main_title_featured_app_jmfm);
            dIcon = ContextCompat.getDrawable(context, R.drawable.ic_hdfm_app_50);
        } else if (app.equalsIgnoreCase(FEATURED_APP_JMOBILE)) {
            sTitle = context.getString(R.string.main_title_featured_app_jmobile);
            dIcon = ContextCompat.getDrawable(context, R.drawable.icon_jmobile_50);
        } else if (app.equalsIgnoreCase(FEATURED_APP_INSTANT)) {
            sTitle = context.getString(R.string.main_title_featured_app_instant);
            dIcon = ContextCompat.getDrawable(context, R.drawable.ic_instant_app_v_2_50);
        }
//        else if (app.equalsIgnoreCase(FEATURED_APP_JMBR)) {
//            sTitle = context.getString(R.string.main_title_featured_app_jmbr);
//            dIcon = ContextCompat.getDrawable(context, R.drawable.ic_jmbr_app_50);
//        } else if (app.equalsIgnoreCase(FEATURED_APP_WEATHER)) {
//            sTitle = context.getString(R.string.main_title_featured_app_weather);
//            dIcon = ContextCompat.getDrawable(context, R.drawable.ic_jmweather_76);
//        } else if (app.equalsIgnoreCase(FEATURED_APP_MOBVISTA)) {
//            sTitle = context.getString(R.string.main_title_featured_app_mobvista);
//            dIcon = ContextCompat.getDrawable(context, R.drawable.icon_bottom_gift_red_50);
//            int ranValue = (int) (Math.random() * 100 + 1);
//            if (ranValue < 30) {
//                dIcon = ContextCompat.getDrawable(context, R.drawable.icon_bottom_gift_yellow_50);
//            } else if (ranValue > 70) {
//                dIcon = ContextCompat.getDrawable(context, R.drawable.icon_bottom_gift_mint_50);
//            }
//        }

        holder.title.setText(sTitle);
        holder.icon.setImageDrawable(dIcon);
        holder.setOnCellClickListener(new ViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ViewHolder holder) {
                if (listener != null) {
                    listener.onItemClick(position, items.get(position));
                }
            }
        });
    }

    public static interface FeaturedAppAdapterListener extends ReAbstractViewHolder.OnItemViewClickListener {
        public void onItemClick(int position, String app);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public OnItemViewClickListener listener;
        ImageView icon;
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.image_icon);
            title = (TextView) itemView.findViewById(R.id.text_title);
        }

        public void setOnCellClickListener(OnItemViewClickListener listener) {
            this.listener = listener;
            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHolder.this.listener.onItemViewClick(getPosition(), ViewHolder.this);
                    }
                });
            }
        }


        public interface OnItemViewClickListener {
            public void onItemViewClick(int position, ViewHolder holder);
        }
    }
}
