package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;


public class FavoriteItem extends TableObject {
    public static final String TABLE_NAME = "favorit";

    public static final String ROW_ID = "_id";
    public static final String FAVORITE_URL = "favorite_url";
    public static final String FAVORITE_SITE = "favorite_site";
    public static final String FAVORITE_TITLE = "favorite_title";
    public static final String FAVORITE_ICON = "favorite_icon";
    public static final String FAVORITE_COUNT = "favorite_count";
    public static final String FAVORITE_AT = "favorite_at";
    public static final Creator<FavoriteItem> CREATOR = new Creator<FavoriteItem>() {
        @Override
        public FavoriteItem createFromParcel(Parcel source) {
            return new FavoriteItem(source);
        }

        @Override
        public FavoriteItem[] newArray(int size) {
            return new FavoriteItem[size];
        }
    };
    private long id;
    private String url;
    private String site;
    private String title;
    private byte[] icon;
    private int count = 0;
    private long favoriteAt;

    public FavoriteItem() {
        super();
    }

    public FavoriteItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.url = in.readString();
        this.site = in.readString();
        this.title = in.readString();
        in.readByteArray(this.icon);
        this.count = in.readInt();
        this.favoriteAt = in.readLong();
    }

    public static void createTableFavorite(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(FAVORITE_URL + " text not null unique, ")
                .append(FAVORITE_SITE + " text, ")
                .append(FAVORITE_TITLE + " text, ")
                .append(FAVORITE_ICON + " BLOB, ")
                .append(FAVORITE_COUNT + " integer not null, ")
                .append(FAVORITE_AT + " integer not null) ")
                .toString());
    }

    public static FavoriteItem getFavoriteItem(SQLiteDatabase db, String url) {
        List<FavoriteItem> list = getFavoriteItems(db, null, FAVORITE_URL + " = ?", new String[]{url}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<FavoriteItem> getFavoriteItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<FavoriteItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                FavoriteItem item = new FavoriteItem()
                        .setRowId(c)
                        .setFavoriteUrl(c)
                        .setFavoriteSite(c)
                        .setFavoriteTitle(c)
                        .setFavoriteIcon(c)
                        .setFavoriteCount(c)
                        .setFavoriteAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateFavoriteItem(SQLiteDatabase db, FavoriteItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putFavoriteUrl(v)
                    .putFavoriteSite(v)
                    .putFavoriteTitle(v)
                    .putFavoriteIcon(v)
                    .putFavoriteCount(v)
                    .putFavoriteAt(v);

            int rowAffected = db.update(TABLE_NAME, v, FAVORITE_URL + " =? ", new String[]{item.getFavoriteUrl()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteFavoriteItem(SQLiteDatabase db, FavoriteItem item) {
        try {
            db.delete(TABLE_NAME, FAVORITE_URL + " =? ", new String[]{item.getFavoriteUrl()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllFavoriteItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(url);
        out.writeString(site);
        out.writeString(title);
        out.writeByteArray(icon);
        out.writeInt(count);
        out.writeLong(favoriteAt);
    }

    public long getRowId() {
        return this.id;
    }

    public FavoriteItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public FavoriteItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public FavoriteItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getFavoriteUrl() {
        return this.url;
    }

    public FavoriteItem setFavoriteUrl(String url) {
        this.url = url;

        return this;
    }

    public FavoriteItem setFavoriteUrl(Cursor c) {
        this.url = s(c, FAVORITE_URL);

        return this;
    }

    public FavoriteItem putFavoriteUrl(ContentValues v) {
        v.put(FAVORITE_URL, url);

        return this;
    }

    public String getFavoriteSite() {
        return this.site;
    }

    public FavoriteItem setFavoriteSite(String site) {
        this.site = site;

        return this;
    }

    public FavoriteItem setFavoriteSite(Cursor c) {
        this.site = s(c, FAVORITE_SITE);

        return this;
    }

    public FavoriteItem putFavoriteSite(ContentValues v) {
        v.put(FAVORITE_SITE, site);

        return this;
    }

    public String getFavoriteTitle() {
        return this.title;
    }

    public FavoriteItem setFavoriteTitle(String title) {
        this.title = title;

        return this;
    }

    public FavoriteItem setFavoriteTitle(Cursor c) {
        this.title = s(c, FAVORITE_TITLE);

        return this;
    }

    public FavoriteItem putFavoriteTitle(ContentValues v) {
        v.put(FAVORITE_TITLE, title);

        return this;
    }

    public long getFavoriteAt() {
        return this.favoriteAt;
    }

    public FavoriteItem setFavoriteAt(long at) {
        this.favoriteAt = at;

        return this;
    }

    public FavoriteItem setFavoriteAt(Cursor c) {
        this.favoriteAt = l(c, FAVORITE_AT);

        return this;
    }

    public FavoriteItem putFavoriteAt(ContentValues v) {
        v.put(FAVORITE_AT, favoriteAt);

        return this;
    }

    public byte[] getFavoriteIcon() {
        return this.icon;
    }

    public FavoriteItem setFavoriteIcon(byte[] icon) {
        this.icon = icon;

        return this;
    }

    public FavoriteItem setFavoriteIcon(Cursor c) {
        this.icon = blob(c, FAVORITE_ICON);

        return this;
    }

    public FavoriteItem putFavoriteIcon(ContentValues v) {
        v.put(FAVORITE_ICON, icon);

        return this;
    }

    public int getFavoriteCount() {
        return this.count;
    }

    public FavoriteItem setFavoriteCount(int count) {
        this.count = count;

        return this;
    }

    public FavoriteItem setFavoriteCount(Cursor c) {
        this.count = i(c, FAVORITE_COUNT);

        return this;
    }

    public FavoriteItem putFavoriteCount(ContentValues v) {
        v.put(FAVORITE_COUNT, count);

        return this;
    }
}
