package com.ne.hdv.image;

import android.content.Context;

import java.io.File;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        cacheDir = new File(android.os.Environment.getExternalStorageDirectory() + "/Android/data/com.ne.hdv/.cache/");

        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }

    public File getFile(String url) {

        String filename = String.valueOf(url.hashCode());

        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);

        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }

}