package com.ne.hdv.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.adapter.UrlAdapter;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.FavoriteItem;

import java.util.ArrayList;

public class URLEditFragment extends BaseFragment {

    LinearLayout urlLayout, textUrlLayout;
    EditText urlEdit;
    ImageButton clearButton;
    TextView urlText;
    ListView urlLv;
    UrlAdapter urlAdapter;
    ArrayList<String> urlList, favList;
    ImageView goImage, searchImage;

    private URLEditFragmentListener listener;
    private Handler urlSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                urlList.clear();
                String keyword = urlEdit.getText().toString();
                for (String temp : favList) {
                    if (temp.contains(keyword))
                        urlList.add(temp);
                }
                if (urlList == null)
                    urlList = new ArrayList<>();
                urlAdapter.setItems(urlList, keyword);
            }
        }
    };

    @Override
    public void onCreateView(Bundle savedInstanceState) {


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_url;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initView() {
        urlLayout = fv(R.id.layout_edit_url);
        urlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(urlEdit);
            }
        });

        searchImage = fv(R.id.image_search);
        searchImage.setVisibility(View.VISIBLE);

        clearButton = fv(R.id.button_close);
        clearButton.setVisibility(View.GONE);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlEdit.setText("");
            }
        });

        textUrlLayout = fv(R.id.layout_url);
        textUrlLayout.setVisibility(View.GONE);
        textUrlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.onGotoUrlButtonClick(urlText.getText().toString().trim());
            }
        });

        urlText = fv(R.id.text_url);
        urlText.setVisibility(View.GONE);

        goImage = fv(R.id.image_go);
        goImage.setVisibility(View.GONE);

        urlEdit = fv(R.id.edit);
        urlEdit.setHint(r.s(R.string.edt_search));
        urlEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    urlEdit.selectAll();
            }
        });

        urlEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                urlText.setText(s.toString());
                urlSearchHandler.removeCallbacksAndMessages(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                clearButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlText.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlLv.setVisibility(empty ? View.GONE : View.VISIBLE);
                goImage.setVisibility(empty ? View.GONE : View.VISIBLE);
                textUrlLayout.setVisibility(empty ? View.GONE : View.VISIBLE);
                searchImage.setVisibility(empty ? View.VISIBLE : View.GONE);
                if (!empty)
                    urlSearchHandler.sendEmptyMessage(0);
            }
        });

        urlEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    Util.hideKeyBoard(urlEdit);
                    if (listener != null)
                        listener.onGotoUrlButtonClick(urlEdit.getText().toString().trim());
                }
                return false;
            }
        });

        urlLv = fv(R.id.list_url);
        urlLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (urlList == null || urlList.size() < position)
                    return;

                String url = urlList.get(position);
                urlEdit.setText(url);
                Util.hideKeyBoard(urlEdit);
                if (listener != null)
                    listener.onGotoUrlButtonClick(url);
            }
        });

        favList = new ArrayList<>();
        urlList = new ArrayList<>();
        urlAdapter = new UrlAdapter(getActivity(), urlList);
        urlLv.setAdapter(urlAdapter);

        refershList("");
    }

    public void setListener(URLEditFragmentListener listener) {
        this.listener = listener;
    }

    public void showKeypad() {
        if (urlEdit != null)
            Util.showKeyBoard(urlEdit);
    }

    public void refershList(String url) {
        if (urlEdit == null || url == null)
            return;

        favList.clear();

        urlEdit.setText(url);
        urlEdit.setSelection(0, url.length());
//        Util.showKeyBoard(urlEdit);

        favList.clear();
        ArrayList<FavoriteItem> fList = new ArrayList<>();
        fList.addAll(db.getFavoriteItems());
        for (FavoriteItem temp : fList) {
            favList.add(temp.getFavoriteSite());
        }
    }

    public static interface URLEditFragmentListener {
        public void onGotoUrlButtonClick(String url);
    }
}
