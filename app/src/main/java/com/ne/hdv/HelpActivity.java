package com.ne.hdv;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ne.hdv.base.BaseActivity;
import com.ne.hdv.common.Util;

public class HelpActivity extends BaseActivity {

    ImageButton nextButton;
    Button goButton;

    int dotCount;
    ImageView[] dots;
    LinearLayout indicatorLayout;

    ViewPager pager;
    HelpPagerAdapter adapter;

    private int[] helpResources = {
//            R.drawable.img_how_to_use_2,
//            R.drawable.img_how_to_use_3,
//            R.drawable.img_how_to_use_4
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_use);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.download_no_Image_bg));
        }

        initView();
    }

    private void initView() {
        nextButton = fv(R.id.button_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pager != null)
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        });

        goButton = fv(R.id.button_go);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        goButton.setVisibility(View.GONE);

        indicatorLayout = fv(R.id.layout_dot);

        pager = fv(R.id.viewpager_help);
        adapter = new HelpPagerAdapter(this, helpResources);
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotCount; i++) {
                    if (i == position) {
                        if (position + 1 == dotCount)
                            dots[i].setImageDrawable(ContextCompat.getDrawable(HelpActivity.this, R.drawable.radio_circle_blue));
                        else
                            dots[i].setImageDrawable(ContextCompat.getDrawable(HelpActivity.this, R.drawable.radio_circle_red));
                    } else
                        dots[i].setImageDrawable(ContextCompat.getDrawable(HelpActivity.this, R.drawable.radio_circle_gray));
                }
                nextButton.setVisibility(position + 1 == dotCount ? View.GONE : View.VISIBLE);
                goButton.setVisibility(position + 1 == dotCount ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setCurrentItem(0);
        setDotImage();
    }

    private void setDotImage() {
        dotCount = adapter.getCount();
        dots = new ImageView[dotCount];

        for (int i = 0; i < dotCount; i++) {
            dots[i] = new ImageView(this);
            if (i == 0)
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.radio_circle_red));
            else
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.radio_circle_gray));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins((int) Util.convertDpToPixel(5, this), 0, (int) Util.convertDpToPixel(5, this), 0);

            indicatorLayout.addView(dots[i], params);
        }

    }

    private class HelpPagerAdapter extends PagerAdapter {
        private Context context;
        private int[] helpResources;

        public HelpPagerAdapter(Context context, int[] helpRes) {
            this.context = context;
            this.helpResources = helpRes;
        }

        @Override
        public int getCount() {
            return helpResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View item = LayoutInflater.from(context).inflate(R.layout.item_help, container, false);

            ImageView iv = (ImageView) item.findViewById(R.id.iv_help);
            iv.setImageResource(helpResources[position]);

            container.addView(item);

            return item;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}
