package com.ne.hdv;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ne.hdv.base.BaseActivity;
import com.ne.hdv.common.Common;
import com.ne.hdv.download.Api;

public class LearnMoreActivity extends BaseActivity {

    Api api;
    TextView contentText, subText;
    ImageButton backButton;
    boolean isFullMode = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_more);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(r.c(this, R.color.main_status));
        }


        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
        if (getIntent() != null && getIntent().getData() != null) {
            Uri uri = getIntent().getData();
            String what = uri.getQueryParameter("what");
            if (what.indexOf("trend") >= 0)
                api.getTrendLearnMoreData();
            else
                api.getReportLearnMoreData();
        } else {
            isFullMode = true;
            api.getTrendLearnMoreData();
            api.getReportLearnMoreData();
        }
    }

    private void initView() {

        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        contentText = fv(R.id.text_content);
        contentText.setMovementMethod(LinkMovementMethod.getInstance());
        subText = fv(R.id.text_content_sub);
        subText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_GET_TREND_LEARNMORE
                || m.what == Common.API_CODE_GET_REPORT_LEARNMORE) {
            if (m.getData() == null)
                return;

            String terms = m.getData().getString("terms");
            if (terms != null && !TextUtils.isEmpty(terms)) {
                SpannableString spanText = new SpannableString(Html.fromHtml(terms));
                if (isFullMode) {
                    if (m.what == Common.API_CODE_GET_REPORT_LEARNMORE)
                        subText.setText(spanText);
                    else
                        contentText.setText(spanText);
                } else
                    contentText.setText(spanText);
            }
        }
        super.handleApiMessage(m);
    }
}
