package com.ne.hdv.download;

import com.ne.hdv.common.FileUtil;
import com.ne.hdv.data.DownItem;

/**
 * Created by Cecilia on 2017-05-16.
 */

public class DownTask extends FileUtil.DownloadTask<DownManager> {
    private static final float PROGRESS_THRESHOLD = 1024 * 500;
    private String id;
    private DownItem item;
    private float prev = 0;

    public DownTask(DownManager manager, String id, DownItem item) {
        super(manager);
        this.id = id;
        this.item = item;
    }

    @Override
    public void onProgress(long progress, long total) {
        if (ref.get() == null)
            return;

//        float downProgress = progress / (float) total;
        if (progress > prev + PROGRESS_THRESHOLD) {
            ref.get().onDownloadProgress(id, item, progress, total);
            prev = progress;
        }
    }

    @Override
    public void onSuccess() {
        if (ref.get() == null)
            return;

        ref.get().onDownloadSuccess(id, item);
    }

    @Override
    public void onError(int resultCode) {
        if (ref.get() == null)
            return;

        ref.get().onDownloadError(id, item, resultCode);
    }

    public String getId() {
        return this.id;
    }

    public DownItem getItem() {
        return this.item;
    }
}
