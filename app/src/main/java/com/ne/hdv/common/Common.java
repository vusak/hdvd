package com.ne.hdv.common;

import android.content.Context;

import com.ne.hdv.data.MyListItem;

import java.util.LinkedList;

public class Common {
    // HEADER_**
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_NO_CACHE = "no-cache";
    public static final String HEADER_APPLICATION_VERSION = "APPLICATION_VERSION";
    public static final String HEADER_OS = "OS";
    public static final String HEADER_DEVICE_IDENTIFIER = "DEVICE_IDENTIFIER";
    public static final String HEADER_ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final int TIMEOUT = 20000;
    public static final int AUTH_DEVICE_CHECK_DURATION = 300000;
    public static final String BASE_URL = "http://jmid.jiran.com/";
    public static final String API_URL_SEND_TREND = "api/trend/set";
    public static final String API_URL_GET_TREND_WORLDWIDE = "json/trend/w.json";
    public static final String API_URL_GET_TREND_LIST = "api/trend/get";
    public static final String API_URL_SEND_TREND_STATUS = "api/trend/set/status";
    public static final String API_URL_SEND_TREND_REPORT = "api/trend/set/confirm";
    public static final String API_URL_SEND_REPORT = "api/report/set";
    public static final String API_URL_GET_TREND_LEARNMORE = "json/trend/learnmore.json";
    public static final String API_URL_GET_REPORT_LEARNMORE = "json/report/learnmore.json";
    public static final String API_URL_SEND_ERROR = "api/error/log";
    public static final int API_CODE_SEND_TREND = 10001;
    public static final int API_CODE_GET_TREND_LIST = 10002;
    public static final int API_CODE_GET_TREND_LIST_WORLDWIDE = 10003;
    public static final int API_CODE_SEND_TREND_STATUS = 10008;
    public static final int API_CODE_SEND_TREND_REPORT = 10009;
    public static final int API_CODE_SEND_REPORT = 10004;
    public static final int API_CODE_GET_TREND_LEARNMORE = 10005;
    public static final int API_CODE_GET_REPORT_LEARNMORE = 10006;
    public static final int API_CODE_SEND_ERROR_LOG = 10007;
    public static final String PARAM_PROGRESS_MESSAGE = "progressMessage";
    public static final String PARAM_SHOW_ERROR_MESSAGE = "showErrorMessage";
    public static final String PARAM_SERVICE_NAME = "sname";
    public static final String PARAM_URL = "url";
    public static final String PARAM_REGION = "region";
    public static final String PARAM_LANGUAGE = "lang";
    public static final String PARAM_TIME = "time";
    public static final String PARAM_SIZE = "size";
    public static final String PARAM_DATE_TYPE = "datetype";
    public static final String PARAM_LIST_COUNT = "listcnt";
    public static final String PARAM_PAGE = "pageurl";
    public static final String PARAM_ERROR_CODE = "errcode";
    public static final String PARAM_SEARCH_URL = "searchurl";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_CONFIRM_TYPE = "confirmtype";
    // RESULT_**
    public static final String RESULT_ERROR_CODE = "errorCode";
    public static final String RESULT_ERROR_DEBUG_MESSAGE = "errorDebugMessage";
    public static final String RESULT_VALIDATION_RESULT_CODE = "validationResultCode";
    public static final String TAG_RESULT = "result";
    public static final String TAG_ERROR = "error";
    public static final String TAG_STATUS = "status";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_LIST = "list";
    public static final String TAG_NUMBER = "no";
    public static final String TAG_URL = "url";
    public static final String TAG_COUNT = "cnt";
    public static final String TAG_TIME = "time";
    public static final String TAG_SIZE = "size";
    public static final String TAG_REGION = "region";
    public static final String TAG_THUMBNAIL_URL = "thumbnail";
    public static final String TAG_LANGUAGE = "language";
    public static final String TAG_PAGE_URL = "pageurl";
    public static final String TAG_TEXT = "text";
    public static final String TAG_BUTTON_REPORT = "button_report";
    public static final String TAG_BUTTON_REPORTED = "button_reported";
    public static final String TAG_BUTTON_DOWNLOAD_BAN = "button_download_ban";

    // RESULT_CODE_**
    public static final int RESULT_CODE_ERROR_OK = 0;
    public static final int RESULT_CODE_ERROR_NO_RESPONSE = -1;
    public static final int RESULT_CODE_ERROR_UNKNOWN = -2;
    public static final int RESULT_CODE_ERROR_CANCELED = -3;
    public static final int RESULT_CODE_VALIDATION_OK = 1001;
    public static final int RESULT_CODE_VALIDATION_LOCKED = 2001;
    public static final int RESULT_CODE_VALIDATION_UPDATE_REQUIRED = 2002;
    public static final int RESULT_CODE_VALIDATION_BAD_ACCESS = 8001;
    public static final int RESULT_CODE_SUCCESS = 200;
    public static final int RESULT_TREND_SET_ERR_LOG = 1;
    public static final int RESULT_TREND_GET_ERR_LOG = 2;
    public static final int RESULT_TREND_JSON_ERR_LOG = 3;
    public static final int RESULT_REPORT_SET_ERR_LOG = 4;
    public static final int RESULT_REPORT_GET_ERR_LOG = 5;
    public static final int RESULT_REPORT_JSON_ERR_LOG = 6;
    public static final int RESULT_UNKNOWN_ERR_LOG = 99;
    public static final String INTENT_VIDEO_ID = "HDVD_VIDEO_ID";
    public static final String INTENT_VIDEO_TITLE = "HDVD_VIDEO_TITLE";
    public static final String INTENT_VIDEO_URL = "HDVD_VIDEO_URL";
    public static int NOTI_ID = 153153;
    public static boolean NOW_DEVELOP_OFF_ADS = false;
    public static LinkedList<MyListItem> videoList;
    public static String FAN_INTRO = "452950815095328_639724003084674";
    public static String FAN_INTRO_NATIVE = "452950815095328_634288890294852";
    public static String FAN_MAIN_NATIVE = "452950815095328_457364141320662";
    //    public static String FAN_SPLASH_NATIVE = "452950815095328_507389692984773";
    public static String FAN_MAIN_BANNER = "452950815095328_452952398428503";
    //    public static String FAN_EXIT_BANNER = "452950815095328_457348577988885";
    public static String FAN_DOWNLOAD_BANNER = "452950815095328_452967615093648";
    //    public static String FAN_GIFT_NATIVE = "452950815095328_634288890294852";
//    public static String ADMOB_SPLASH_NATIVE = "ca-app-pub-8870534780213740/8930034510";
    public static String ADMOB_APP_ID = "ca-app-pub-8870534780213740~9376379312";
    public static String ADMOB_APP_ID_TEST = "ca-app-pub-3940256099942544~3347511713";
    public static String ADMOB_INTRO_NATIVE = "ca-app-pub-8870534780213740/2477626088";
    public static String ADMOB_INTRO_NATIVE_TEST = "ca-app-pub-3940256099942544/2247696110";
    public static String ADMOB_INTRO_INTERSTITIAL = "ca-app-pub-8870534780213740/8280524288";
    public static String ADMOB_INTRO_INTERSTITIAL_TEST = "ca-app-pub-3940256099942544/1033173712";
    public static String ADMOB_MID_INTRO = "ca-app-pub-8870534780213740/2581358915";
    public static String ADMOB_MID_INTRO_TEST = "ca-app-pub-3940256099942544/6300978111";
    public static String ADMOB_EXIT_BANNER = "ca-app-pub-8870534780213740/8990723317";
    public static String ADMOB_EXIT_BANNER_TEST = "ca-app-pub-3940256099942544/6300978111";
    //    public static String ADMOB_MAIN_NATIVE = "ca-app-pub-8870534780213740/7543081714";
//    public static String ADMOB_MAIN_NATIVE_MID = "ca-app-pub-8870534780213740/1228200515";
//    public static String ADMOB_MAIN_NATIVE_MID_TEST = "ca-app-pub-3940256099942544/2793859312";
//    public static String ADMOB_MAIN_NATIVE_SMALL = "ca-app-pub-8870534780213740/8891068114";
    public static String ADMOB_MAIN_NATIVE_ADVANCED = "ca-app-pub-8870534780213740/2935798110";
    public static String ADMOB_MAIN_NATIVE_ADVANCED_TEST = "ca-app-pub-3940256099942544/2247696110";
    public static String ADMOB_MAIN_BANNER_OPT = "ca-app-pub-8870534780213740/4640965712";
    public static String ADMOB_MAIN_BANNER_OPT_TEST = "ca-app-pub-3940256099942544/6300978111";
    public static String ADMOB_DOWNLOAD_BANNER = "ca-app-pub-8870534780213740/1467456512";
    public static String ADMOB_DOWNLOAD_BANNER_TEST = "ca-app-pub-3940256099942544/6300978111";
    //    public static String ADMOB_GIFT_NATIVE_MID = "ca-app-pub-8870534780213740/3528390204";
//    public static String ADMOB_GIFT_NATIVE_MID_TEST = "ca-app-pub-3940256099942544/2177258514";
//    public static String MOBVISTA_APP_WALL = "10738";
    public static String SITE = "";
    public static String PREFIX_DOWNLOAD_FILE = "HDV_";
    public static int width = 720;
    public static boolean NOW_LOADING = false;
    public static boolean NOW_DOWNLOAD = false;
    public static String FRAG_INTENT_DOWNLOAD_STATUS = "downloadStatus";
    public static String TAG_DIALOG_ALERT_YOUTUBE = "youtube_alert";
    public static String TAG_DIALOG_ALERT_WARNING = "warning_alert";
    public static String TAG_DIALOG_ADD_DOWNLOAD = "add_download";
    public static String TAG_DIALOG_DELETE_ALL_ITEMS = "delete_all_items";
    public static String SEARCH_ENGINE_GOOGLE = "http://www.google.com/m?q=";
    public static String SEARCH_ENGINE_NAVER = "http://m.search.naver.com/search.naver?query=";
    public static String SEARCH_ENGINE_BING = "http://www.bing.com/search?q=";
    public static String PACKAGE_JM_FILEMANAGER = "myfilemanager.jiran.com.myfilemanager";
    public static String PACKAGE_JM_BROWSER = "io.jmobile.browser";
    public static String PACKAGE_JM_INSTANT = "io.jmobile.instant";
    public static String PACKAGE_JM_WEATHER = "com.jiran.weatherlocker";
    public static String PACKAGE_EGG_STORY = "com.jirantech.eggstory";
    public static String PACKAGE_QR_SCANNER = "io.jmobile.jmscanner";
    public static String PACKAGE_TM_FILEMANAGER = "jiran.com.tmfilemanager";
    //	public static String PACKAGE_VD_FACEBOOK = "com.inho.vff";
    public static String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=";
    public static String MAIN_HOMEPAGE = "http://jmobile.io";
    public static String MAIN_JMOBILE_CHANNAL = "https://www.youtube.com/user/dooyonce13/feed";
    public static String MAIN_TUTORIAL_URL = "https://www.youtube.com/watch?v=cKxSVgjuaT8&feature=youtu.be";
    public static String MAIN_JMOBILE_FACEBOOK = "https://www.facebook.com/jmobile.io";
    public static String MAIN_JMOBILE_INSTAGRAM = "https://www.instagram.com/jmobile.io/";
    public static String MAIN_JMOBILE_APP = "http://jmobile.io/apps/";
    public static String MAIN_JMOBILE_HELP_CENTER = "https://rte4a.app.goo.gl/hdvdhelp";
    //    public static String MAIN_JMOBILE_HELP_CENTER = "http://help.jmobile.io/article-category/hd-video-downloader/";
    public static String MAIN_JMOBILE_VIDEO_INSTAGRAM = "https://www.instagram.com/p/BXPvGjahT54/?taken-by=jmobile.io";
    public static String MAIN_JMOBILE_VIDEO_FACEBOOK = "http://www.facebook.com/jmobile.io/videos/1889693647946776";
    public static int MAX_BOOKMARK_COUNT = 17;
    public static String INAPP_LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtiAetVmU43gkXfl+/i1DaVmw0iiP5WYb54+kDDTbKqCgAn57Tp69rRCXsMJ7rj2EEAjcn8LhkY30PkMPYGQdYMoGTZs6lfg7SlcI6JnJtL4pi4iqcwzqxxj9pl0koZaEWv18ufXfWZhK/tZyFSQxGXvuilIIdA/hHQaW3ZqvmXATErpkMGm9I+Z/tXQbsCeRlhQ50CwNwsrbV70PNX955Snqu0x9ZOx6kz7TmrDz9kHqxyf/k1LRC0f9qCv+ctzevlfsXXoIxFQFPDKPjt7usOPzO94ohOSVHiJDnalfZWbNP7smbPM39OxBTMdbs2TOhmKVrmqBRL6eFzcPP7ihFQIDAQAB";
    public static String PREMIUM_DOWNLOADER_ID = "premium_downloader";
    public static String NOTIFICATION_CHANNEL_ID = "hdvd_download_s";

    public static String getVideoDir(Context context) {
        String path;
        try {
            path = android.os.Environment.getExternalStorageDirectory()
                    + "/HDV Downloader/";
        } catch (Exception e) {
            path = context.getFilesDir().getPath() + "/HDV Downloader/";
        }
        return path;
    }

    public static String checkVideoExtension(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.ogg.*")) {
            fileExt = ".ogg";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.ape.*")) {
            fileExt = ".ape";
        } else if (url.matches(".*.avi.*")) {
            fileExt = ".avi";
        } else if (url.matches(".*.mp3.*")) {
            fileExt = ".mp3";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = ".mp4";
        }
        return fileExt;
    }

    public static String checkAvailableDownload(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = "novideo";
        }
        return fileExt;
    }

    public static String checkAvailableDownload2(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac")) {
            fileExt = ".flac";
        } else if (url.matches(".*.wmv")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.mpg")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov")) {
            fileExt = ".mov";
        } else {
            fileExt = "novideo";
        }
        return fileExt;
    }
}
