package com.ne.hdv.download;

public class DownMgr {
//	public static final int DOWNLOAD_START = 0;
//	public static final int DOWNLOAD_SUCCESS = 1;
//	public static final int DOWNLOAD_FAIL = 2;
//	public static final int DOWNLOAD_CANCEL = 3;
//	public static final int DOWNLOAD_ITEM_START = 11;
//	public static final int DOWNLOAD_ITEM_PROGRESS = 12;
//	public static final int DOWNLOAD_ITEM_SUCCESS = 13;
//	public static final int DOWNLOAD_ITEM_FAIL = 14;
//	public static boolean NOW_DOWNLOAD = false;
//	public static DownMgr downloadManager;
//	private static Context mContext;
//	public static LinkedList<DownItem> linkedList;
//	static int downloadCount = 0;
//	static int itemIndex = 0;
//	static DownloadFileAsync downloadAync;
//	public DownMgr(Context ctx) {
//		linkedList = new LinkedList<DownItem>();
//		downloadManager = this;
//		mContext = ctx;
//	}
//
//	public static void init(Context ctx) {
//		if (downloadManager == null)
//			downloadManager = new DownMgr(ctx);
//	}
//	public static DownMgr getInstance(Context ctx) {
//		if (downloadManager == null) {
//			downloadManager = new DownMgr(ctx);
//		}
//		return downloadManager;
//	}
//
//	public static void setHandler(Handler h){
//		mHandler = h;
//	}
//	public static void addItem(DownItem item){
//		try {
//			linkedList.addLast(item);
//
//		} catch (Exception e){
//			linkedList = new LinkedList<DownItem>();
//			linkedList.addLast(item);
//
//		}
//	}
//	static Handler mHandler;
//	static String downloadFileName ="VID";
//	public static boolean requestDownload() {
//		if(!NOW_DOWNLOAD){
//			sendStatus(DOWNLOAD_START, 0, 0);
//			DownItem item = linkedList.get(0);
//			downloadAync = new DownloadFileAsync();
//			downloadAync.execute(item.getName(), item.getURL());
//			downloadFileName = item.getName();
//		}
//		try { startNotification(0); } catch (Exception e) { }
//
//		return true;
//	}
//
//	public static void cancelIndex(){
//		isStop = true;
//		if(downloadAync.cancel(true)){
//			NOW_DOWNLOAD = false;
//			startNotification(2);
//			linkedList = new LinkedList<DownItem>();
//			downloadCount = 0;
//			itemIndex = 0;
//			sendStatus(DOWNLOAD_CANCEL, 0, 0);
//		}
//	}
//	static boolean isStop = false;
//	static boolean isComplete;
//	static class DownloadFileAsync extends AsyncTask<String, String, String> {
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			NOW_DOWNLOAD = true;
//			isStop = false;
//			isComplete = true;
//		}
//
//		protected String doInBackground(String... params) {
//			downloadCount++;
//
//			String filePath = Common.getVideoDir(mContext) + params[0];
//			try {
//				URL url = new URL(params[1]);
//				URLConnection connection = url.openConnection();
//				connection.connect();
//				int fileLength = connection.getContentLength();
//
//				sendStatus(DOWNLOAD_ITEM_START, itemIndex, fileLength);
//				InputStream input = new BufferedInputStream(url.openStream());
//				OutputStream output = new FileOutputStream(filePath);
//
//				byte data[] = new byte[4096];
//				long total = 0;
//				int count;
//
//				while ((count = input.read(data)) != -1) {
//					total += count;
//					onProgressUpdate((int) (total * 100 / fileLength));
//					output.write(data, 0, count);
//
//					if (isStop) {
//						final File f = new File(filePath);
//						if (f.exists()) {
//							Thread t = new Thread(new Runnable() {
//								@Override
//								public void run() {
//									f.delete();
//								}
//							});
//							t.start();
//							isComplete = false;
//
//						}
//						break;
//					}
//				}
//				output.flush();
//				output.close();
//				input.close();
//			} catch (Exception e) {
//				final File f = new File(filePath);
//				if (f.exists()) {
//					Thread t = new Thread(new Runnable() {
//						@Override
//						public void run() {
//							f.delete();
//						}
//					});
//					t.start();
//				}
//				isComplete = false;
//			}
//			return null;
//		}
//
//		int compare  = 0;
//		protected void onProgressUpdate(int progress) {
//			if (compare == progress) { // 1단위로 증가
//				compare = progress + 2;
//				sendStatus(DOWNLOAD_ITEM_PROGRESS, itemIndex, progress);
//			}
//		}
//
//		@Override
//		protected void onPostExecute(String unused) {
//			try {
//				mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Common.getVideoDir(mContext) + downloadFileName)));
//			} catch (Exception e){
//			}
//
//			if (linkedList.size() == downloadCount) {
//				if(isComplete)
//					try { startNotification(1); } catch (Exception e) { }
//				else
//					try { startNotification(2);
//					} catch (Exception d) { }
//
//				sendStatus(DOWNLOAD_SUCCESS, 0, 0);
//				NOW_DOWNLOAD = false;
//				linkedList = new LinkedList<DownItem>();
//				downloadCount = 0;
//				itemIndex = 0;
//				return;
//			} else {
//				sendStatus(DOWNLOAD_ITEM_SUCCESS, itemIndex, 0);
//				itemIndex++;
//
//				DownItem item = linkedList.get(itemIndex);
//				downloadAync = new DownloadFileAsync();
//				downloadAync.execute(item.getName(), item.getURL());
//				downloadFileName = item.getName();
//			}
//		}
//
//	}
//	public static void sendStatus(int what, int arg1, int arg2) {
//		if (mHandler != null) {
//			Message msg = new Message();
//			msg.what = what;
//			msg.arg1 = arg1;
//			msg.arg2 = arg2;
//			mHandler.sendMessage(msg);
//		}
//	}
//
//	public static void startNotification(int type) {
//		CharSequence tickerTitle = "";
//		CharSequence tickerText = "";
//		int tickerIcon = R.drawable.icon_download;
//
//		Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_download);
//
//		if(type == 0){
//			tickerIcon = R.drawable.icon_download;
//			icon =  BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_download);
//			tickerTitle = mContext.getResources().getString(R.string.noti_start_download);
//			tickerText = linkedList.size() + " videos..";
//			MainActivity.txtStatus.setText(tickerTitle);
//		}
//		else if(type == 1) {
//			tickerIcon = R.drawable.icon_download_complete;
//			icon =  BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_download_complete);
//			tickerTitle = mContext.getResources().getString(R.string.noti_download_complete);
//			tickerText = downloadCount +" videos download";
//			MainActivity.txtStatus.setText(tickerTitle);
//			statusTextOff();
//		} else if(type == 2) {
//			tickerIcon = R.drawable.icon_fail;
//			icon =  BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_fail);
//
//			tickerTitle = mContext.getResources().getString(R.string.noti_download_cancel);
//			tickerText = "";
//			MainActivity.txtStatus.setText(tickerTitle);
//			statusTextOff();
//		} else if(type == 3) {
//			tickerIcon = R.drawable.icon_fail;
//			icon =  BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_fail);
//			tickerTitle = mContext.getResources().getString(R.string.noti_download_fail);
//			tickerText = "";
//			MainActivity.txtStatus.setText(tickerTitle);
//			statusTextOff();
//		}
//
//		NotificationCompat.Builder mBuilder =
//				new NotificationCompat.Builder(mContext)
//						.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon))main
//						.setSmallIcon(tickerIcon)
//						.setContentTitle(tickerTitle)
//						.setContentText(tickerText);
//
//		Intent resultIntent = new Intent(mContext, MainActivity.class);
//        // cecil
//        resultIntent.putExtra("downloadStatus", tickerTitle);
//
//
//		TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
//		stackBuilder.addParentStack(MainActivity.class);
//		stackBuilder.addNextIntent(resultIntent);
//		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
//		mBuilder.setContentIntent(resultPendingIntent);
//		NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//		mNotificationManager.notify(160604, mBuilder.build());
//	}
//
//
//	public static void statusTextOff(){
//		Handler handle = new Handler();
//		handle.postDelayed(new Runnable() {
//			public void run() {
//
//				MainActivity.txtStatus.setText("HD Video Downloader");
//
//			}
//		}, 2000);
//	}


}
