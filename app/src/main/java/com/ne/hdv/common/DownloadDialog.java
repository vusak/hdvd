package com.ne.hdv.common;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

public class DownloadDialog extends BaseDialogFragment {
    public static final String TAG = DownloadDialog.class.getSimpleName();

    String hint;
    Bitmap bitmap;
    EditText saveText;
    ImageView thumbnail;
    TextView timeText;
    Button cancelButton, downloadButton;
    long time;

    private DownloadDialogListener listener;

    public static DownloadDialog newInstance(String tag, String hint, Bitmap thumbnail, long time) {
        DownloadDialog d = new DownloadDialog();

        d.hint = hint;
        d.bitmap = thumbnail;
        d.time = time;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_download;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        saveText = fv(R.id.edt_save);
        saveText.setHint(hint);

        thumbnail = fv(R.id.image_thumbnail);
        if (bitmap != null)
            thumbnail.setImageBitmap(bitmap);
        else
            thumbnail.setVisibility(View.GONE);

        cancelButton = fv(R.id.layout_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelButtonClick();

                Util.hideKeyBoard(saveText);
                dismiss();
            }
        });
        downloadButton = fv(R.id.layout_download);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    String filename = hint;
                    if (!TextUtils.isEmpty(saveText.getText().toString().trim())) {
                        filename = saveText.getText().toString() + Common.checkVideoExtension(filename);
                    }

                    listener.onDownloadButtonClick(filename);
                }

                Util.hideKeyBoard(saveText);
                dismiss();
            }
        });

        timeText = fv(R.id.text_time);
        if (time > 0) {
            timeText.setVisibility(View.VISIBLE);
            timeText.setText(FileUtil.convertToStringTime(time));
        } else
            timeText.setVisibility(View.GONE);
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (getDialog() == null || getDialog().getWindow() == null || getDialog().getWindow().getAttributes() == null)
                return;
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    public DownloadDialog setListener(DownloadDialogListener listener) {
        this.listener = listener;

        return this;
    }

//    public void setHint(String hint) {
//        saveText.setHint(hint);
//    }

    public static interface DownloadDialogListener {
        public void onDownloadButtonClick(String name);

        public void onCancelButtonClick();
    }

}
