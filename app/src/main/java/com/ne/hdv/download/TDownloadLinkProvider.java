package com.ne.hdv.download;

import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Twitch Clip
 **/
public class TDownloadLinkProvider {

    public static final String DEFAULT_MOBILE_USER_AGENT_STRING
            = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
    private String userAgent = DEFAULT_MOBILE_USER_AGENT_STRING;

    private String source;
    private String videoId;

    public TDownloadLinkProvider(String source) {
        this.source = source;
    }

    public String getVideoLink() {
        String videoList = "";
        try {
            videoList = getRemoteContent(source);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
        if ((videoList == null) || (videoList.isEmpty())) {
            return null;
        }

        JSONObject info;
        String videoURL = "";
        try {
            info = new JSONObject(videoList);
            JSONArray arr = info.getJSONArray("quality_options");
            if (arr != null) {
                if (arr.length() > 1)
                    videoURL = Util.optString(arr.getJSONObject(1), "source");
                else
                    videoURL = Util.optString(arr.getJSONObject(0), "source");
            }
        } catch (Exception e) {
            return null;
        }

        return videoURL;
    }

    private String getRemoteContent(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        connection.setRequestProperty("User-Agent", userAgent);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Referrer", url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

//    private String getRemoteContent(String url) {
//        String content = null;
//
//        try {
//
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet(url);
//            HttpResponse httpResponse = httpClient.execute(httpGet);
//            content = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
//
//        } catch (Exception e) {
//            content = null;
//        }
//
//        return content;
//    }
}