package com.ne.hdv.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class ReDownloadHolder extends ReAbstractViewHolder {
    LinearLayout root, editLayout;
    View line, selBg;
    CheckBox check;
    ImageView thumbnail;
    ProgressBar progressBar;
    TextView title, subTitle, sizeText, url, count, readyTitle, readySub;
    ImageButton downloadButton, deleteButton, editButton, clearButton;
    private OnItemViewClickListener downloadButtonClickListener, deleteButtonClickListener, editButtonClickListener, clearButtonClickListener;

    public ReDownloadHolder(View itemView) {
        super(itemView);
    }

    public void setOnDownloadButtonClickListener(OnItemViewClickListener listener) {
        downloadButtonClickListener = listener;
        if (listener != null && downloadButton != null)
            downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadButtonClickListener.onItemViewClick(getPosition(), ReDownloadHolder.this);
                }
            });
    }

    public void setOnDeleteButtonClickListener(OnItemViewClickListener listener) {
        deleteButtonClickListener = listener;
        if (listener != null && deleteButton != null)
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButtonClickListener.onItemViewClick(getPosition(), ReDownloadHolder.this);
                }
            });
    }

    public void setOnEditdButtonClickListener(OnItemViewClickListener listener) {
        editButtonClickListener = listener;
        if (listener != null && editButton != null)
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editButtonClickListener.onItemViewClick(getPosition(), ReDownloadHolder.this);
                }
            });
    }

    public void setOnClearButtonClickListener(OnItemViewClickListener listener) {
        clearButtonClickListener = listener;
        if (listener != null && clearButton != null)
            clearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearButtonClickListener.onItemViewClick(getPosition(), ReDownloadHolder.this);
                }
            });
    }
}
