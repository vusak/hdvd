package com.ne.hdv.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.WindowManager;

import com.google.android.exoplayer2.ExoPlayer;
import com.ne.hdv.common.LogUtil;

import static android.view.MotionEvent.INVALID_POINTER_ID;

public class BaseVideoActivity extends BaseActivity {
    AudioManager audio;
    ExoPlayer videoView;
    boolean action_down = true;
    float dist = 0;
    private int mActivePointerId = INVALID_POINTER_ID;
    private int currentVolume;
    private int numberOfTaps = 0;
    private long lastTapTimeMs = 0;
    private long touchDownMs = 0;

    private float brightness;
    private float seekDistance = 0;
    private float distanceCovered = 0;

    private boolean lock = false;
    private boolean start = true;
    private boolean checkBrightness;
    private boolean checkVolume;
    private boolean checkSeek;

    private Display display;
    private Point size;
    private int screenWidth, sHeight;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        sHeight = size.y;

    }

    public void setControl(Context context, ExoPlayer player, boolean lock) {
        this.lock = lock;
        videoView = player;
        brightness = android.provider.Settings.System.getFloat(getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, -1);
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = brightness / 255;
        getWindow().setAttributes(layout);

        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            commonVolume(false);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            commonVolume(true);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    long startClickTime = 0;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = MotionEventCompat.getActionMasked(event);
        final int MAX_CLICK_DURATION = 200;
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                LogUtil.log("ACTION_DOWN : " + screenWidth);
                seekDistance = 0;
                distanceCovered = 0;
                touchDownMs = System.currentTimeMillis();

                start = true;
                checkBrightness = false;
                checkVolume = false;
                checkSeek = false;
                if (!lock) {
                    if (event.getX() < screenWidth / 4)
                        checkBrightness = true;
                    else if (event.getX() > screenWidth / 4 * 3)
                        checkVolume = true;
                    else
                        checkSeek = true;
                }
                else {
                    startClickTime = System.currentTimeMillis();
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (action_down && !lock) {
                    start = true;
                    action_down = false;
                    checkBrightness = false;
                    checkVolume = false;
                    checkSeek = false;
                    if (event.getX() < screenWidth / 4)
                        checkBrightness = true;
                    else if (event.getX() > screenWidth / 4 * 3)
                        checkVolume = true;
                    else
                        checkSeek = true;
                }

                if (action_down && lock)
                    startClickTime = System.currentTimeMillis();

                final float x = event.getX();
                final float y = event.getY();
                distanceCovered = getDistance(x, y, event);
                try {
                    if (checkBrightness)
                        changeBrightness(event.getHistoricalX(0, 0), event.getHistoricalY(0, 0), x, y, distanceCovered);
                    else if (checkVolume)
                        changeVolume(event.getHistoricalX(0, 0), event.getHistoricalY(0, 0), x, y, distanceCovered);
                    else if (checkSeek)
                        changeSeek(event.getHistoricalX(0, 0), event.getHistoricalY(0, 0), x, y, distanceCovered);
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
                break;
            case MotionEvent.ACTION_UP: {
                if (lock) {
                    long clickDuration = System.currentTimeMillis() - startClickTime;
                    if (clickDuration < MAX_CLICK_DURATION) {
                        clicked();
                    }
                }

                start = false;
                action_down = true;
                seekDistance = 0;
                distanceCovered = 0;
                touchDownMs = System.currentTimeMillis();

                checkBrightness = false;
                checkVolume = false;
                checkSeek = false;
                LogUtil.log("ACTION_UP : " + screenWidth);

                if ((System.currentTimeMillis() - touchDownMs) > ViewConfiguration.getTapTimeout()) {
                    numberOfTaps = 0;
                    lastTapTimeMs = 0;
                    break;
                }

                if (numberOfTaps > 0 && (System.currentTimeMillis() - lastTapTimeMs) < ViewConfiguration.getDoubleTapTimeout()) {
                    numberOfTaps += 1;
                } else {
                    numberOfTaps = 1;
                }

                lastTapTimeMs = System.currentTimeMillis();
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }
            case MotionEvent.ACTION_CANCEL: {
                start = false;
                action_down = true;
                seekDistance = 0;
                distanceCovered = 0;
                touchDownMs = System.currentTimeMillis();

                checkBrightness = false;
                checkVolume = false;
                checkSeek = false;
                LogUtil.log("ACTION_CANCEL : " + screenWidth);
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }
            case MotionEvent.ACTION_POINTER_UP: {
                LogUtil.log("ACTION_POINTER_UP : " + screenWidth);
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex);

                if (pointerId == mActivePointerId) {
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
                }
                break;
            }
        }
        return false;
    }

    public void clicked() {

    }

    public void changeVolume(float X, float Y, float x, float y, float distance) {
        if (start) {
            if (distance > 10) start = false;
            return ;
        }

        distance = distance / 300;
        if (y < Y) {
            commonVolume(distance);
        } else {
            commonVolume(-distance);
        }
    }

    public void commonVolume(float distance) {
        currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

        dist = dist + distance;
        if (Math.abs(dist) > 0.1) {
            dist = 0;

            int vol = currentVolume;
            if (distance > 0)
                vol += 1;
            else if (distance < 0)
                vol -= 1;
            audio.setStreamVolume(AudioManager.STREAM_MUSIC, vol, 0);
            currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
    }

    public void commonVolume(boolean up) {
        currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, up ? currentVolume + 1 : currentVolume - 1, 0);
        currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    public int getVolumePer() {
        return currentVolume;
    }

    public void changeBrightness(float X, float Y, float x, float y, float distance) {
        if (start) {
            if (distance > 10) start = false;
            return ;
        }

        distance = distance / 400;
        if (y < Y) {
            commonBrightness(distance);
        } else {
            commonBrightness(-distance);
        }
    }

    public void commonBrightness(float distance) {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        if (getWindow().getAttributes().screenBrightness + distance <= 1 && getWindow().getAttributes().screenBrightness + distance >= 0) {
            layout.screenBrightness = getWindow().getAttributes().screenBrightness + distance;
            getWindow().setAttributes(layout);
        }
    }

    public void changeSeek(float X, float Y, float x, float y, float distance) {
        if (start) {
            if (distance > 10) start = false;
            return ;
        }

        distance = distance / 200;
        if (x > X) {
            seekCommon(distance);
        } else {
            seekCommon(-distance);
        }
    }

    ;

    public float getSeekDistance() {
        return seekDistance;
    }

    public void seekCommon(float distance) {
        seekDistance += distance * 60000;
    }

    float getDistance(float startX, float startY, MotionEvent ev) {
        float distanceSum = 0;
        final int historySize = ev.getHistorySize();
        for (int h = 0; h < historySize; h++) {
            float hx = ev.getHistoricalX(0, h);
            float hy = ev.getHistoricalY(0, h);
            float dx = (hx - startX);
            float dy = (hy - startY);
            distanceSum += Math.sqrt(dx * dx + dy * dy);
            startX = hx;
            startY = hy;
        }
        float dx = (ev.getX(0) - startX);
        float dy = (ev.getY(0) - startY);
        distanceSum += Math.sqrt(dx * dx + dy * dy);
        return distanceSum;
    }
}
