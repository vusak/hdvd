package com.ne.hdv.adapter;


import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.data.BookmarkItem;

import java.util.List;

public class BookmarkAdapter extends ReSelectableAdapter<BookmarkItem, BookmarkAdapter.ReQuickDialHolder> {

    private Context context;
    private boolean deleteMode = false;


    public BookmarkAdapter(Context context, int layoutId, List<BookmarkItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ReQuickDialHolder holder, int position) {
        BookmarkItem item = items.get(position);

        holder.title.setText(item.getBookmarkName());
        if (item.getBookmarkId().equals(context.getString(R.string.add_new)))
            holder.title.setText(context.getString(R.string.str_add_bookmark));
        if (item.getBookmarkUrl() != null && item.getBookmarkUrl().equals(BookmarkItem.ID_HOW_TO_USE))
            holder.title.setText(context.getString(R.string.str_how_to_use));

        String url = item.getBookmarkName();
        if (!TextUtils.isEmpty(url)) {

        } else {
            url = item.getBookmarkUrl();
            url = url.replace("http://", "");
            url = url.replace("https://", "");
            url = url.replace("www.", "");
            url = url.replace("m.", "");
            url = url.replace("tv.", "");
        }
        url = url.substring(0, 1);
        holder.initial.setText(url);

        if (item.getRowId() == -2 || (item.getBookmarkUrl() != null &&
                (item.getBookmarkUrl().equals(BookmarkItem.ID_HOW_TO_USE) || item.getBookmarkUrl().equals(BookmarkItem.ID_DO_IT_YOURSELF)))) {
            holder.icon.setBackgroundColor(Color.TRANSPARENT);
            if (item.getBookmarkId().equals(BookmarkItem.ID_AD_INSTANT)) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_instant_app_v_2_50));
            } else if (item.getBookmarkId().equals(BookmarkItem.ID_AD_JM_BROWSER)) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_jmbr_app_50));
            } else if (item.getBookmarkId().equals(BookmarkItem.ID_AD_FILEMANAGMER)) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_hdfm_app_50));
            } else if (item.getBookmarkId().equals(BookmarkItem.ID_AD_EGG_STORY)) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_egg_story_app_50));
            } else if (item.getBookmarkId().equals(BookmarkItem.ID_AD_QR_SCANNER)) {
                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_jmqr_50));
            } else if (item.getBookmarkUrl() != null) {
//                holder.icon.setBackgroundResource(R.drawable.app_background_a);
                if (item.getBookmarkUrl().equals(BookmarkItem.ID_HOW_TO_USE))
                    holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_htu_blue_50));
                else if (item.getBookmarkUrl().equals(BookmarkItem.ID_DO_IT_YOURSELF))
                    holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_tut_blue_50));
            }
            holder.icon.setPadding(0, 0, 0, 0);
            holder.mask.setVisibility(View.GONE);
            holder.icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            holder.icon.setPadding(0, 0, 0, 0);


            if (item.getBookmarkIcon() != null) {
                holder.icon.setBackgroundResource(R.color.transparent);
                holder.icon.setImageBitmap(ImageUtil.getImage(item.getBookmarkIcon(), 50, 50));
                holder.icon.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.mask.setVisibility(View.VISIBLE);
                holder.initial.setVisibility(View.GONE);
            } else {
                holder.mask.setVisibility(View.GONE);
                holder.icon.setBackgroundResource(R.drawable.app_background);
                holder.icon.setImageResource(R.color.transparent);
                holder.initial.setVisibility(View.VISIBLE);
//            holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_public_navy_24));
//            holder.icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//            holder.mask.setVisibility(View.VISIBLE);
            }

            if (item.getRowId() == -1) {
                holder.icon.setBackgroundResource(R.drawable.app_background_g);
                holder.icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                holder.mask.setVisibility(View.GONE);
            }
//            holder.icon.setBackground(ContextCompat.getDrawable(context, R.drawable.app_background));
        }

        holder.deleteButton.setVisibility(deleteMode ? View.VISIBLE : View.GONE);
        holder.title.setTextColor(ContextCompat.getColor(context, deleteMode ? R.color.white : R.color.primary1_a90));
        holder.mask.setImageResource(deleteMode ? R.drawable.ic_mask_edit_gray_50 : R.drawable.ic_mask_white_50);
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                deleteMode = !isDeleteMode();
                if (items.get(position).getRowId() == -1 || items.get(position).getRowId() == -2)
                    return false;

                setDeleteMode(true);
                if (listener != null && items.size() > position) {
                    listener.OnItemLongClick(position, items.get(position));
                }
                return true;
            }
        });
    }

    @Override
    public ReQuickDialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReQuickDialHolder holder = new ReQuickDialHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.icon = holder.fv(R.id.image_icon);
        holder.title = holder.fv(R.id.text_title);
        holder.mask = holder.fv(R.id.image_mask);
        holder.initial = holder.fv(R.id.text_initial);
        holder.deleteButton = holder.fv(R.id.button_del);
        return holder;
    }

    public boolean isDeleteMode() {
        return this.deleteMode;
    }

    public void setDeleteMode(boolean mode) {
        this.deleteMode = mode;
        if (mode) {
            for (int i = items.size() - 1; i >= 0; i--) {
                if (items.get(i).getRowId() == -1 || items.get(i).getRowId() == -2)
                    items.remove(items.get(i));
            }
        }

        notifyDataSetChanged();
    }

    public class ReQuickDialHolder extends ReAbstractViewHolder {

        ImageView icon, mask;
        TextView title, initial;
        ImageButton deleteButton;

        public ReQuickDialHolder(View itemView) {
            super(itemView);
        }
    }
}
