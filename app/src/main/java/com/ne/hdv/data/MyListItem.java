package com.ne.hdv.data;

import java.io.Serializable;

public class MyListItem implements Serializable {

    public static final long serialVersionUID = 1L;

    public int type;
    public String name;
    public String date;

    public String extension;
    public String size;

    public String path;
    public String thumb;

    public boolean isSelected;
    public boolean isSaved;
    public boolean isFolder;


    public MyListItem() {

    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String path) {
        this.thumb = path;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public boolean getIsFolder() {
        return isFolder;
    }

    public void setIsFolder(boolean folder) {
        this.isFolder = folder;
    }

    public boolean getSaved() {
        return isSaved;
    }

    public void setSaved(boolean s) {
        this.isSaved = s;
    }

}
