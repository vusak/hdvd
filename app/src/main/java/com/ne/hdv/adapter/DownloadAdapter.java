package com.ne.hdv.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ne.hdv.R;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.DownItem;

import java.util.List;

public class DownloadAdapter extends ReSelectableAdapter<DownItem, ReDownloadHolder> {

    private Context context;
    private int editIndex = -1;
    private boolean isTrend = false;
    private boolean isVideo = false;

//	private ImageLoader imageLoader;

    public DownloadAdapter(Context context, int layoutId, List<DownItem> items, DownloadAdapterListener listener) {
        super(layoutId, items, context);

        this.listener = listener;
        this.context = context;
//		imageLoader = new ImageLoader(context);
    }

    @Override
    public void onBindViewHolder(ReDownloadHolder holder, int position) {
        DownItem item = items.get(position);
        if (item == null)
            return;

        if (holder.title != null)
            holder.title.setText(item.getDownloadFileName());
        holder.check.setVisibility(isCheckMode() ? View.VISIBLE : View.GONE);
        if (holder.selBg != null)
            holder.selBg.setVisibility(View.GONE);
        if (isCheckMode()) {
            holder.check.setChecked(item.isSelected());
            if (holder.selBg != null) {
                holder.selBg.setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
                holder.check.setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
            } else if (holder.root != null)
                holder.root.setBackgroundColor(ContextCompat.getColor(context, item.isSelected() ? R.color.download_selected_bg : R.color.download_non_bg));
        } else {
            if (holder.root != null)
                holder.root.setBackgroundColor(ContextCompat.getColor(context, R.color.download_non_bg));
        }

        if (holder.selBg != null && !isCheckMode())
            holder.selBg.setVisibility(position == editIndex ? View.VISIBLE : View.GONE);
        if (holder.editLayout != null)
            holder.editLayout.setVisibility(position == editIndex && !isCheckMode() ? View.VISIBLE : View.GONE);

        if (isVideo) {
            holder.root.setBackgroundResource(R.drawable.ripple_effect_main_transparent);
            holder.title.setTextColor(ContextCompat.getColor(context, position == editIndex ? R.color.main_red : R.color.white_a50));
        }

        if (holder.url != null) {
            String url = "";
            if (!TextUtils.isEmpty(item.getDownloadSite())) {
                url = item.getDownloadSite().replace("http://", "");
                url = url.replace("https://", "");
            }
            holder.url.setText(url);
        }

        if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DONE) {
            if (holder.downloadButton != null)
                holder.downloadButton.setVisibility(View.GONE);

            String sub = item.getDownloadFileTime() > 0 ? FileUtil.convertToStringTime(item.getDownloadFileTime()) + " / " : "";
            sub = sub + (item.getDownloadFileTotalSize() > 0 ? FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize()) : "");
            holder.subTitle.setText(sub);

            if (holder.progressBar != null)
                holder.progressBar.setVisibility(View.GONE);

//			if (holder.count != null)
//				holder.count.setVisibility(View.GONE);

            if (holder.count != null) {
                holder.count.setVisibility(isTrend ? View.VISIBLE : View.GONE);
                holder.count.setText("" + item.getDownloadCount());
            }
        } else {
            if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_NONE) {
                String sub = item.getDownloadFileTime() > 0 ? FileUtil.convertToStringTime(item.getDownloadFileTime()) + " / " : "";
                sub = sub + (item.getDownloadFileTotalSize() > 0 ? FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize()) : "");
                holder.subTitle.setText(sub);

                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.GONE);
                if (holder.downloadButton != null)
                    holder.downloadButton.setImageResource(R.drawable.button_down);
            } else {
                if (holder.progressBar != null)
                    holder.progressBar.setVisibility(View.VISIBLE);
                long total = item.getDownloadFileTotalSize();
                long remain = item.getDownloadFileRemainSize();

                if (remain <= 0)
                    holder.subTitle.setText("Ready");
                else
                    holder.subTitle.setText(FileUtil.convertToStringRepresentation(remain) + " / " + FileUtil.convertToStringRepresentation(total));
                if (holder.progressBar != null) {
                    holder.progressBar.setMax((int) total);
                    holder.progressBar.setProgress((int) remain);

                    if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_PAUSE)
                        holder.downloadButton.setImageResource(R.drawable.button_refresh);
                    else
                        holder.downloadButton.setImageResource(R.drawable.button_pause);
                }

                if (holder.readyTitle != null)
                    holder.readyTitle.setText(item.getDownloadUrl());

                if (holder.readySub != null) {
                    String sub = item.getDownloadFileTime() > 0 ? FileUtil.convertToStringTime(item.getDownloadFileTime()) + " / " : "";
                    sub = sub + (item.getDownloadFileTotalSize() > 0 ? FileUtil.convertToStringRepresentation(item.getDownloadFileTotalSize()) : "");
                    holder.readySub.setText(sub);
                }
            }


            if (holder.count != null) {
                holder.count.setVisibility(isTrend ? View.VISIBLE : View.GONE);
                holder.count.setText("" + item.getDownloadCount());
            }
        }
        if (item.getDownloadThumbnail() != null) {
            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getDownloadThumbnail(), (int) Util.convertDpToPixel(120, context), (int) Util.convertDpToPixel(68, context)));
            holder.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            Bitmap bmp = null;
            try {
                bmp = ThumbnailUtils.createVideoThumbnail(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""), MediaStore.Video.Thumbnails.MINI_KIND);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bmp != null) {
                holder.thumbnail.setImageBitmap(bmp);
                holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                holder.thumbnail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_videocam_gray_36));
                holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER);
            }
        }
    }

    @Override
    public ReDownloadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReDownloadHolder holder = new ReDownloadHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.editLayout = holder.fv(R.id.layout_edit);
        holder.deleteButton = holder.fv(R.id.item_btn_delete);
        holder.editButton = holder.fv(R.id.item_btn_edit);
        holder.clearButton = holder.fv(R.id.item_btn_clear);
        holder.root = holder.fv(R.id.layout_download);
        holder.check = holder.fv(R.id.checkbox);
        holder.line = holder.fv(R.id.line);
        holder.selBg = holder.fv(R.id.view_sel);
        holder.thumbnail = holder.fv(R.id.item_iv);
        holder.title = holder.fv(R.id.item_text_title);
        holder.subTitle = holder.fv(R.id.item_text_sub);
        holder.url = holder.fv(R.id.item_text_url);
        holder.downloadButton = holder.fv(R.id.item_btn);
        holder.progressBar = holder.fv(R.id.item_progress);
        holder.sizeText = holder.fv(R.id.text_size);
        holder.count = holder.fv(R.id.text_down_count);
        holder.readyTitle = holder.fv(R.id.item_text_ready_url);
        holder.readySub = holder.fv(R.id.item_text_ready_sub);
        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    listener.OnItemLongClick(position, items.get(position));
                return true;
            }
        });

        holder.setOnDownloadButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((DownloadAdapterListener) listener).onDownloadButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnDeleteButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((DownloadAdapterListener) listener).onDeleteButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnEditdButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((DownloadAdapterListener) listener).onEditButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnClearButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((DownloadAdapterListener) listener).onClearButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setTrend(boolean isTrend) {
        this.isTrend = isTrend;
    }

    public void updateProgress(DownItem item) {
        if (items == null || item == null)
            return;

        for (int i = 0; i < items.size(); i++)
            if (items.get(i) != null && items.get(i).getDownloadId() != null && items.get(i).getDownloadId().equalsIgnoreCase(item.getDownloadId())) {
                items.get(i).setDownloadFileRemainSize(item.getDownloadFileRemainSize());
                items.get(i).setDownloadState(item.getDownloadState());
                items.get(i).setDownloadCount(item.getDownloadCount());
                notifyItemChanged(i);

                return;
            }
    }

    public void insertDownloadItem(DownItem item) {
        if (item == null)
            return;

        if (isContains(item))
            return;

        items.add(0, item);
        notifyItemInserted(0);
    }

    public void removeDownloadItem(String id) {
        if (items == null)
            return;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i) != null && items.get(i).getDownloadId() != null && items.get(i).getDownloadId().equalsIgnoreCase(id)) {
                items.remove(i);
                notifyItemRemoved(i);

                return;
            }
        }
    }

    public long getRemainFileSize() {
        long result = 0;
        for (DownItem item : items)
            result += item.getDownloadFileRemainSize();

        return result;
    }

    public long getTotalFileSize() {
        long result = 0;
        for (DownItem item : items)
            result += item.getDownloadFileTotalSize();

        return result;

    }

    public void setEditId(String id) {
        for (int i = 0 ; i < items.size(); i++) {
            if (items.get(i).getDownloadId().equalsIgnoreCase(id)) {
                this.editIndex = i;
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void setEditIndex(int index) {
        this.editIndex = index;
        notifyDataSetChanged();
    }

    public int getEcitIndex() {
        return this.editIndex;
    }

    public boolean isContains(DownItem item) {
        if (items == null || item == null || item.getDownloadId() == null)
            return false;

        for (DownItem temp : items)
            if (temp != null && temp.getDownloadId() != null && temp.getDownloadId().equalsIgnoreCase(item.getDownloadId()))
                return true;

        return false;
    }

    public void setVideoMode(boolean mode) {
        this.isVideo = mode;
    }

    public static interface DownloadAdapterListener extends ReOnItemClickListener<DownItem> {
        public void onDownloadButtonClick(int position, DownItem item);

        public void onEditButtonClick(int position, DownItem item);

        public void onDeleteButtonClick(int position, DownItem item);

        public void onClearButtonClick(int position, DownItem item);
    }
}
