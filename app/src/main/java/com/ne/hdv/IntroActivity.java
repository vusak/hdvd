package com.ne.hdv;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.L;
import com.airbnb.lottie.LottieAnimationView;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.ne.hdv.base.BaseActivity;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.ne.hdv.common.LogUtil.DEBUG_MODE;


public class IntroActivity extends BaseActivity implements BillingProcessor.IBillingHandler {
//    LinearLayout premiumLayout;
    LinearLayout adsLayout;
    AdView adView;
    com.facebook.ads.AdView fanIntroBanner;

    FrameLayout nativeAdsLayout;
    NativeAd fanNativeAd;

    FrameLayout startLayout;
    Button startButton;
    LottieAnimationView lottie;

    Runnable initRunnable = () -> {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        sp.setIntroWidth(displayMetrics.widthPixels);

        Thread t = new Thread(() -> {
            try {
                File dir = new File(
                        Common.getVideoDir(IntroActivity.this));

                if (!dir.exists())
                    dir.mkdirs();
            } catch (Exception e) {
            }
        });
        t.start();

    };
    private BillingProcessor bp;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);

        MobileAds.initialize(this, LogUtil.DEBUG_MODE ? Common.ADMOB_APP_ID_TEST : Common.ADMOB_APP_ID);

        startLayout = fv(R.id.layout_start);
//        premiumLayout = fv(R.id.layout_premium);
        startButton = fv(R.id.button_start);
        startButton.setOnClickListener(v -> {
            Intent i = new Intent(IntroActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        });
        nativeAdsLayout = fv(R.id.layout_native_ads);
        adsLayout = fv(R.id.layout_ads_intro);

        lottie = fv(R.id.lottie_intro);
        lottie.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
//                premiumLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (sp.isPremiumMode()) {
                    Intent i = new Intent(IntroActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
                else {
                    Animation anim = new AlphaAnimation(0, 1);
                    anim.setDuration(1000);
                    startButton.setAnimation(anim);
                    startButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        if (Util.isNetworkAbailable(this)) {
            // premium
            String licenseKey = Common.INAPP_LICENSE_KEY;
            bp = new BillingProcessor(this, licenseKey, this);
        } else {
            lottie.playAnimation();
            if (sp.isPremiumMode()) {
                startLayout.setVisibility(View.GONE);
                adsLayout.setVisibility(View.GONE);
//                Handler handler = new Handler();
//                handler.postDelayed(() -> {
//                    Intent i = new Intent(IntroActivity.this, MainActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
//                    finish();
//                }, 0);
            } else {
                startLayout.setVisibility(View.VISIBLE);
                adsLayout.setVisibility(View.VISIBLE);
                adsLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initAds();
                    }
                }, 100);
//                lottie.playAnimation();
            }
        }
        Thread startThread = new Thread(initRunnable);
        startThread.start();
    }

    @Override
    protected void onPause() {
        if (adView != null)
            adView.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adView != null)
            adView.resume();
    }

    @Override
    protected void onDestroy() {
        if (bp != null) {
            bp.release();
        }

        if (adView != null) {
            adView.setAdListener(null);
            adView.destroyDrawingCache();
            adView.destroy();
            adView = null;
        }
        if (fanIntroBanner != null)
            fanIntroBanner.destroy();
        super.onDestroy();
//        if (fanNativeAd != null)
//            fanNativeAd.destroy();
    }

    private void initAds() {
//        if (nativeAdsLayout != null)
//            nativeAdsLayout.removeAllViews();
//        onLoadAdmobAdvanced();
        try {
            adView = new AdView(this);
            adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
            adView.setAdUnitId(DEBUG_MODE ? Common.ADMOB_MID_INTRO_TEST : Common.ADMOB_MID_INTRO);
            AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
            // Optionally populate the ad request builder.
            adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            adsLayout.addView(adView);
            adView.loadAd(adRequestBuilder.build());
//            ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
//            scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
//                public void run() {
//                    runOnUiThread(new Runnable() {
//                        public void run() {
//                            AdRequest adRequest = new AdRequest.Builder().build();
//                            adView.loadAd(adRequest);
//                        }
//                    });
//                }
//
//            }, 0, 30, TimeUnit.SECONDS);
            adView.setAdListener(new com.google.android.gms.ads.AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    LogUtil.log("onAdFailedToLoad :: " + i);
                    if (adView != null)
                        adView.destroy();
                    if (adsLayout != null)
                        adsLayout.removeAllViews();

                    fanIntroBanner = new com.facebook.ads.AdView(IntroActivity.this, Common.FAN_INTRO, com.facebook.ads.AdSize.RECTANGLE_HEIGHT_250);
                    adsLayout.addView(fanIntroBanner);
                    fanIntroBanner.loadAd();

                    fanIntroBanner.setAdListener(new AbstractAdListener() {
                        @Override
                        public void onError(Ad ad, AdError adError) {
                            if (fanIntroBanner != null)
                                fanIntroBanner.destroy();

                            if (adsLayout != null)
                                adsLayout.removeAllViews();
                        }
                    });
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();

                    LogUtil.log("onAdLoaded :: ");
                }
            });


        } catch (Exception e) {
            Util.showToast(IntroActivity.this, e.getMessage(), true);
            LogUtil.log(e.getMessage());
        }
    }

    private void onFANAdLoad() {
        try {
            fanNativeAd = new NativeAd(this, Common.FAN_INTRO_NATIVE);
            fanNativeAd.setAdListener(new AbstractAdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    if (fanNativeAd != null)
                        fanNativeAd.destroy();

                    if (nativeAdsLayout != null)
                        nativeAdsLayout.removeAllViews();

                    onLoadAdmobAdvanced();
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (fanNativeAd.isAdLoaded())
                        fanNativeAd.unregisterView();

                    if (nativeAdsLayout == null)
                        return;

                    LayoutInflater inflater = LayoutInflater.from(IntroActivity.this);
                    // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
                    LinearLayout adView = (LinearLayout) inflater.inflate(R.layout.fan_native_intro, nativeAdsLayout, false);
                    nativeAdsLayout.addView(adView);

                    // Create native UI using the ad metadata.
                    ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
                    TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
                    MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
                    //TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
                    Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);

                    // Set the Text.
                    nativeAdTitle.setText(fanNativeAd.getAdTitle());
                    //nativeAdBody.setText(nativeAd.getAdBody());
                    nativeAdCallToAction.setText(fanNativeAd.getAdCallToAction());

                    // Download and display the ad icon.
                    NativeAd.Image adIcon = fanNativeAd.getAdIcon();
                    NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);

                    // Download and display the cover image.
                    nativeAdMedia.setNativeAd(fanNativeAd);

                    // Add the AdChoices icon
                    LinearLayout adChoicesContainer = fv(R.id.ad_choices_container);
                    AdChoicesView adChoicesView = new AdChoicesView(IntroActivity.this, fanNativeAd, true);
                    adChoicesContainer.addView(adChoicesView);

                    // Register the Title and CTA button to listen for clicks.
                    List<View> clickableViews = new ArrayList<>();
                    clickableViews.add(nativeAdTitle);
                    clickableViews.add(nativeAdCallToAction);
                    fanNativeAd.registerViewForInteraction(nativeAdsLayout, clickableViews);
                }
            });

            fanNativeAd.loadAd(NativeAd.MediaCacheFlag.ALL);

        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    private void onLoadAdmobAdvanced() {
        AdLoader adLoader = new AdLoader.Builder(this, LogUtil.DEBUG_MODE ? Common.ADMOB_INTRO_NATIVE_TEST : Common.ADMOB_INTRO_NATIVE)
                .forAppInstallAd(nativeAppInstallAd -> {
                    if (nativeAppInstallAd == null || getLayoutInflater() == null)
                        return;
                    NativeAppInstallAdView adView = (NativeAppInstallAdView) getLayoutInflater().inflate(R.layout.admob_app_install_intro, null);
                    populateAppInstallAdView(nativeAppInstallAd, adView);
                    if (nativeAdsLayout != null) {
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .forContentAd(nativeContentAd -> {
                    if (nativeContentAd == null || getLayoutInflater() == null)
                        return;

                    NativeContentAdView adView = (NativeContentAdView) getLayoutInflater()
                            .inflate(R.layout.admob_content_intro, null);
                    populateContentAdView(nativeContentAd, adView);
                    if (nativeAdsLayout != null) {
                        nativeAdsLayout.removeAllViews();
                        nativeAdsLayout.addView(adView);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(int i) {
                        if (nativeAdsLayout != null)
                            nativeAdsLayout.removeAllViews();

                        onFANAdLoad();
//                        super.onAdFailedToLoad(i);
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder().build())
                .build();
        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAppInstallAd.getVideoController();

        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.appinstall_media);
        adView.setMediaView(mediaView);

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }

    private void populateContentAdView(NativeContentAd nativeContentAd,
                                       NativeContentAdView adView) {

        adView.setHeadlineView(adView.findViewById(R.id.contentad_headline));
        adView.setImageView(adView.findViewById(R.id.contentad_image));
        adView.setBodyView(adView.findViewById(R.id.contentad_body));
        adView.setLogoView(adView.findViewById(R.id.contentad_logo));
        adView.setCallToActionView(adView.findViewById(R.id.contentad_call_to_action));

        // Some assets are guaranteed to be in every NativeContentAd.
        ((TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());

        if (nativeContentAd.getImages().size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(nativeContentAd.getImages().get(0).getDrawable());
        }

        // The MediaView will display a video asset if one is present in the ad, and the first image
        // asset otherwise.
        com.google.android.gms.ads.formats.MediaView mediaView = (com.google.android.gms.ads.formats.MediaView) adView.findViewById(R.id.contentad_media);
        adView.setMediaView(mediaView);

        // Some aren't guaranteed, however, and should be checked.

        if (nativeContentAd.getLogo() == null) {
            adView.getLogoView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getLogoView()).setImageDrawable(nativeContentAd.getLogo().getDrawable());
            adView.getLogoView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeContentAd);
    }

    @Override
    public void onBillingInitialized() {
        // BillingProcessor 초기화, 구매 준비 완료
        if (DEBUG_MODE)
            Util.showToast(this, "INTRO onBillingInitialized", false);

        if (DEBUG_MODE)
            Util.showToast(this, "purchased ? " + bp.isPurchased(Common.PREMIUM_DOWNLOADER_ID), true);
        sp.setPremiumMode(bp.isPurchased(Common.PREMIUM_DOWNLOADER_ID));
        lottie.playAnimation();
        if (sp.isPremiumMode()) {
            adsLayout.setVisibility(View.GONE);
            startLayout.setVisibility(View.GONE);
//            Handler handler = new Handler();
//            handler.postDelayed(() -> {
//                Intent i = new Intent(IntroActivity.this, MainActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(i);
//                finish();
//            }, 0);
        } else {
            adsLayout.setVisibility(View.VISIBLE);
            startLayout.setVisibility(View.VISIBLE);
            adsLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    initAds();
                }
            }, 100);

//            lottie.playAnimation();
        }
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        // 구매 성공시 호출
        if (DEBUG_MODE)
            Util.showToast(this, "INTRO onProductPurchased", false);
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        // 구매시 오류 발생했을때 호출
        // Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        if (DEBUG_MODE)
            Util.showToast(this, "INTRO onBillingError", false);
    }

    @Override
    public void onPurchaseHistoryRestored() {
        // Called when purchase history was restored and the list of all owned PRODUCT ID's was loaded from Google Play
        if (DEBUG_MODE)
            Util.showToast(this, "INTRO onPurchaseHistoryRestored", false);
    }
}


