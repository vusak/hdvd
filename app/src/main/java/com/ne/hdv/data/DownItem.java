package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownItem extends TableObject {
    public static final String TABLE_NAME = "downlaod";

    public static final String ROW_ID = "id";
    public static final String DOWNLOAD_ID = "dounload_id";

    public static final String DOWNLOAD_URL = "download_url";
    public static final String DOWNLOAD_SITE = "download_site";
    public static final String DOWNLOAD_FILE_PATH = "download_file_path";
    public static final String DOWNLOAD_FILE_NAME = "download_file_name";
    public static final String DOWNLOAD_FILE_EXT = "download_file_ext";
    public static final String DOWNLOAD_FILE_TOTAL_SIZE = "download_file_total_size";
    public static final String DOWNLOAD_FILE_REMAIN_SIZE = "download_file_remain_size";
    public static final String DOWNLOAD_FILE_THUMBNAIL_PATH = "download_file_thumbnail_path";
    public static final String DOWNLOAD_THUMBNAIL = "download_thumbnail";
    public static final String DOWNLOAD_FILE_TIME = "download_file_time";

    public static final String DOWNLOAD_STATE = "download_state";
    public static final String DOWNLOAD_PROGRESS = "download_progress";
    public static final String DOWNLOAD_AT = "download_at";
    public static final String DOWNLOAD_START_AT = "download_start_at";

    public static final String DOWNLOAD_TRENDS_COUNT = "download_trend_count";

    public static final int DOWNLOAD_STATE_NONE = 0;
    public static final int DOWNLOAD_STATE_WAITING = 1;
    public static final int DOWNLOAD_STATE_HAS_DOWNLOAD = 2;
    public static final int DOWNLOAD_STATE_DOWNLOADING = 3;
    public static final int DOWNLOAD_STATE_PAUSE = 4;
    public static final int DOWNLOAD_STATE_DONE = 5;
    public static final Parcelable.Creator<DownItem> CREATOR = new Creator<DownItem>() {
        @Override
        public DownItem createFromParcel(Parcel source) {
            return new DownItem(source);
        }

        @Override
        public DownItem[] newArray(int size) {
            return new DownItem[size];
        }
    };
    private long id;
    private String downId;
    private String downUrl;
    private String downSite;
    private String downFilePath;
    private String downFileName;
    private String downFileExt;
    private long downFileTime;
    private long downFileTotalSize;
    private long downFileRemainSize;
    private long before;
    private String downFileThumbnailPath;
    private byte[] thumbnail;
    private int downState = DOWNLOAD_STATE_NONE;
    private float downProgress;
    private int downAt;
    private int downStartAt;
    private int count;

    public DownItem() {
        super();
    }

    public DownItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.downId = in.readString();
        this.downUrl = in.readString();
        this.downSite = in.readString();
        this.downFilePath = in.readString();
        this.downFileName = in.readString();
        this.downFileExt = in.readString();
        this.downFileTime = in.readLong();
        this.downFileTotalSize = in.readLong();
        this.downFileRemainSize = in.readLong();
        this.downFileThumbnailPath = in.readString();
        in.readByteArray(this.thumbnail);
        this.downState = in.readInt();
        this.downProgress = in.readFloat();
        this.count = in.readInt();
        this.downAt = in.readInt();
        this.downStartAt = in.readInt();
    }

    public static void createTableDownload(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(DOWNLOAD_ID + " text not null unique, ")
                .append(DOWNLOAD_URL + " text not null, ")
                .append(DOWNLOAD_SITE + " text, ")
                .append(DOWNLOAD_FILE_PATH + " text, ")
                .append(DOWNLOAD_FILE_NAME + " text, ")
                .append(DOWNLOAD_FILE_EXT + " text, ")
                .append(DOWNLOAD_FILE_TIME + " integer, ")
                .append(DOWNLOAD_FILE_TOTAL_SIZE + " integer, ")
                .append(DOWNLOAD_FILE_REMAIN_SIZE + " integer, ")
                .append(DOWNLOAD_FILE_THUMBNAIL_PATH + " text, ")
                .append(DOWNLOAD_THUMBNAIL + " BLOB, ")
                .append(DOWNLOAD_STATE + " integer not null, ")
                .append(DOWNLOAD_PROGRESS + " float, ")
                .append(DOWNLOAD_TRENDS_COUNT + " integer, ")
                .append(DOWNLOAD_AT + " integer, ")
                .append(DOWNLOAD_START_AT + " integer) ")
                .toString());

    }

    // DB
    public static void updateAddColumnVer2to3(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + DOWNLOAD_FILE_TIME + " integer");
        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + DOWNLOAD_THUMBNAIL + " BLOB");
    }

    public static void updateAddColumnVer3to4(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + DOWNLOAD_TRENDS_COUNT + " integer");
    }

    public static boolean isDownloadFileName(SQLiteDatabase db, String name) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_FILE_NAME + " = ?", new String[]{name}, null, null, null, "1");

        if (list.size() <= 0)
            return false;

        boolean result = false;
        for (DownItem item : list)
            if (item.getDownloadState() != DOWNLOAD_STATE_DONE) {
                result = true;
                break;
            }

        return result;
    }

    public static DownItem getDownloadItem(SQLiteDatabase db, String id) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static boolean isDownloadedItem(SQLiteDatabase db, String url) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_URL + " = ?", new String[]{url}, null, null, null, "1");

        return list.size() > 0;
    }

    public static List<DownItem> getDownloadItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<DownItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                DownItem item = new DownItem()
                        .setRowId(c)
                        .setDownloadId(c)
                        .setDownloadUrl(c)
                        .setDownloadSite(c)
                        .setDownloadFilePath(c)
                        .setDownloadFileName(c)
                        .setDownloadFileExt(c)
                        .setDownloadFileTime(c)
                        .setDownloadFileTotalSize(c)
                        .setDownloadFileRemainSize(c)
                        .setDownloadFileThumbnailPath(c)
                        .setDownloadThumbnail(c)
                        .setDownloadState(c)
                        .setDownProgress(c)
                        .setDownloadCount(c)
                        .setDownloadAt(c)
                        .setDownloadStartAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean restateDownloadState(SQLiteDatabase db) {
        try {
            db.execSQL("update " + TABLE_NAME + " set " + DOWNLOAD_STATE + " = '" + DOWNLOAD_STATE_PAUSE + "' where " + DOWNLOAD_STATE + " = '" +
                    DOWNLOAD_STATE_DOWNLOADING + "'");

            db.execSQL("update " + TABLE_NAME + " set " + DOWNLOAD_STATE + " = '" + DOWNLOAD_STATE_PAUSE + "' where " + DOWNLOAD_STATE + " = '" +
                    DOWNLOAD_STATE_WAITING + "'");

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean cancelAllDownloadItems(SQLiteDatabase db) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " <> " + DOWNLOAD_STATE_DONE, null, null, null, null, null);
        if (list != null && list.size() > 0)
            for (DownItem item : list) {
                deleteDownloadItem(db, item);
            }

        return false;
    }

    public static boolean insertOrUpdateDownloadItem(SQLiteDatabase db, DownItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putDownloadId(v)
                    .putDownloadUrl(v)
                    .putDownloadSite(v)
                    .putDownloadFilePath(v)
                    .putDownloadFIleName(v)
                    .putDownloadFileExt(v)
                    .putDownloadFileTime(v)
                    .putDownloadFileTotalSize(v)
                    .putDownloadFileRemainSize(v)
                    .putDownloadFileThumbnailPath(v)
                    .putDownloadThumbnail(v)
                    .putDownloadState(v)
                    .putDownProgress(v)
                    .putDownloadCount(v)
                    .putDownloadAt(v)
                    .putDownloadStartAt(v);

            int rowAffected = db.update(TABLE_NAME, v, DOWNLOAD_ID + " =? ", new String[]{item.getDownloadId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getDownloadFileName());
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateDownloadState(SQLiteDatabase db, DownItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putDownloadState(v)
                    .putDownProgress(v)
                    .putDownloadFileTotalSize(v)
                    .putDownloadFileRemainSize(v)
                    .putDownloadAt(v)
                    .putDownloadStartAt(v);

            db.update(TABLE_NAME, v, DOWNLOAD_ID + " = ?", new String[]{item.getDownloadId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteDownloadItem(SQLiteDatabase db, DownItem item) {
        try {
            db.delete(TABLE_NAME, DOWNLOAD_ID + " = ? ", new String[]{item.getDownloadId()});

            try {
                File vFile = new File(item.getDownloadFilePath());
                if (vFile.exists())
                    FileUtil.delete(vFile);
                File cFile = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (cFile.exists())
                    FileUtil.delete(cFile);
                File tFile = new File(item.getDownloadFileThumbnailPath());
                if (tFile.exists())
                    FileUtil.delete(tFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllNonDownloadItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_NONE)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteAllDownloadedItems(SQLiteDatabase db) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_DONE)}, null, null, null, null);
        try {
            db.delete(TABLE_NAME, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_DONE)});

            for (DownItem item : list) {
                File vFile = new File(item.getDownloadFilePath());
                if (vFile.exists())
                    FileUtil.delete(vFile);

                File cFile = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (cFile.exists())
                    FileUtil.delete(cFile);

                File tFile = new File(item.getDownloadFileThumbnailPath());
                if (tFile.exists())
                    FileUtil.delete(tFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteDownloadedItems(SQLiteDatabase db, ArrayList<DownItem> list) {
        for (DownItem item : list)
            deleteDownloadItem(db, item);
        return false;
    }

    public static int readyDownloadItemCount(SQLiteDatabase db) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + " = ?", new String[]{String.valueOf(DOWNLOAD_STATE_NONE)}, null, null, null, null);

        return list.size();
    }

    public static int downloadingItemCount(SQLiteDatabase db) {
        List<DownItem> list = getDownloadItems(db, null, DOWNLOAD_STATE + "= ?", new String[]{String.valueOf(DOWNLOAD_STATE_DOWNLOADING)}, null, null, null, null);

        List<DownItem> wList = getDownloadItems(db, null, DOWNLOAD_STATE + "= ?", new String[]{String.valueOf(DOWNLOAD_STATE_WAITING)}, null, null, null, null);
        return list.size() + wList.size();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(downId);
        out.writeString(downUrl);
        out.writeString(downSite);
        out.writeString(downFilePath);
        out.writeLong(downFileTime);
        out.writeString(downFileName);
        out.writeString(downFileExt);
        out.writeLong(downFileTotalSize);
        out.writeLong(downFileRemainSize);
        out.writeString(downFileThumbnailPath);
        out.writeByteArray(thumbnail);
        out.writeInt(downState);
        out.writeFloat(downProgress);
        out.writeInt(count);
        out.writeInt(downAt);
        out.writeInt(downStartAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DownItem && downId == ((DownItem) obj).downId;
    }

    public long getRowId() {
        return this.id;
    }

    public DownItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public DownItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public DownItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getDownloadId() {
        return this.downId;
    }

    public DownItem setDownloadId(String downId) {
        this.downId = downId;

        return this;
    }

    public DownItem setDownloadId(Cursor c) {
        this.downId = s(c, DOWNLOAD_ID);

        return this;
    }

    public DownItem putDownloadId(ContentValues v) {
        v.put(DOWNLOAD_ID, downId);

        return this;
    }

    public String getDownloadUrl() {
        return this.downUrl;
    }

    public DownItem setDownloadUrl(String url) {
        this.downUrl = url;

        return this;
    }

    public DownItem setDownloadUrl(Cursor c) {
        this.downUrl = s(c, DOWNLOAD_URL);

        return this;
    }

    public DownItem putDownloadUrl(ContentValues v) {
        v.put(DOWNLOAD_URL, downUrl);

        return this;
    }

    public String getDownloadSite() {
        return this.downSite;
    }

    public DownItem setDownloadSite(String site) {
        this.downSite = site;

        return this;
    }

    public DownItem setDownloadSite(Cursor c) {
        this.downSite = s(c, DOWNLOAD_SITE);

        return this;
    }

    public DownItem putDownloadSite(ContentValues v) {
        v.put(DOWNLOAD_SITE, downSite);

        return this;
    }

    public String getDownloadFilePath() {
        return this.downFilePath;
    }

    public DownItem setDownloadFilePath(String filePath) {
        this.downFilePath = filePath;

        return this;
    }

    public DownItem setDownloadFilePath(Cursor c) {
        this.downFilePath = s(c, DOWNLOAD_FILE_PATH);

        return this;
    }

    public DownItem putDownloadFilePath(ContentValues v) {
        v.put(DOWNLOAD_FILE_PATH, downFilePath);

        return this;
    }

    public String getDownloadFileName() {
        return this.downFileName;
    }

    public DownItem setDownloadFileName(String fileName) {
        this.downFileName = fileName;

        return this;
    }

    public DownItem setDownloadFileName(Cursor c) {
        this.downFileName = s(c, DOWNLOAD_FILE_NAME);

        return this;
    }

    public DownItem putDownloadFIleName(ContentValues v) {
        v.put(DOWNLOAD_FILE_NAME, downFileName);

        return this;
    }

    public String getDownloadFileExt() {
        return this.downFileExt;
    }

    public DownItem setDownloadFileExt(String fileExt) {
        this.downFileExt = fileExt;

        return this;
    }

    public DownItem setDownloadFileExt(Cursor c) {
        this.downFileExt = s(c, DOWNLOAD_FILE_EXT);

        return this;
    }

    public DownItem putDownloadFileExt(ContentValues v) {
        v.put(DOWNLOAD_FILE_EXT, downFileExt);

        return this;
    }

    public long getDownloadFileTime() {
        return this.downFileTime;
    }

    public DownItem setDownloadFileTime(long time) {
        this.downFileTime = time;

        return this;
    }

    public DownItem setDownloadFileTime(Cursor c) {
        this.downFileTime = l(c, DOWNLOAD_FILE_TIME);

        return this;
    }

    public DownItem putDownloadFileTime(ContentValues v) {
        v.put(DOWNLOAD_FILE_TIME, downFileTime);

        return this;
    }

    public long getDownloadFileTotalSize() {
        return this.downFileTotalSize;
    }

    public DownItem setDownloadFileTotalSize(long totalSize) {
        this.downFileTotalSize = totalSize;

        return this;
    }

    public DownItem setDownloadFileTotalSize(Cursor c) {
        this.downFileTotalSize = l(c, DOWNLOAD_FILE_TOTAL_SIZE);

        return this;
    }

    public DownItem putDownloadFileTotalSize(ContentValues v) {
        v.put(DOWNLOAD_FILE_TOTAL_SIZE, downFileTotalSize);

        return this;
    }

    public long getDownloadFileRemainSize() {
        return this.downFileRemainSize;
    }

    public DownItem setDownloadFileRemainSize(long remainSize) {
        this.downFileRemainSize = remainSize;

        if (downFileRemainSize < before)
            downFileRemainSize = before;
        else
            before = downFileRemainSize;

        return this;
    }

    public DownItem setDownloadFileRemainSize(Cursor c) {
        this.downFileRemainSize = l(c, DOWNLOAD_FILE_REMAIN_SIZE);

        if (downFileRemainSize < before)
            downFileRemainSize = before;
        else
            before = downFileRemainSize;
        return this;
    }

    public DownItem putDownloadFileRemainSize(ContentValues v) {
        v.put(DOWNLOAD_FILE_REMAIN_SIZE, downFileRemainSize);

        return this;
    }

    public String getDownloadFileThumbnailPath() {
        return this.downFileThumbnailPath;
    }

    public DownItem setDownloadFileThumbnailPath(String path) {
        this.downFileThumbnailPath = path;

        return this;
    }

    public DownItem setDownloadFileThumbnailPath(Cursor c) {
        this.downFileThumbnailPath = s(c, DOWNLOAD_FILE_THUMBNAIL_PATH);

        return this;
    }

    public DownItem putDownloadFileThumbnailPath(ContentValues v) {
        v.put(DOWNLOAD_FILE_THUMBNAIL_PATH, downFileThumbnailPath);

        return this;
    }

    public byte[] getDownloadThumbnail() {
        return this.thumbnail;
    }

    public DownItem setDownloadThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public DownItem setDownloadThumbnail(Cursor c) {
        this.thumbnail = blob(c, DOWNLOAD_THUMBNAIL);

        return this;
    }

    public DownItem putDownloadThumbnail(ContentValues v) {
        v.put(DOWNLOAD_THUMBNAIL, thumbnail);

        return this;
    }

    public int getDownloadState() {
        return this.downState;
    }

    public DownItem setDownloadState(int state) {
        this.downState = state;

        return this;
    }

    public DownItem setDownloadState(Cursor c) {
        this.downState = i(c, DOWNLOAD_STATE);

        return this;
    }

    public DownItem putDownloadState(ContentValues v) {
        v.put(DOWNLOAD_STATE, downState);

        return this;
    }

    public float getDownProgress() {
        return this.downProgress;
    }

    public DownItem setDownProgress(float progress) {
        this.downProgress = progress;

        return this;
    }

    public DownItem setDownProgress(Cursor c) {
        this.downProgress = f(c, DOWNLOAD_PROGRESS);

        return this;
    }

    public DownItem putDownProgress(ContentValues v) {
        v.put(DOWNLOAD_PROGRESS, downProgress);

        return this;
    }

    public int getDownloadCount() {
        return this.count;
    }

    public DownItem setDownloadCount(int count) {
        this.count = count;

        return this;
    }

    public DownItem setDownloadCount(Cursor c) {
        this.count = i(c, DOWNLOAD_TRENDS_COUNT);

        return this;
    }

    public DownItem putDownloadCount(ContentValues v) {
        v.put(DOWNLOAD_TRENDS_COUNT, count);

        return this;
    }

    public int getDownloadAt() {
        return this.downAt;
    }

    public DownItem setDownloadAt(int downloadAt) {
        this.downAt = downloadAt;

        return this;
    }

    public DownItem setDownloadAt(Cursor c) {
        this.downAt = i(c, DOWNLOAD_AT);

        return this;
    }

    public DownItem putDownloadAt(ContentValues v) {
        v.put(DOWNLOAD_AT, downAt);

        return this;
    }

    public int getDownloadStartAt() {
        return this.downStartAt;
    }

    public DownItem setDownloadStartAt(int downloadStartAt) {
        this.downStartAt = downloadStartAt;

        return this;
    }

    public DownItem setDownloadStartAt(Cursor c) {
        this.downStartAt = i(c, DOWNLOAD_START_AT);

        return this;
    }

    public DownItem putDownloadStartAt(ContentValues v) {
        v.put(DOWNLOAD_START_AT, downStartAt);

        return this;
    }

}
