package com.ne.hdv.download;

import com.ne.hdv.common.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Vimeo
 **/
public class VDownloadLinkProvider {

    public static final String DEFAULT_MOBILE_USER_AGENT_STRING
            = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

    public static final String VIDEO_LINK_PREFIX = "https://player.vimeo.com/video/";
    public static final String SD_QUALITY = "sd";

    private String source;
    private String userAgent = DEFAULT_MOBILE_USER_AGENT_STRING;
    private String vimeoId;
    private String videoLink;
    private String quality = SD_QUALITY;

    public VDownloadLinkProvider(String source) throws ParseException {
        this.source = source;
        vimeoId = extractVimeoId(source);
        videoLink = VIDEO_LINK_PREFIX + vimeoId + "/config";
    }

    public String generateDownloadLink() {
        try {
            String videoPage = getText(videoLink);
            JSONObject ob = new JSONObject(videoPage);
            String str = Util.optString(ob, "request");
            ob = new JSONObject(str);
            str = Util.optString(ob, "files");
            ob = new JSONObject(str);
            JSONArray arr = ob.getJSONArray("progressive");
            ob = arr.getJSONObject(arr.length() - 1);
            return Util.optString(ob, "url");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    protected String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        connection.setRequestProperty("User-Agent", userAgent);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Referrer", url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    private String extractVimeoId(String src) throws ParseException {
        Pattern p = Pattern.compile("((http://|https://){0,1}vimeo.com/(m/){0,1}){0,1}(\\d+)[^0-9]*");
        Matcher m = p.matcher(src);
        if (m.find()) {
            return m.group(4);
        } else {
            throw new ParseException(src, -1);
        }
    }

}