package com.ne.hdv.common;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

public class ReportCompleteDialog extends BaseDialogFragment {

    public static final String TAG = ReportCompleteDialog.class.getSimpleName();

    private TextView urlText;
    private Button okButton;
    private String url;

    public static ReportCompleteDialog newInstance(String tag, String url) {
        ReportCompleteDialog d = new ReportCompleteDialog();

        d.url = url;
        d.createArguments(tag);

        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_report_complete;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() == null)
            return;

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (getDialog() == null || getDialog().getWindow() == null)
                return;

            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.85f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initializeViews() {
        urlText = fv(R.id.text_report_url);
        urlText.setText(url);

        okButton = fv(R.id.button_thx);
        okButton.setOnClickListener(v -> dismiss());
    }
}
