package com.ne.hdv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Cecilia on 2017-05-16.
 */

public abstract class AbstractArrayAdapter<T> extends ArrayAdapter<T> {
    private int layoutId;

    private View dummyView;

    public AbstractArrayAdapter(Context context, int layoutId, List<T> items) {
        super(context, layoutId, items);

        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null || (position != 0 && dummyView == convertView)) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutId, null);
            convertView.setTag(initView(convertView));
        }

        if (position == 0 && dummyView == null)
            dummyView = convertView;

        AbstractViewHolder holder = (AbstractViewHolder) convertView.getTag();
        holder.position = position;
        setView(holder, position, getItem(position));

        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        dummyView = null;
        super.notifyDataSetChanged();
    }

    public abstract AbstractViewHolder initView(View convertView);

    public abstract void setView(AbstractViewHolder abstractViewHolder, int position, T item);

    public static abstract class AbstractViewHolder {
        int position;
    }
}
