package com.ne.hdv.adapter;

import android.content.Context;

import com.ne.hdv.data.Selectable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cecil on 2017-05-17.
 */

public abstract class ReSelectableAdapter<T extends Selectable, VH extends ReAbstractViewHolder> extends ReAdapter<T, VH> {
    public static final int CHOICE_SINGLE = 0;
    public static final int CHOICE_SINGLE_ENABLE_OFF = 1;
    public static final int CHOICE_MULTIPLE = 2;
    protected boolean checkMode = false;
    private int selectMode;
    private List<T> selectedList;

    public ReSelectableAdapter(int layoutId, List<T> items, Context context) {
        super(layoutId, items, context);
        selectedList = new ArrayList<>();
    }

    public void setSelectMode(int selectMode) {
        this.selectMode = selectMode;
    }

    public List<T> getSelectedList() {
        return selectedList;
    }

    public T getFirstSelectedItem() {
        synchronized (selectedList) {
            if (selectedList.size() > 0)
                return selectedList.get(selectedList.size() - 1);

            return null;
        }
    }

    public void clearSelection() {
        synchronized (selectedList) {
            for (T selectedItem : selectedList) {
                selectedItem.setSelected(false);
                notifyItemChanged(items.indexOf(selectedItem));
            }
            selectedList.clear();
        }
    }

    public void setAllSelection() {
        selectedList.clear();
        for (T item : items) {
            item.setSelected(true);
            selectedList.add(item);
        }
        notifyDataSetChanged();
    }

    public void select(int position) {
        T item = null;
        try {
            item = items.get(position);
        } catch (Exception e) {
            return;
        }

        synchronized (selectedList) {
            boolean contains = selectedList.contains(item);

            if (contains) {
                if (selectMode == CHOICE_SINGLE)
                    return;

                item.setSelected(false);
                selectedList.remove(item);
            } else {
                if (selectMode != CHOICE_MULTIPLE)
                    clearSelection();

                item.setSelected(true);
                selectedList.add(item);
            }
        }

        if (item == null || !item.isEnabled())
            return;

        notifyItemChanged(position);
    }

    public void updateSelectedItems() {
        for (T item : items) {
            updateSelectedItem(item);
        }
        notifyDataSetChanged();
    }

    private void updateSelectedItem(T item) {
        synchronized (selectedList) {
            boolean contains = selectedList.contains(item);

            if (item.isSelected() && !contains)
                selectedList.add(item);
            else if (!item.isSelected() && contains)
                selectedList.remove(item);
        }
    }

    public boolean isCheckMode() {
        return checkMode;
    }

    public void setCheckMode(boolean checkMode) {
        this.checkMode = checkMode;

        notifyDataSetChanged();
    }

    public boolean isSelectedAll() {
        for (T item : items)
            if (item.isSelected() == false)
                return false;

        return true;
    }
}
