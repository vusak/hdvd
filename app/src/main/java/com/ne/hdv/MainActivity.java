package com.ne.hdv;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.ne.hdv.adapter.DownloadAdapter;
import com.ne.hdv.adapter.ReSelectableAdapter;
import com.ne.hdv.base.BaseActivity;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseViewPager;
import com.ne.hdv.base.ExpandLayout;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.common.PremiumDialog;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.DownItem;
import com.ne.hdv.data.ReportItem;
import com.ne.hdv.download.Api;
import com.ne.hdv.download.DownManager;
import com.ne.hdv.download.JNetworkMonitor;
import com.ne.hdv.fragment.DownloadCompleteFragment;
import com.ne.hdv.fragment.DownloadingFragment;
import com.ne.hdv.fragment.FrontFragment;
import com.ne.hdv.fragment.MenuFragment;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static com.ne.hdv.common.LogUtil.DEBUG_MODE;

public class MainActivity extends BaseActivity implements FrontFragment.FrontFragmentListener,
        DownloadingFragment.DownloadListener, JNetworkMonitor.OnChangeNetworkStatusListener,
        MenuFragment.MenuFragmentListener, BillingProcessor.IBillingHandler {

    public static final int FRAGMENT_HOME = 0;
    //    public static final int FRAGMENT_TREND = 1;
    public static final int FRAGMENT_DOWNLOAD = 1;
    public static final int FRAGMENT_FILE = 2;
    public static final int FRAGMENT_MENU = 3;
    private static final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 10001;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 10002;
    private static final int REQUEST_CODE_READ_PHONE_STATE = 10003;
    public static int CURRENT_FRAGMENT = FRAGMENT_HOME;
    public static SkuDetails premiumProduct;
    private static String[] PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static String[] PHONE_PERMISSIONS = {
            Manifest.permission.READ_PHONE_STATE
    };
    Uri URI;
    LinearLayout layoutBackground;
    RelativeLayout fullAdsLayout;
    ImageButton closeFullAdsButton;
    FrontFragment frontFragment;
    MenuFragment menuFragment;
    //    DownloadFragment downloadFragment;
    DownloadingFragment downloadFragment;
    DownloadCompleteFragment fileFragment;
    LinearLayout editModeLayout, mainMenuLayout;
    Button doneButton;
    LinearLayout homeButton, menuButton;
    ImageView homeImage, downImage, fileImage, menuImage;
    TextView homeText, downText, fileText, menuText;
    FrameLayout downloadButton, fileButton;
    TextView downCntText, fileCntText;
    //    MoPubView adMainBanner; //, adExitSquare; //, adGiftSquare;
    LinearLayout bannerAdLayout, exitAdLayout, readyBannerAdLayout;
    com.facebook.ads.AdView fanMainBanner, fanReadyBanner;
    AudioManager audioManager;
    ExpandLayout toastLayout;
    final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (toastLayout.isExpanded())
                toastLayout.collapse(true);
        }
    };
    TextView toastText;
    Api api;
    String checkUrl = "";
    ArrayList<String> videoNames = new ArrayList<>();
    private DownManager downManager;
    private JNetworkMonitor monitor;
    // Premium
    private BaseViewPager viewPager;
    private BackPressCloseHandler backPressCloseHandler;
    // Ready Download layout
    private LinearLayout readyDownLayout, closeReadyButton;
    private Button downloadReadyButton, clearReadyButton;
    private ExpandLayout readyDownExpand;
    private RecyclerView readyDownLv;
    private LinearLayoutManager readyDownManager;
    private ArrayList<DownItem> readyDownList;
    private DownloadAdapter readyDownAdapter;
    // 인앱결재
    private BillingProcessor bp;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main_status));
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        downManager = app.getDownloadManager();
        downManager.addListener(new DownManager.DownListener() {
            @Override
            public void onDownloadStart(DownItem item) {

            }

            @Override
            public void onDownloadProgress(DownItem item, long progress, long total) {

            }

            @Override
            public void onDownloadSuccess(DownItem item) {
                if (CURRENT_FRAGMENT != FRAGMENT_FILE) {
                    if (fileCntText != null) {
                        fileCntText.postDelayed(() -> {
                            sp.addNotiCompleteDownloadCount();
                            setCheckCompleteDownloadItem();
                            setCheckReadyDownloadItem();
                        }, 800);
                    }
                } else {
                    if (fileFragment != null)
                        fileFragment.refreshLIst();
                }
            }

            @Override
            public void onDownloadError(DownItem item, int errorCode) {

            }
        });
        monitor = app.getNetworkMonitor();
        monitor.setListener(this);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        init();
        if (!sp.isPremiumMode())
            initAds(); // 엑시트 & 하단 배너 광고 호출
        backPressCloseHandler = new BackPressCloseHandler(this);
        //loadHandler();

        if (!TextUtils.isEmpty(sp.getApiErrorLog()))
            api.sendErrorLog();

        // premium
        String licenseKey = Common.INAPP_LICENSE_KEY;
        bp = new BillingProcessor(this, licenseKey, this);
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_SEND_TREND) {

        }
        super.handleApiMessage(m);
    }

    protected void initAds() {
        /** Main Banner **/
        AdView admobMainBanner = new AdView(MainActivity.this);
        admobMainBanner.setAdSize(AdSize.BANNER);
        admobMainBanner.setAdUnitId(DEBUG_MODE ? Common.ADMOB_MAIN_BANNER_OPT_TEST : Common.ADMOB_MAIN_BANNER_OPT);
        admobMainBanner.loadAd(new AdRequest.Builder().build());
        admobMainBanner.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                if (bannerAdLayout != null)
                    bannerAdLayout.removeAllViews();

                fanMainBanner = new com.facebook.ads.AdView(MainActivity.this, Common.FAN_MAIN_BANNER, com.facebook.ads.AdSize.BANNER_HEIGHT_50);
                bannerAdLayout.addView(fanMainBanner);
                fanMainBanner.loadAd();

                fanMainBanner.setAdListener(new AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        if (fanMainBanner != null) {
                            fanMainBanner.destroy();
                        }

                        if (bannerAdLayout != null)
                            bannerAdLayout.removeAllViews();

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                    }
                });
            }
        });
        bannerAdLayout.addView(admobMainBanner);

        /** Ready Banner **/
        AdView admobReadyBanner = new AdView(MainActivity.this);
        admobReadyBanner.setAdSize(AdSize.BANNER);
        admobReadyBanner.setAdUnitId(DEBUG_MODE ? Common.ADMOB_DOWNLOAD_BANNER_TEST : Common.ADMOB_DOWNLOAD_BANNER);
        admobReadyBanner.loadAd(new AdRequest.Builder().build());
        admobReadyBanner.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                if (readyBannerAdLayout != null)
                    readyBannerAdLayout.removeAllViews();

                fanReadyBanner = new com.facebook.ads.AdView(MainActivity.this, Common.FAN_DOWNLOAD_BANNER, com.facebook.ads.AdSize.BANNER_HEIGHT_50);
                readyBannerAdLayout.addView(fanReadyBanner);
                fanReadyBanner.loadAd();

                fanReadyBanner.setAdListener(new AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        if (fanReadyBanner != null) {
                            fanReadyBanner.destroy();
                        }
                        readyBannerAdLayout.removeAllViews();

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                    }
                });
            }
        });
        readyBannerAdLayout.addView(admobReadyBanner);

        /** Exit AD **/
        try {
            AdView adView = new AdView(this);
            adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
            adView.setAdUnitId(DEBUG_MODE ? Common.ADMOB_EXIT_BANNER_TEST : Common.ADMOB_EXIT_BANNER);
            adView.loadAd(new AdRequest.Builder().build());
            adView.setAdListener(new com.google.android.gms.ads.AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                }
            });
            exitAdLayout.addView(adView);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int curVouume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (CURRENT_FRAGMENT == FRAGMENT_HOME) {
                    if (readyDownLayout != null && readyDownLayout.getVisibility() == View.VISIBLE) {
                        showReadyLayout(false);
                    } else if (editModeLayout != null && editModeLayout.getVisibility() == View.VISIBLE) {
                        editModeLayout.setVisibility(View.GONE);
                        mainMenuLayout.setVisibility(View.VISIBLE);
                        if (frontFragment != null)
                            frontFragment.setEditMode(false);
                    } else if (frontFragment == null || !frontFragment.setBackPress()) {
                        if (layoutBackground != null && layoutBackground.getVisibility() == View.VISIBLE)
                            layoutBackground.setVisibility(View.GONE);
                        else if (fullAdsLayout != null && fullAdsLayout.getVisibility() == View.VISIBLE)
                            fullAdsLayout.setVisibility(View.GONE);
                        else {
                            if (sp.isPremiumMode())
                                backPressCloseHandler.onBackPressed();
                            else
                                layoutBackground.setVisibility(View.VISIBLE);
                        }
                    }
                } else if (CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD) {
                    if (downloadFragment == null || !downloadFragment.setBackPress()) {
                        setBottomMenuLayout(FRAGMENT_HOME);
                    }
                } else if (CURRENT_FRAGMENT == FRAGMENT_FILE) {
                    if (fileFragment == null || !fileFragment.setBackPress()) {
                        setBottomMenuLayout(FRAGMENT_HOME);
                    }
                } else {
                    setBottomMenuLayout(FRAGMENT_HOME);
                }
                break;

            case KeyEvent.KEYCODE_VOLUME_UP:
                if (curVouume < maxVolume)
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, curVouume + 1, AudioManager.FLAG_SHOW_UI);
                break;

            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (curVouume > 0)
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, curVouume - 1, AudioManager.FLAG_SHOW_UI);
                break;
        }
        return true;
    }

    void init() {
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        viewPager = fv(R.id.viewpager);
        viewPager.setEnableSwipe(false);
        setupViewPager(viewPager);

        homeButton = fv(R.id.button_main_home);
        homeButton.setOnClickListener(v -> {
            if (CURRENT_FRAGMENT == FRAGMENT_HOME && frontFragment != null)
                frontFragment.setHomePress();
            else {
                if (CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD && downloadFragment != null)
                    downloadFragment.onPause();

                if (CURRENT_FRAGMENT == FRAGMENT_FILE && fileFragment != null)
                    fileFragment.onPause();

                if (CURRENT_FRAGMENT == FRAGMENT_MENU && menuFragment != null)
                    menuFragment.onPause();

                if (CURRENT_FRAGMENT != FRAGMENT_HOME && frontFragment != null)
                    frontFragment.onResume();

                setBottomMenuLayout(FRAGMENT_HOME);
            }
        });
        homeImage = fv(R.id.image_main_home);
        homeText = fv(R.id.text_main_home);

        downloadButton = fv(R.id.button_main_download);
        downloadButton.setOnClickListener(v -> {
            if (CURRENT_FRAGMENT == FRAGMENT_HOME && frontFragment != null)
                frontFragment.onPause();

            if (CURRENT_FRAGMENT == FRAGMENT_MENU && menuFragment != null)
                menuFragment.onPause();

            if (CURRENT_FRAGMENT != FRAGMENT_DOWNLOAD && downloadFragment != null)
                downloadFragment.onResume();

            if (CURRENT_FRAGMENT == FRAGMENT_FILE && fileFragment != null)
                fileFragment.onPause();

            setBottomMenuLayout(FRAGMENT_DOWNLOAD);
        });
        downImage = fv(R.id.image_main_download);
        downText = fv(R.id.text_main_download);
        downCntText = fv(R.id.text_bottom_down_count);

        fileButton = fv(R.id.button_main_file);
        fileButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CURRENT_FRAGMENT == FRAGMENT_HOME && frontFragment != null)
                    frontFragment.onPause();

                if (CURRENT_FRAGMENT == FRAGMENT_MENU && menuFragment != null)
                    menuFragment.onPause();

                if (CURRENT_FRAGMENT != FRAGMENT_FILE && fileFragment != null)
                    fileFragment.onResume();

                if (CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD && downloadFragment != null)
                    downloadFragment.onPause();

                setBottomMenuLayout(FRAGMENT_FILE);
                if (fileCntText != null) {
                    fileCntText.postDelayed(() -> {
                        sp.setNotiCompleteDownloadCount(0);
                        setCheckCompleteDownloadItem();
                    }, 800);
                }
            }
        });
        fileImage = fv(R.id.image_main_file);
        fileText = fv(R.id.text_main_file);
        fileCntText = fv(R.id.text_bottom_file_count);

        menuButton = fv(R.id.button_main_menu);
        menuButton.setOnClickListener(v -> {
            if (CURRENT_FRAGMENT == FRAGMENT_HOME && frontFragment != null)
                frontFragment.onPause();

            if (CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD && downloadFragment != null)
                downloadFragment.onPause();

            if (CURRENT_FRAGMENT == FRAGMENT_FILE && fileFragment != null)
                fileFragment.onPause();

            if (CURRENT_FRAGMENT != FRAGMENT_MENU && menuFragment != null)
                menuFragment.onResume();

            setBottomMenuLayout(FRAGMENT_MENU);
        });
        menuImage = fv(R.id.image_main_menu);
        menuText = fv(R.id.text_main_menu);


        layoutBackground = fv(R.id.layout_black);
        bannerAdLayout = fv(R.id.layout_ads_banner);
        readyBannerAdLayout = fv(R.id.layout_ads_banner_ready);
        exitAdLayout = fv(R.id.layout_ads_exit);
//        adMainBanner = fv(R.id.adview);
//        adExitSquare = fv(R.id.adview_exit);
//        adGiftSquare = fv(R.id.adview_gift);

        fullAdsLayout = fv(R.id.layout_full_ads);
        closeFullAdsButton = fv(R.id.button_full_ads);
        closeFullAdsButton.setOnClickListener(v -> fullAdsLayout.setVisibility(View.GONE));

        mainMenuLayout = fv(R.id.layout_main_menu);
        editModeLayout = fv(R.id.layout_edit_mode);
        doneButton = fv(R.id.button_done);
        doneButton.setOnClickListener(v -> {
            editModeLayout.setVisibility(View.GONE);
            mainMenuLayout.setVisibility(View.VISIBLE);
            if (frontFragment != null)
                frontFragment.setEditMode(false);
        });

        toastLayout = fv(R.id.layout_expend_toast);
        toastLayout.collapse(false);
        toastText = fv(R.id.text_toast);

        Button layoutExitOk = fv(R.id.layout_exit_ok);
        Button layoutExitCancel = fv(R.id.layout_exit_cancel);
        layoutExitOk.setOnClickListener(v -> finish());
        layoutExitCancel.setOnClickListener(v -> layoutBackground.setVisibility(View.GONE));

        readyDownLayout = fv(R.id.layout_ready_download);
        readyDownLayout.setVisibility(View.GONE);
        closeReadyButton = fv(R.id.button_close_ready_download);
        closeReadyButton.setOnClickListener(v -> {
            showReadyLayout(false);
        });
        downloadReadyButton = fv(R.id.button_ready_download);
        downloadReadyButton.setOnClickListener(v -> {
            if (readyDownList == null || readyDownList.size() < 1)
                return;

            if (readyDownList.size() == 1) {
                DownItem item = readyDownList.get(0);
                if (item == null)
                    return;

                db.insertOrUpdateDownloadItem(item);
                item = db.getDownloadItem(item.getDownloadId());
                downManager.addToRequestList(item.getDownloadId());
                readyDownList.clear();
                readyDownAdapter.notifyDataSetChanged();
                setCheckReadyDownloadItem();
                showReadyLayout(false);
                if (frontFragment != null)
                    frontFragment.showLottieButton(false);
            } else {
                if (readyDownAdapter != null) {
                    if (readyDownAdapter.getSelectedList() != null && readyDownAdapter.getSelectedList().size() > 0) {
                        for (DownItem item : readyDownAdapter.getSelectedList()) {
                            if (item == null)
                                continue;
                            readyDownList.remove(item);
                            db.insertOrUpdateDownloadItem(item);
                            item = db.getDownloadItem(item.getDownloadId());
                            downManager.addToRequestList(item.getDownloadId());
                        }
                        setCheckReadyDownloadItem();
                        showReadyLayout(false);
                        if (readyDownList.size() <= 0) {
                            if (frontFragment != null)
                                frontFragment.showLottieButton(false);
                        }
                    } else {
                        Util.showToast(MainActivity.this, r.s(R.string.message_no_selected_item), false);
                    }
                }
            }
        });
        clearReadyButton = fv(R.id.button_clear_ready_download);
        clearReadyButton.setOnClickListener(v -> {
            if (readyDownList != null)
                readyDownList.clear();
            readyDownAdapter.notifyDataSetChanged();
            showReadyLayout(false);
            if (frontFragment != null)
                frontFragment.showLottieButton(false);

        });
        readyDownExpand = fv(R.id.layout_expend_ready_download);
        readyDownExpand.collapse(false);
        readyDownLv = fv(R.id.lv_ready_download);
        readyDownManager = new GridLayoutManager(this, 1);
        readyDownLv.setLayoutManager(readyDownManager);
        readyDownList = new ArrayList<>();
        readyDownAdapter = new DownloadAdapter(this, R.layout.item_ready_download, readyDownList, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownItem item) {

            }

            @Override
            public void onDeleteButtonClick(int position, DownItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownItem item) {

            }

            @Override
            public void OnItemClick(int position, DownItem item) {
                if (readyDownAdapter.isCheckMode()) {
                    readyDownAdapter.select(position);
                }
            }

            @Override
            public void OnItemLongClick(int position, DownItem item) {

            }
        });
        readyDownAdapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
        readyDownAdapter.setCheckMode(true);
        readyDownLv.setAdapter(readyDownAdapter);

//        setBottomMenuLayout(FRAGMENT_HOME);
        /** 비디오Intent 받음 **/
        if (getIntent().getData() != null) {
            CURRENT_FRAGMENT = FRAGMENT_DOWNLOAD;
            setBottomMenuLayout(FRAGMENT_DOWNLOAD);
            URI = getIntent().getData();

            if (URI == null)
                return;

            if (URI.toString().matches("file:.*")) { // 스토리지내에 파일일 경우
                try {
                    URI = Uri.parse("file://" + getPathFromUri(URI));
                    // permission check here!!! //
                    if (!askForPermission()) {
//                        Toast.makeText(this, "NEED PERMISSION CHECK", Toast.LENGTH_SHORT).show();
                        return;
                    } else
                        showDownloadDialog(false, URI.toString(), URI.toString(), true);
                } catch (Exception e) {
                }
            } else {
                if (URI.getHost() == null)
                    return;

                // permission check here!!! //
                if (!askForPermission()) {
//                    Toast.makeText(this, "NEED PERMISSION CHECK", Toast.LENGTH_SHORT).show();
                    return;
                } else
                    showDownloadDialog(true, URI.getHost().toString(), URI.toString(), true);
            }


        } else {
            int status = getIntent().getIntExtra(Common.FRAG_INTENT_DOWNLOAD_STATUS, DownItem.DOWNLOAD_STATE_NONE);
            if (status == DownItem.DOWNLOAD_STATE_DOWNLOADING || status == DownItem.DOWNLOAD_STATE_PAUSE || status == DownItem.DOWNLOAD_STATE_DONE) {

//                if (downloadFragment != null) {
                if (downloadFragment != null && (status == DownItem.DOWNLOAD_STATE_DOWNLOADING || status == DownItem.DOWNLOAD_STATE_PAUSE))
                    setBottomMenuLayout(FRAGMENT_DOWNLOAD);
//                        downloadFragment.showDownloadTab(0);
                else if (fileFragment != null && status == DownItem.DOWNLOAD_STATE_DONE) {
                    setBottomMenuLayout(FRAGMENT_FILE);
                    if (fileCntText != null) {
                        fileCntText.postDelayed(() -> {
                            sp.setNotiCompleteDownloadCount(0);
                            setCheckCompleteDownloadItem();
                        }, 800);
                    }
                }
//                        downloadFragment.showDownloadTab(2);
//                }
//                CURRENT_FRAGMENT = FRAGMENT_DOWNLOAD;
            } else
                setBottomMenuLayout(FRAGMENT_HOME);
//                CURRENT_FRAGMENT = FRAGMENT_HOME;
        }
    }

    // isBrowser : 브라우저에서 다운로드할 경우
    public void showDownloadDialog(final boolean isBrowser, final String site, String url, boolean isDirect) {
        if (url == null || TextUtils.isEmpty(url) || url.matches("(?i).*youtube.*"))
            return;

        if (checkUrl.equalsIgnoreCase(url))
            return;

        String fUrl = url;
        String filename = URLUtil.guessFileName(url, "https", "video/mp4");
        if (TextUtils.isEmpty(filename))
            return;

        // facebook filtering
        if (url.contains("fbcdn.net")) {

            if (videoNames.contains(filename)) {
//                LogUtil.log("CONTAINS!!! " + filename);
                return;
            } else {
                videoNames.add(filename);
            }
            LogUtil.log("FILE NAME :: " + filename);

            if (url.contains("&bytestart=")) {
                int index = url.indexOf("&bytestart=");
                fUrl = url.substring(0, index);
            } else
                fUrl = url;
        }

        checkUrl = fUrl;
        LogUtil.log(checkUrl);

//        api.getDownloadUrl(site, false, false);
        String fileName = Common.PREFIX_DOWNLOAD_FILE + System.currentTimeMillis() + Common.checkVideoExtension(fUrl);
        if (!sp.isUseNotification()) {
            addDownloadList(isBrowser, site, fUrl, fileName, isDirect);
        } else {
//            if (fdf(Common.TAG_DIALOG_ADD_DOWNLOAD) != null)
//                return;

            // test //
            Bitmap bitmap = null;
            long time = 0;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(fUrl, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(fUrl);

                if (url.contains("fbcdn.net"))
                    bitmap = mediaMetadataRetriever.getFrameAtTime(2, MediaMetadataRetriever.OPTION_CLOSEST);
                String temp = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (!TextUtils.isEmpty(temp)) {
                    time = Long.parseLong(temp);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }

            if (url.contains("fbcdn.net") && bitmap == null)
                return;

            // check time filtering
            if (sp.getTimeFiltering() > 0) {
                long limit = 15 * sp.getTimeFiltering() * 1000;
                if (time < limit)
                    return;
            }

            DownItem item = new DownItem();
            if (url.contains("fbcdn.net")) {
                item.setDownloadFileTime(time);
                if (bitmap != null)
                    item.setDownloadThumbnail(ImageUtil.getImageBytes(bitmap));
            }
            item.setDownloadId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setDownloadUrl(fUrl);
            item.setDownloadSite(site);
            item.setDownloadFileName(fileName);
            item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
            item.setDownloadFileRemainSize(0);
            item.setDownloadFilePath(Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION);
            item.setDownloadFileThumbnailPath(Common.getVideoDir(app.getApplicationContext()) + "." + FileUtil.getFileName(item.getDownloadFileName()) + FileUtil.TEMP_THUMBNAIL_EXTENSION);
            readyDownList.add(item);
            if (readyDownExpand.isExpanded()) {
                readyDownAdapter.insertDownloadItem(item);

                readyDownLv.postDelayed(() -> {
                    if (readyDownList.size() > 1)
                        readyDownAdapter.setCheckMode(true);
                    else
                        readyDownAdapter.setCheckMode(false);
                }, 300);
            } else {
                if (readyDownList.size() > 1)
                    readyDownAdapter.setCheckMode(true);
                else
                    readyDownAdapter.setCheckMode(false);
//            readyDownAdapter.notifyDataSetChanged();
            }
            if (frontFragment != null)
                frontFragment.showLottieButton(true);
            new AddReadyDownloadItem().execute(item);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        setCheckReadyDownloadItem();
        setCheckCompleteDownloadItem();

        String locale, contry;
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0).getLanguage();
            contry = getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = getResources().getConfiguration().locale.getLanguage();
            contry = getResources().getConfiguration().locale.getCountry();
        }
        app.setConfiguration(locale, contry);
        super.onResume();
    }

    //갤러리에서 비디오 공유시 content 로 시작하는 동영상 파일 path로 변환
    public String getPathFromUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToNext();
        String path = cursor.getString(cursor.getColumnIndex("_data"));
        cursor.close();

        return path;
    }

    private void setBottomMenuLayout(int fragment) {
        switch (fragment) {
            case 0:
                CURRENT_FRAGMENT = FRAGMENT_HOME;
                break;

            case 1:
                CURRENT_FRAGMENT = FRAGMENT_DOWNLOAD;
                break;

            case 2:
                CURRENT_FRAGMENT = FRAGMENT_FILE;
                break;

            case 3:
                CURRENT_FRAGMENT = FRAGMENT_MENU;
                break;
        }
        viewPager.setCurrentItem(fragment);
        homeImage.setImageResource(fragment == FRAGMENT_HOME ? R.drawable.ic_home_blue_24 : R.drawable.ic_home_gray_24);
        homeText.setTextColor(ContextCompat.getColor(this, fragment == FRAGMENT_HOME ? R.color.main_blue : R.color.gray5_a100));
        downImage.setImageResource(fragment == FRAGMENT_DOWNLOAD ? R.drawable.ic_get_app_blue_24 : R.drawable.ic_get_app_gray_24);
        downText.setTextColor(ContextCompat.getColor(this, fragment == FRAGMENT_DOWNLOAD ? R.color.main_blue : R.color.gray5_a100));
        fileImage.setImageResource(fragment == FRAGMENT_FILE ? R.drawable.ic_folder_blue_24 : R.drawable.ic_folder_gray_24);
        fileText.setTextColor(ContextCompat.getColor(this, fragment == FRAGMENT_FILE ? R.color.main_blue : R.color.gray5_a100));
        menuImage.setImageResource(fragment == FRAGMENT_MENU ? R.drawable.ic_menu_blue_24 : R.drawable.ic_menu_gray_24);
        menuText.setTextColor(ContextCompat.getColor(this, fragment == FRAGMENT_MENU ? R.color.main_blue : R.color.gray5_a100));
        bannerAdLayout.setVisibility(fragment == FRAGMENT_HOME || sp.isPremiumMode() ? View.GONE : View.VISIBLE);
        readyBannerAdLayout.setVisibility(sp.isPremiumMode() ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onRefreshDownloadCount() {
        setCheckReadyDownloadItem();
        setCheckCompleteDownloadItem();
    }

    @Override
    public void onSendReportUrl(ReportItem item) {
        if (askForPhonePermission())
            api.sendReportUrl(item, false, false);
    }

    @Override
    public void onGiftAdsButtonClicked() {
        if (fullAdsLayout != null)
            fullAdsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStartPremiumMode() {
        if (bp == null)
            return;

        if (bp.isInitialized()) {
            bp.purchase(this, Common.PREMIUM_DOWNLOADER_ID);
        }

    }

    @Override
    public void onResetPremiumMode() {
        if (bp == null || !DEBUG_MODE)
            return;

        if (bp.isInitialized()) {
            if (bp.isPurchased(Common.PREMIUM_DOWNLOADER_ID))
                bp.consumePurchase(Common.PREMIUM_DOWNLOADER_ID);

            sp.setPremiumMode(false);
            sp.setShowPremiumBanner(true);
            sp.setShowPremiumBannerCount(0);
            sp.setDeletePremiumBannerTime(0);
            sp.setShowPremiumDialogTime(0);
            sp.setShowPremiumDialogCount(0);
            sp.setPremiumDownloadCount(0);
            Util.showToast(this, "Clear Premium Mode", false);
            Intent intent = new Intent(this, IntroActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onNetworkChanged(int status) {
        if (downManager == null || downManager.isEmpty())
            return;

        if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            showAlertMessageDialog(r.s(R.string.msg_dlg_check_network));
            if (downManager != null)
                downManager.cancelAll(null);
            downManager.cancelAll(null);
            db.restateDownloadState();

//            if (downloadFragment != null && CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD)
//                downloadFragment.refreshList();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE && !sp.isUseCellularData()) {
            showAlertMessageDialog(r.s(R.string.msg_dlg_check_wifi_only));
            if (downManager != null)
                downManager.cancelAll(null);
            downManager.cancelAll(null);
            db.restateDownloadState();

//            if (downloadFragment != null && CURRENT_FRAGMENT == FRAGMENT_DOWNLOAD)
//                downloadFragment.refreshList();
        }
    }

    @Override
    public void setEditMode(boolean edit) {
        editModeLayout.setVisibility(edit ? View.VISIBLE : View.GONE);
        mainMenuLayout.setVisibility(edit ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onShowToastMessage(String message) {
        showToast(message);
    }

    @Override
    public void onReadyButtonClicked() {
        showReadyLayout(true);
    }

    @Override
    public void onReadyListClear() {
        LogUtil.log("FILE NAME :: clear");
        if (videoNames == null)
            videoNames = new ArrayList<>();
        else
            videoNames.clear();

        if (readyDownList != null)
            readyDownList.clear();
        readyDownAdapter.notifyDataSetChanged();
        if (readyDownLayout != null && readyDownLayout.getVisibility() == View.VISIBLE)
            showReadyLayout(false);
    }

    @Override
    public void onShowDownloadDialog(boolean isBrowser, String site, String url) {
        // permission check here!!! //
        if (!askForPermission()) {
//            Toast.makeText(this, "NEED PERMISSION CHECK", Toast.LENGTH_SHORT).show();
            return;
        } else {
            showDownloadDialog(isBrowser, site, url, false);
        }
    }

    public void showAlertMessageDialog(String msg) {
        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle("Download");
            dlg.setPositiveLabel(r.s(R.string.ok));
            dlg.setMessage(msg);

            sdf(dlg);
        }
    }

    private void showReadyLayout(boolean show) {
        if (show) {
            int count = readyDownList.size();
            readyDownAdapter.setCheckMode(count > 1);
            readyDownAdapter.clearSelection();
            readyDownExpand.expand(true);
        } else {
            readyDownExpand.collapse(true);
        }
        readyDownLayout.postDelayed(() -> readyDownLayout.setVisibility(show ? View.VISIBLE : View.GONE), show ? 0 : 300);
    }

    private void setCheckReadyDownloadItem() {
        if (downCntText == null)
            return;

        int cnt = db.getReadyDownloadItemCount();
        if (sp.isUseNotification())
            cnt = db.getDownloadingItemCount();

        if (cnt >= 100)
            downCntText.setText(":)");
        else
            downCntText.setText(String.valueOf(cnt));
        downCntText.setVisibility(cnt > 0 ? View.VISIBLE : View.GONE);

        int random = (new Random().nextInt(16));
        downCntText.setBackgroundResource(getResources().obtainTypedArray(R.array.down_count_bg).getResourceId(random, 0));

        if (cnt > 0) {
            downImage.post(() -> {
                int left = downCntText.getMeasuredWidth() / 3 * 2;
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) downCntText.getLayoutParams();
                params.setMargins(left, (int) Util.convertDpToPixel(3, MainActivity.this), 0, 0);
                downCntText.setLayoutParams(params);
            });
        }
    }

    private void setCheckCompleteDownloadItem() {
        if (fileCntText == null)
            return;

        int cnt = sp.getNotiCompleteDownloadCount();

        if (cnt >= 100)
            fileCntText.setText(":)");
        else
            fileCntText.setText(String.valueOf(cnt));
        fileCntText.setVisibility(cnt > 0 ? View.VISIBLE : View.GONE);

        int random = (new Random().nextInt(16));
        fileCntText.setBackgroundResource(getResources().obtainTypedArray(R.array.down_count_bg).getResourceId(random, 0));

        if (cnt > 0) {
            fileImage.post(() -> {
                int left = fileCntText.getMeasuredWidth() / 3 * 2;
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) fileCntText.getLayoutParams();
                params.setMargins(left, (int) Util.convertDpToPixel(3, MainActivity.this), 0, 0);
                fileCntText.setLayoutParams(params);
            });
        }
    }

    private void addDownloadList(boolean isBrowser, String site, String url, String filename, boolean isDirect) {
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(filename))
            return;

        if (url.matches("(?i).*youtube.*"))
            return;

//        api.sendTrendUrl(url, false, false); if (isBrowser && isDirect)
        DownItem item = new DownItem();
        item.setDownloadId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setDownloadUrl(url);
        item.setDownloadSite(site);
        item.setDownloadFileName(filename);
        item.setDownloadState(isDirect ? DownItem.DOWNLOAD_STATE_WAITING : DownItem.DOWNLOAD_STATE_NONE);
        item.setDownloadFileRemainSize(0);
        item.setDownloadFilePath(Common.getVideoDir(app.getApplicationContext()) + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION);
        item.setDownloadFileThumbnailPath(Common.getVideoDir(app.getApplicationContext()) + "." + FileUtil.getFileName(item.getDownloadFileName()) + FileUtil.TEMP_THUMBNAIL_EXTENSION);

        LogUtil.log("HDVD Download " + item.getDownloadUrl());
        if (!isBrowser) {
            try {
                URL fildUrl = new URL(url);
                URLConnection connection = fildUrl.openConnection();
                connection.connect();
                long fileLength = connection.getContentLength();
                item.setDownloadFileTotalSize(fileLength);
            } catch (Exception e) {
                e.printStackTrace();
            }

            db.insertOrUpdateDownloadItem(item);
            item = db.getDownloadItem(item.getDownloadId());
            downManager.addToRequestList(item.getDownloadId());
        } else {
            new AddDownloadItem().execute(item);
            if (!sp.isPremiumMode())
                checkShowPremiumDialog();
        }
    }

    private void checkShowPremiumDialog() {
        long time = 1000 * 60 * 60 * 24 * 5;
        int showCount = sp.getShowPremiumDialogCount();
        if (showCount < 2) {
            if (showCount == 1 && sp.getShowPremiumDialogTime() > 0 && System.currentTimeMillis() > sp.getShowPremiumDialogTime() + time) {
                if (DEBUG_MODE)
                    Util.showToast(this, "1 Download", true);
                sp.setPremiumDownloadCount(1);
                sp.setShowPremiumDialogTime(0);
                return;
            }

            int count = sp.getPremiumDownloadCount();
            if (count == 4) {
                sp.setPremiumDownloadCount(count + 1);
                if (fdf(PremiumDialog.TAG) == null) {
                    PremiumDialog dlg = PremiumDialog.newInstance(PremiumDialog.TAG, this);
                    dlg.setListener(new PremiumDialog.PremiumDialogListener() {
                        @Override
                        public void onPurchaseButtonClicked() {
                            onStartPremiumMode();
                        }

                        @Override
                        public void onNotNowButtonClicked() {

                        }
                    });
                    sdf(dlg);
                    sp.setShowPremiumDialogCount(showCount + 1);
                    sp.setShowPremiumDialogTime(System.currentTimeMillis());
                }
            } else if (count < 4) {
                sp.setPremiumDownloadCount(count + 1);
                if (DEBUG_MODE)
                    Util.showToast(this, String.format("%d Downloads", count + 1), true);
            }

        }
    }

    private boolean askForPhonePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.app_name) + " needs permission.\nGo to Settings > Permissions, then allow the following permissions and try again:");
                    dlg.setPositiveLabel("Settings");
                    dlg.setPositiveListener((dialog, tag) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + getPackageName()));
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    });
                    dlg.setNegativeLabel("cancel");
                    sdf(dlg);
                }

                return false;
            } else {
                ActivityCompat.requestPermissions(this, PHONE_PERMISSIONS, REQUEST_CODE_READ_PHONE_STATE);
                return false;
            }
        }

        return true;
    }

    private boolean askForPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                LogUtil.log("NEED PERMISSION CHECK");
//                Toast.makeText(this, "NEED PERMISSION CHECK", Toast.LENGTH_SHORT).show();
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.app_name) + " needs permission.\nGo to Settings > Permissions, then allow the following permissions and try again:");
                    dlg.setPositiveLabel("Settings");
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:" + getPackageName()));
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
                    dlg.setNegativeLabel("cancel");
                    sdf(dlg);
                }

                return false;
            } else {
                ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_CODE_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
//            case REQUEST_CODE_READ_EXTERNAL_STORAGE :
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;

            case REQUEST_CODE_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Util.showToast(this, "NEED PERMISSION CHECK", false);
                }
                return;

            case REQUEST_CODE_READ_PHONE_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                    app.setSimContry(manager.getSimCountryIso());
                } else {
                    Util.showToast(this, "NEED PERMISSION CHECK", false);
                }
                return;

            default:
                break;
        }
    }

    private void showToast(String msg) {
        toastText.setText(msg);
        toastLayout.expand(true);
        toastHandler.sendEmptyMessageDelayed(0, 1500);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        frontFragment = new FrontFragment();
        frontFragment.setListener(this);
        downloadFragment = new DownloadingFragment();
        downloadFragment.setListener(this);
        fileFragment = new DownloadCompleteFragment();
        menuFragment = new MenuFragment();
        menuFragment.setListener(this);

        adapter.addFrag(frontFragment, frontFragment.TAG);
        adapter.addFrag(downloadFragment, downloadFragment.TAG);
        adapter.addFrag(fileFragment, fileFragment.TAG);
        adapter.addFrag(menuFragment, menuFragment.TAG);

        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBillingInitialized() {
        // BillingProcessor 초기화, 구매 준비 완료
        if (DEBUG_MODE)
            Util.showToast(this, "Main onBillingInitialized", false);

        if (bp.isPurchased(Common.PREMIUM_DOWNLOADER_ID))
            return;

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    premiumProduct = bp.getPurchaseListingDetails(Common.PREMIUM_DOWNLOADER_ID);
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (premiumProduct != null && DEBUG_MODE) {
                    Util.showToast(MainActivity.this, premiumProduct.title + "\n\n" + premiumProduct.priceText, true);
                }
            }
        }.execute();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        // 구매 성공시 호출
        if (DEBUG_MODE)
            Util.showToast(this, "Main onProductPurchased", false);

        SkuDetails sku = bp.getPurchaseListingDetails(productId);
        if (DEBUG_MODE)
            Util.showToast(this, sku.title + "\n\n" + "Completed!!!!!", true);

        sp.setPremiumMode(true);
        Util.showToast(this, r.s(R.string.message_premium_complete), true);
        if (frontFragment != null)
            frontFragment.startPremiumMode();

        if (bannerAdLayout != null)
            bannerAdLayout.setVisibility(View.GONE);

        if (fileFragment != null)
            fileFragment.startPremiumMode();

        if (menuFragment != null)
            menuFragment.startPremiumMode();

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        // 구매시 오류 발생했을때 호출
        // Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        if (DEBUG_MODE)
            Util.showToast(this, "Main onBillingError", false);
        if (errorCode != Constants.BILLING_RESPONSE_RESULT_USER_CANCELED) {
            String errorMessage = "Billing response result user canceled (" + errorCode + ")";
            Util.showToast(this, errorMessage, false);
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {
        // Called when purchase history was restored and the list of all owned PRODUCT ID's was loaded from Google Play
        if (DEBUG_MODE)
            Util.showToast(this, "Main onPurchaseHistoryRestored", false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (DEBUG_MODE)
            Util.showToast(this, "Main onActivityResult", false);
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    class AddReadyDownloadItem extends AsyncTask<DownItem, Void, DownItem> {
        @Override
        protected DownItem doInBackground(DownItem... params) {
            DownItem item = params[0];

            String urlString = item.getDownloadUrl();

            if (!TextUtils.isEmpty(urlString)) {
                long totalSize = -1;
                HttpURLConnection conn = null;
                try {
                    URL url = new URL(urlString);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(1000);
                    conn.setReadTimeout(1000);
                    conn.setRequestMethod("HEAD");
                    String temp = conn.getHeaderField("Content-Length");
                    if (TextUtils.isEmpty(temp) || temp.equalsIgnoreCase("0")
                            || temp.equalsIgnoreCase("-1"))
                        totalSize = conn.getContentLength();
                    else
                        totalSize = Long.parseLong(temp);
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                } finally {
                    if (conn != null)
                        conn.disconnect();
                }
                item.setDownloadFileTotalSize(totalSize);
            }

            if (!item.getDownloadUrl().contains("fbcdn.net")) {
                Bitmap bitmap = null;
                MediaMetadataRetriever mediaMetadataRetriever = null;
                try {
                    mediaMetadataRetriever = new MediaMetadataRetriever();
                    if (Build.VERSION.SDK_INT >= 14)
                        mediaMetadataRetriever.setDataSource(urlString, new HashMap<String, String>());
                    else
                        mediaMetadataRetriever.setDataSource(urlString);
                    bitmap = mediaMetadataRetriever.getFrameAtTime(2, MediaMetadataRetriever.OPTION_CLOSEST);

                    String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    if (TextUtils.isEmpty(time))
                        return null;
//                    item.setDownloadFileTime(0);
                    else {
                        long timeInmillisec = Long.parseLong(time);
                        item.setDownloadFileTime(timeInmillisec);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    {
                        if (mediaMetadataRetriever != null)
                            mediaMetadataRetriever.release();
                    }
                }
                if (bitmap != null)
                    item.setDownloadThumbnail(ImageUtil.getImageBytes(bitmap));
            }
            return item;

        }

        @Override
        protected void onPostExecute(DownItem item) {
            if (item == null)
                return;

            for (int i = 0; i < readyDownList.size(); i++) {
                if (item.getDownloadId().equalsIgnoreCase(readyDownList.get(i).getDownloadId())) {
                    readyDownList.get(i).setDownloadThumbnail(item.getDownloadThumbnail());
                    readyDownList.get(i).setDownloadFileTime(item.getDownloadFileTime());
                    readyDownList.get(i).setDownloadFileTotalSize(item.getDownloadFileTotalSize());
                    readyDownAdapter.notifyItemChanged(i);
                    break;
                }
            }
        }
    }

    class AddDownloadItem extends AsyncTask<DownItem, Void, DownItem> {
        @Override
        protected DownItem doInBackground(DownItem... params) {
            DownItem item = params[0];

            LogUtil.log("HDVD Download 2 " + item.getDownloadUrl());
            String urlString = item.getDownloadUrl();

            if (!TextUtils.isEmpty(urlString)) {
                long totalSize = -1;
                HttpURLConnection conn = null;
                try {
                    URL url = new URL(urlString);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(1000);
                    conn.setReadTimeout(1000);
                    conn.setRequestMethod("HEAD");
                    totalSize = (long) conn.getContentLength();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null)
                        conn.disconnect();
                }
                item.setDownloadFileTotalSize(totalSize);
            }

            Bitmap bitmap = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(urlString, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(urlString);
                bitmap = mediaMetadataRetriever.getFrameAtTime();

                String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (TextUtils.isEmpty(time))
                    return null;
//                    item.setDownloadFileTime(0);
                else {
                    long timeInmillisec = Long.parseLong(time);
                    item.setDownloadFileTime(timeInmillisec);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }
            if (bitmap != null)
                item.setDownloadThumbnail(ImageUtil.getImageBytes(bitmap));
            else {
                if (item.getDownloadUrl().contains("fbcdn.net")) {
                    item = null;
                }
            }
            return item;

        }

        @Override
        protected void onPostExecute(DownItem item) {
            if (item == null)
                return;

            LogUtil.log("HDVD Download 3 " + item.getDownloadUrl());
            if (!sp.isUseNotification() && sp.getTimeFiltering() > 0) {
                long limit = 15 * sp.getTimeFiltering() * 1000;
                if (item.getDownloadFileTime() < limit)
                    return;
            }

            if (sp.isUseNotification() || item.getDownloadState() == DownItem.DOWNLOAD_STATE_WAITING) {
                item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
                db.insertOrUpdateDownloadItem(item);
                downManager.addToRequestList(item.getDownloadId());
            } else {
                db.insertOrUpdateDownloadItem(item);

            }

            setCheckReadyDownloadItem();
            if (sp.isJoinedTrends() && item.getDownloadFileTime() > 30 * 1000) {
                if (askForPhonePermission())
                    api.sendTrendUrl(item, false, false);
            }


//            item = db.getDownloadItem(item.getDownloadId());
//            downManager.addToRequestList(item.getDownloadId());
//            if (downloadFragment != null)
//                downloadFragment.updateDownloadProgress(item);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case FRAGMENT_HOME:
                    if (frontFragment == null) {
                        frontFragment = new FrontFragment();
                        frontFragment.setListener(MainActivity.this);
                    }
                    return frontFragment;

                case FRAGMENT_DOWNLOAD:
                    if (downloadFragment == null) {
                        downloadFragment = new DownloadingFragment();
//                        downloadFragment.setListener(MainActivity.this);
                    }
                    return downloadFragment;

                case FRAGMENT_FILE:
                    if (fileFragment == null) {
                        fileFragment = new DownloadCompleteFragment();
                    }

                    return fileFragment;

                case FRAGMENT_MENU:
                    if (menuFragment == null)
                        menuFragment = new MenuFragment();
                    return menuFragment;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    class BackPressCloseHandler {
        private long backKeyPressedTime = 0;
        private Activity activity;
        private Toast toast;

        public BackPressCloseHandler(Activity context) {
            this.activity = context;
        }

        public void onBackPressed() {
            if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                backKeyPressedTime = System.currentTimeMillis();
                showGuide();
                return;
            }

            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                activity.finish();
                toast.cancel();
            }
        }

        public void showGuide() {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.layout_toast,
                    (ViewGroup) activity.findViewById(R.id.toast_layout_root));

            TextView text = (TextView) layout.findViewById(R.id.text_message);
            text.setText(r.s(R.string.message_back_exit));

            toast = new Toast(activity.getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }
}
