package com.ne.hdv.common;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

public class PremiumDialog extends BaseDialogFragment {
    public static final String TAG = PremiumDialog.class.getSimpleName();

    Context context;

    TextView costText;
    Button nowButton, notNowButton;

    private PremiumDialogListener listener;

    public static PremiumDialog newInstance(String tag, Context context) {
        PremiumDialog d = new PremiumDialog();

        d.context = context;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_premium;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        costText = fv(R.id.text_cost);
        costText.setPaintFlags(costText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        nowButton = fv(R.id.button_now);
        nowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onPurchaseButtonClicked();
                dismiss();
            }
        });

        notNowButton = fv(R.id.button_not_now);
        notNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onNotNowButtonClicked();
                dismiss();
            }
        });
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }

    }

    public PremiumDialog setListener(PremiumDialogListener listener) {
        this.listener = listener;

        return this;
    }

    public static interface PremiumDialogListener {
        public void onPurchaseButtonClicked();

        public void onNotNowButtonClicked();
    }
}
