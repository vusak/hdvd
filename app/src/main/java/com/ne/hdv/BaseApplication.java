package com.ne.hdv;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import com.ne.hdv.common.Common;
import com.ne.hdv.common.DBController;
import com.ne.hdv.common.SPController;
import com.ne.hdv.common.Util;
import com.ne.hdv.download.DownManager;
import com.ne.hdv.download.JNetworkMonitor;

/**
 * Created by Cecilia on 2017-05-15.
 * Base Application
 */
public class BaseApplication extends Application {
    private static JNetworkMonitor networkMonitor;
    private SPController sp;
    private DBController db;
    private ResourceWrapper r;

    private DownManager downloadManager;

    private String version;
    private String os = "Android " + Build.VERSION.RELEASE;
    private String deviceName;
    private String locale;
    private String modelName = Build.MODEL;
    private String deviceIdentifier;
    private String contry;
    private String simContry;
    private TelephonyManager tManager;

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();

        sp = new SPController(getApplicationContext());
        db = new DBController(getApplicationContext());
        db.restateDownloadState();
        r = new ResourceWrapper(getResources());

        version = Util.getVersion(getApplicationContext());
        deviceName = Build.MODEL;
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0).getLanguage();
            contry = getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = getResources().getConfiguration().locale.getLanguage();
            contry = getResources().getConfiguration().locale.getCountry();
        }
        tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        simContry = tManager.getSimCountryIso();

        deviceIdentifier = Util.sha256(Util.getMACAddress());

        downloadManager = new DownManager(this);
        networkMonitor = new JNetworkMonitor(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkMonitor, filter);

        createNotificationChannel();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public SPController getSPController() {
        return this.sp;
    }

    public DBController getDBController() {
        return this.db;
    }

    public ResourceWrapper getResourceWrapper() {
        return r;
    }

    public DownManager getDownloadManager() {
        return this.downloadManager;
    }

    public JNetworkMonitor getNetworkMonitor() {
        return this.networkMonitor;
    }

    public String getOS() {
        return this.os;
    }

    public String getOSName() {
        return "Android";
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getVersion() {
        return this.version;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setConfiguration(String locale, String contry) {
        this.locale = locale;
        this.contry = contry;
    }

    public String getContry() {
        return this.contry;
    }

    public String getSimContry() {
        return this.simContry;
    }

    public void setSimContry(String val) {
        this.simContry = val;
    }

    public String getModelName() {
        return this.modelName;
    }

    public String getDeviceIdentifier() {
        return this.deviceIdentifier;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "HDVD Download State";
            String description = "You can see the progress of the download.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Common.NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static class ResourceWrapper {
        private Resources r;

        public ResourceWrapper(Resources r) {
            this.r = r;
        }

        public int px(int id) {
            return r.getDimensionPixelSize(id);
        }

        public String s(int id) {
            return r.getString(id);
        }

        public String[] sa(int id) {
            return r.getStringArray(id);
        }

        public int c(Context context, int id) {
            if (context == null)
                return 0;
            else
                return ContextCompat.getColor(context, id);
        }

        public Drawable d(Context context, int id) {
            if (context == null)
                return null;
            else
                return ContextCompat.getDrawable(context, id);
        }

        public boolean b(int id) {
            return r.getBoolean(id);
        }

        public int i(int id) {
            return r.getInteger(id);
        }

        public long l(int id) {
            return Long.parseLong(s(id));
        }
    }
}
