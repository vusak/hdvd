package com.ne.hdv.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.TrendItem;

import java.util.List;

public class TrendAdapter extends ReSelectableAdapter<TrendItem, TrendAdapter.ReTrendHolder> {

    private Context context;
    private int editIndex = -1;

    public TrendAdapter(Context context, int layoutId, List<TrendItem> items, TrendAdapterListener listener) {
        super(layoutId, items, context);

        this.context = context;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ReTrendHolder holder, int position) {
        TrendItem item = items.get(position);
        if (item == null)
            return;

//        holder.root.setBackgroundColor(ContextCompat.getColor(context, item.isTrendDownloaded() ? R.color.download_selected_bg : R.color.download_non_bg));
        holder.root.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background));
        holder.blur.setVisibility(item.getTrendConfirmedStatus() != TrendItem.TREND_STATE_CONFIRMED ? View.VISIBLE : View.GONE);
        holder.reportedLayout.setVisibility(item.isTrendReported() ? View.VISIBLE : View.GONE);
        if (item.getTrendThumbnail() != null) {
            holder.number.setVisibility(View.VISIBLE);
            holder.bigNumber.setVisibility(View.GONE);
            Bitmap bmp = ImageUtil.getImage(item.getTrendThumbnail(), (int) Util.convertDpToPixel(120, context), (int) Util.convertDpToPixel(68, context));
            if (item.getTrendConfirmedStatus() != TrendItem.TREND_STATE_CONFIRMED) {
                try {
                    Bitmap temp = Bitmap.createScaledBitmap(bmp, 10, 10, false);
                    Bitmap temp2 = Bitmap.createScaledBitmap(temp, bmp.getWidth(), bmp.getHeight(), false);
                    holder.thumbnail.setImageBitmap(temp2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else
                holder.thumbnail.setImageBitmap(bmp);
//            holder.thumbnail.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            holder.number.setVisibility(View.GONE);
            holder.bigNumber.setVisibility(View.VISIBLE);
            holder.thumbnail.setImageDrawable(null);
//            holder.thumbnail.setScaleType(ImageView.ScaleType.CENTER);
        }

        holder.number.setText(String.format("%02d", item.getTrendNumber()));
        holder.bigNumber.setText(String.format("%02d", item.getTrendNumber()));
        Drawable numberBG = ContextCompat.getDrawable(context, R.drawable.text_triangle_gray);
        if (item.getTrendNumber() == 1) {
            numberBG = ContextCompat.getDrawable(context, R.drawable.text_triangle_red);
            holder.thumbnail.setBackgroundColor(ContextCompat.getColor(context, R.color.number_red_bg));
        } else if (item.getTrendNumber() == 2) {
            numberBG = ContextCompat.getDrawable(context, R.drawable.text_triangle_yellow);
            holder.thumbnail.setBackgroundColor(ContextCompat.getColor(context, R.color.number_yellow_bg));
        } else if (item.getTrendNumber() == 3) {
            numberBG = ContextCompat.getDrawable(context, R.drawable.text_triangle_blue);
            holder.thumbnail.setBackgroundColor(ContextCompat.getColor(context, R.color.number_blue_bg));
        } else {
            holder.thumbnail.setBackgroundColor(ContextCompat.getColor(context, R.color.number_gray_bg));
        }
        holder.number.setBackground(numberBG);
        holder.count.setText("" + item.getTrendCount());
        if (!TextUtils.isEmpty(item.getTrendSite()))
            holder.url.setText(item.getTrendSite());
        else
            holder.url.setText(item.getTrendUrl());

        holder.title.setText(item.getTrendUrl().substring(item.getTrendUrl().lastIndexOf('/') + 1).split("\\?")[0].split("#")[0]);
        String sub = item.getTrendFIleTime() > 0 ? FileUtil.convertToStringTime(item.getTrendFIleTime()) + " / " : "";
        sub = sub + (item.getTrenFileSize() > 0 ? FileUtil.convertToStringRepresentation(item.getTrenFileSize()) : "");
        holder.size.setText(sub);

//		holder.downloadButton.setVisibility(item.isTrendDownloaded() ? View.GONE : View.VISIBLE);
        holder.downloadButton.setVisibility(View.GONE);
    }

    @Override
    public ReTrendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReTrendHolder holder = new ReTrendHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_trend);
        holder.thumbnail = holder.fv(R.id.item_iv);
        holder.number = holder.fv(R.id.text_down_number);
        holder.bigNumber = holder.fv(R.id.text_down_numer_big);
        holder.count = holder.fv(R.id.text_down_count);
        holder.title = holder.fv(R.id.item_text_title);
        holder.url = holder.fv(R.id.item_text_url);
        holder.size = holder.fv(R.id.item_text_sub);
        holder.downloadButton = holder.fv(R.id.item_btn);
        holder.line = holder.fv(R.id.line);
        holder.blur = holder.fv(R.id.view_blur);
        holder.reportedLayout = holder.fv(R.id.layout_reported);
        holder.undoButton = holder.fv(R.id.button_report_undo);
        holder.setOnUndoButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    ((TrendAdapterListener) listener).onReportUndoButtonClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    listener.OnItemClick(position, items.get(position));
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0)
                    listener.OnItemLongClick(position, items.get(position));
                return true;
            }
        });

        return holder;
    }

    public void updateItem(TrendItem item) {
        if (item == null || items == null)
            return;

        for (int i = 0; i < items.size(); i++) {
            if (item.getTrendUrl().equalsIgnoreCase(items.get(i).getTrendUrl())) {
                items.get(i).setTrendThumbnail(item.getTrendThumbnail());
                items.get(i).setTrendFileSize(item.getTrenFileSize());
                items.get(i).setTrendFileTime(item.getTrendFIleTime());
                notifyItemChanged(i);
                return;
            }
        }
    }

    public static interface TrendAdapterListener extends ReOnItemClickListener<TrendItem> {
        public void onReportUndoButtonClick(int position, TrendItem item);
    }

    public class ReTrendHolder extends ReAbstractViewHolder {
        LinearLayout root, reportedLayout;
        ImageView thumbnail;
        TextView number, count, title, url, size, bigNumber;
        View line, blur;
        ImageButton downloadButton, undoButton;
        private OnItemViewClickListener undoButtonClickListener;

        public ReTrendHolder(View itemView) {
            super(itemView);
        }

        public void setOnUndoButtonClickListener(OnItemViewClickListener listener) {
            undoButtonClickListener = listener;
            if (listener != null && undoButton != null)
                undoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        undoButtonClickListener.onItemViewClick(getPosition(), ReTrendHolder.this);
                    }
                });
        }
    }

}
