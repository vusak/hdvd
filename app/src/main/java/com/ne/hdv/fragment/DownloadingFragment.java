package com.ne.hdv.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.ne.hdv.R;
import com.ne.hdv.adapter.DownloadAdapter;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.EditDownloadDialog;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.DownItem;
import com.ne.hdv.download.Api;
import com.ne.hdv.download.DownManager;
import com.ne.hdv.download.JNetworkMonitor;

import java.io.File;
import java.util.ArrayList;

import static com.ne.hdv.common.Util.showToast;

public class DownloadingFragment extends BaseFragment implements JNetworkMonitor.OnChangeNetworkStatusListener, DownManager.DownListener {

    public static final String TAG = DownloadingFragment.class.getSimpleName();
    private static final int REQUEST_CODE_READ_PHONE_STATE = 10003;
    private static String[] PHONE_PERMISSIONS = {
            Manifest.permission.READ_PHONE_STATE
    };
    FloatingActionButton allDownButton, allCancelButton, allPauseButton;
    FloatingActionMenu floatingActionMenu;

    RecyclerView lv;
    LinearLayoutManager manager;
    DownloadAdapter adapter;
    ArrayList<DownItem> list;
    FrameLayout totalLayout;
    TextView totalText;
    ProgressBar totalProgress;
    Api api;

    private DownloadListener listener;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download_status;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        floatingActionMenu = fv(R.id.button_floating_menu);

        allDownButton = fv(R.id.sub_floating_menu_download_all);
        allDownButton.setOnClickListener(v -> {
            visibleMenu(false);

            ArrayList<DownItem> items = getDownloadingItems();
            if (items != null && items.size() > 0) {
                for (DownItem item : items) {
                    item.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
                    db.updateDownloadState(item);
                    downloadManager.addToRequestList(item.getDownloadId());
                }
                refreshList();
                if (listener != null)
                    listener.onRefreshDownloadCount();
            }
        });

        allPauseButton = fv(R.id.sub_floating_menu_pause_all);
        allPauseButton.setOnClickListener(v -> {
            visibleMenu(false);

            downloadManager.cancelAll(null);
            db.restateDownloadState();
            refreshList();

            if (listener != null)
                listener.onRefreshDownloadCount();
        });

        allCancelButton = fv(R.id.sub_floating_menu_cancel_all);
        allCancelButton.setOnClickListener(v -> {
            visibleMenu(false);

            downloadManager.cancelAll(null);
            ArrayList<DownItem> list = getDownloadingItems();
            if (list != null && list.size() > 0)
                db.deleteDownloadItems(list);
            refreshList();

            if (listener != null)
                listener.onRefreshDownloadCount();
        });

        totalLayout = fv(R.id.layout_down_total);
        totalText = fv(R.id.text_down_total);
        totalProgress = fv(R.id.progress_down_total);

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        lv.getItemAnimator().setRemoveDuration(500);
        lv.getItemAnimator().setAddDuration(300);
        lv.getItemAnimator().setChangeDuration(0);
        adapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownItem item) {
                if (!Util.isNetworkAbailable(getActivity())) {
                    showAlertMessageDialog(r.s(R.string.dlg_msg_check_network));
                    return;
                } else if (!sp.isUseCellularData()) {
                    if (!Util.getNetworkConnectionType(getActivity()).equalsIgnoreCase("WIFI")) {
                        showAlertMessageDialog(r.s(R.string.dlg_msg_check_wifi));
                        return;
                    }
                }

                DownItem sendItem = item;

                if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DOWNLOADING) {
                    item.setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
                } else {
                    item.setDownloadStartAt(DownItem.DOWNLOAD_STATE_WAITING);
                }
                adapter.notifyItemChanged(position);

                if (sendItem.getDownloadState() == DownItem.DOWNLOAD_STATE_DOWNLOADING || sendItem.getDownloadState() == DownItem.DOWNLOAD_STATE_WAITING) {
                    downloadManager.cancelDownload(item.getDownloadId(), sendItem);
                    sendItem.setDownloadState(DownItem.DOWNLOAD_STATE_PAUSE);
                } else {
                    downloadManager.addToRequestList(sendItem.getDownloadId());
                    sendItem.setDownloadState(DownItem.DOWNLOAD_STATE_WAITING);
                }
                db.updateDownloadState(sendItem);
                if (listener != null)
                    listener.onRefreshDownloadCount();

            }

            @Override
            public void onDeleteButtonClick(int position, DownItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownItem item) {

            }

            @Override
            public void OnItemClick(int position, DownItem item) {

            }

            @Override
            public void OnItemLongClick(int position, DownItem item) {

            }
        });
        lv.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        downloadManager.addListener(this);
        networkMonitor.setListener(this);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {


    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_SEND_TREND) {

        }
        super.handleApiMessage(m);
    }

    @Override
    public void onDownloadError(DownItem item, int errorCode) {
        updateProgress(item);

        if (listener != null)
            listener.onRefreshDownloadCount();
    }

    @Override
    public void onDownloadProgress(DownItem item, long progress, long total) {
        updateProgress(item);
        updateDownloadProgressUI();
    }

    @Override
    public void onDownloadStart(DownItem item) {
        insertItem(item);
        updateDownloadProgressUI();
    }

    @Override
    public void onDownloadSuccess(DownItem item) {
        if (item == null)
            return;

        removeItem(item.getDownloadId());
        updateDownloadProgressUI();

        if (listener != null)
            listener.onRefreshDownloadCount();
    }

    @Override
    public void onNetworkChanged(int status) {
        if (downloadManager == null || downloadManager.isEmpty())
            return;

        if (status == JNetworkMonitor.NETWORK_TYPE_NOT_CONNECTED) {
            showAlertMessageDialog(r.s(R.string.dlg_msg_check_network));
            if (downloadManager != null)
                downloadManager.cancelAll(null);
            db.restateDownloadState();

            refreshList();
        } else if (status == JNetworkMonitor.NETWORK_TYPE_MOBILE && !sp.isUseCellularData()) {
            showAlertMessageDialog(r.s(R.string.dlg_msg_check_wifi));
            if (downloadManager != null)
                downloadManager.cancelAll(null);
            db.restateDownloadState();

            refreshList();
        }
    }

    public boolean setBackPress() {
        if (floatingActionMenu != null && floatingActionMenu.isOpened()) {
            visibleMenu(false);
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        refreshList();
        super.onResume();
    }

    public ArrayList<DownItem> getDownloadingItems() {
        return list;
    }


    public void deleteItems(boolean isAll) {
//        if (isAll)
//            db.deleteAllBookmarkItems();
//        else {
//            for (BookmarkItem item : adapter.getSelectedList()) {
//                db.deleteBookmarkItem(item);
//            }
//            adapter.clearSelection();
//            listener.onCheckChanged(0);
//        }

        refreshList();
    }

    public void refreshList() {
        if (list == null || adapter == null || lv == null)
            return;

        list.clear();

        ArrayList<DownItem> temp = new ArrayList<>();
        temp.addAll(db.getAllRDownloadItems());

        for (DownItem item : temp) {
            if (item.getDownloadState() != DownItem.DOWNLOAD_STATE_DONE)
                list.add(item);
        }
        adapter.setTrend(sp.isJoinedTrends());
        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        updateDownloadProgressUI();
        floatingActionMenu.setVisibility(empty ? View.GONE : View.VISIBLE);
    }

    public void insertItem(DownItem item) {
        if (adapter != null)
            adapter.insertDownloadItem(item);

        if (lv != null)
            lv.setVisibility(View.VISIBLE);
    }

    public void updateProgress(DownItem item) {
        if (adapter != null) {
            if (adapter.isContains(item))
                adapter.updateProgress(item);
            else
                adapter.insertDownloadItem(item);
        }

        if (lv != null)
            lv.setVisibility(View.VISIBLE);
    }

    public void removeItem(String id) {
        if (adapter != null)
            adapter.removeDownloadItem(id);

        if (lv != null && adapter.getItems().size() <= 0)
            lv.setVisibility(View.GONE);
    }

    public void updateDownloadProgressUI() {
        if (totalLayout == null)
            return;

        if (adapter == null || adapter.getItems() == null || adapter.getItems().size() <= 0) {
            totalLayout.setVisibility(View.GONE);
            floatingActionMenu.setVisibility(View.GONE);
            return;
        }

        long total = adapter.getTotalFileSize();
        long remain = adapter.getRemainFileSize();

        if (remain <= 0)
            totalText.setText("Ready");
        else
            totalText.setText(FileUtil.convertToStringRepresentation(remain) + " / " + FileUtil.convertToStringRepresentation(total));
        totalProgress.setMax((int) total);
        totalProgress.setProgress((int) remain);
        totalLayout.setVisibility(View.VISIBLE);
    }

    public void setListener(DownloadListener listener) {
        this.listener = listener;
    }

    private void deleteItem(final int position) {
        final DownItem item = list.get(position);
        if (item != null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.dlg_delete_title))
                        .setMessage(r.s(R.string.dlg_msg_delete_item))
                        .setNegativeLabel(r.s(R.string.str_cancel))
                        .setPositiveLabel(r.s(R.string.str_delete));
                dlg.setPositiveListener((dialog, tag) -> {
                    adapter.removeDownloadItem(item.getDownloadId());
                    db.deleteDownloadItem(item);
                });
                dlg.setNegativeListener((dialog, tag) -> {
                    if (adapter != null)
                        adapter.notifyItemChanged(position);
                });
                sdf(dlg);
            }
        }
    }

    private void editItem(final int position) {
        DownItem item = list.get(position);
        if (item != null) {
            if (fdf(EditDownloadDialog.TAG) == null) {
                EditDownloadDialog dlg = EditDownloadDialog.newInstance(EditDownloadDialog.TAG, getActivity(), item);
                dlg.setListener(new EditDownloadDialog.EditDownloadDialogListener() {
                    @Override
                    public void onSaveButtonClick(DownItem item) {
                        String newPath = new File(item.getDownloadFilePath()).getParent() + "/" + item.getDownloadFileName() + FileUtil.TEMP_FILE_EXTENSION;
                        item.setDownloadFilePath(newPath);
                        db.insertOrUpdateDownloadItem(item);
                        if (adapter != null) {
                            adapter.updateProgress(item);
                        }
                    }

                    @Override
                    public void onCancelButtonClick() {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                    }
                });

                sdf(dlg);
            }
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    deleteItem(viewHolder.getAdapterPosition());
                } else {
                    editItem(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.download_edit_bg));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mode_edit_white_24);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.str_edit), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.download_delete_bg));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white_24);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.str_delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

    private void visibleMenu(boolean show) {
        if (show) ;
        else
            floatingActionMenu.close(true);
    }

    private void showAlertMessageDialog(String msg) {
        if (fdf(MessageDialog.TAG) == null) {
            MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
            dlg.setTitle(r.s(R.string.download));
            dlg.setPositiveLabel(r.s(R.string.ok));
            dlg.setMessage(msg);

            sdf(dlg);
        }
    }

    private boolean askForPhonePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_PHONE_STATE)) {
//                LogUtil.log("NEED PERMISSION CHECK");
//                Toast.makeText(this, "NEED PERMISSION CHECK", Toast.LENGTH_SHORT).show();
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.app_name) + " needs permission.\nGo to Settings > Permissions, then allow the following permissions and try again:");
                    dlg.setPositiveLabel("Settings");
                    dlg.setPositiveListener((dialog, tag) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + getActivity().getPackageName()));
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    });
                    dlg.setNegativeLabel("cancel");
                    sdf(dlg);
                }

                return false;
            } else {
                ActivityCompat.requestPermissions(getActivity(), PHONE_PERMISSIONS, REQUEST_CODE_READ_PHONE_STATE);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_READ_PHONE_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
                    app.setSimContry(manager.getSimCountryIso());
                } else {
                    showToast(getActivity(), "NEED PERMISSION CHECK", false);
                }
                return;

            default:
                break;
        }
    }

    public static interface DownloadListener {
        public void onRefreshDownloadCount();
    }
}
