package com.ne.hdv.download;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ne.hdv.BaseApplication;
import com.ne.hdv.base.BaseActivity;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.DBController;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.SPController;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.DownItem;
import com.ne.hdv.data.ReportItem;
import com.ne.hdv.data.TrendItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Api extends Handler {

    private static RequestQueue requestQueue;
    private static Queue<Message> messageQueue = new LinkedList<Message>();

    private WeakReference<ApiListener> ref;
    private BaseApplication app;
    private SPController sp;
    private DBController db;
    private BaseApplication.ResourceWrapper r;

    public Api(BaseApplication application, ApiListener target) {
        app = application;
        ref = new WeakReference<ApiListener>(target);
        sp = application.getSPController();
        db = application.getDBController();
        r = application.getResourceWrapper();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(app.getApplicationContext());
    }

    public static void cancelApprequest() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void setTarget(ApiListener target) {
        if (ref != null)
            ref.clear();

        ref = new WeakReference<ApiListener>(target);
    }

    public void sendReportUrl(final ReportItem item, boolean showProgress, boolean showErrorMessage) {
        Message m = newMessage(Common.API_CODE_SEND_REPORT, null, showErrorMessage);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_SERVICE_NAME, "HDVD");
            params.put(Common.PARAM_URL, URLEncoder.encode(item.getReportSite(), "utf-8"));
            params.put(Common.PARAM_REGION, item.getReportRegion());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_REPORT, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                m.getData().putBoolean("reported", res);
                if (!res) {
                    if (!TextUtils.isEmpty(sp.getApiErrorLog()))
                        sp.setApiErrorLog(sp.getApiErrorLog() + ",");
                    sp.setApiErrorLog(sp.getApiErrorLog() + Common.RESULT_REPORT_SET_ERR_LOG);
                }
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void sendTrendUrl(final DownItem item, boolean showProgress, boolean showErrorMessage) {
        Message m = newMessage(Common.API_CODE_SEND_TREND, null, showErrorMessage);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_SERVICE_NAME, "HDVD");
            params.put(Common.PARAM_URL, URLEncoder.encode(item.getDownloadUrl(), "utf-8"));
            String region = app.getSimContry();
            if (TextUtils.isEmpty(region))
                region = "WW";
            params.put(Common.PARAM_REGION, region);
            params.put(Common.PARAM_LANGUAGE, app.getLocale());
            params.put(Common.PARAM_TIME, "0" + item.getDownloadFileTime());
            params.put(Common.PARAM_SIZE, "0" + item.getDownloadFileTotalSize());
            params.put(Common.PARAM_PAGE, URLEncoder.encode(item.getDownloadSite(), "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_TREND, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);

                if (res) {
                    LogUtil.log("onSuccess" + j_result.toString());
                    int count = Integer.valueOf(Util.optString(j_result, Common.TAG_COUNT));
                    item.setDownloadCount(count + 1);
                    db.insertOrUpdateDownloadItem(item);

                    m.getData().putBoolean("update", true);
                    m.getData().putParcelable("download_item", item);
                } else {
                    m.getData().putBoolean("update", false);
                    if (!TextUtils.isEmpty(sp.getApiErrorLog()))
                        sp.setApiErrorLog(sp.getApiErrorLog() + ",");
                    sp.setApiErrorLog(sp.getApiErrorLog() + Common.RESULT_TREND_SET_ERR_LOG);
                }
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void sendTrendStatus(final TrendItem item) {
        Message m = newMessage(Common.API_CODE_SEND_TREND_STATUS, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_DATE_TYPE, "w");
            params.put(Common.PARAM_SEARCH_URL, URLEncoder.encode(item.getTrendUrl(), "utf-8"));
            params.put(Common.PARAM_STATUS, "" + item.getTrendConfirmedStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_TREND_STATUS, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);

                if (res) {
                    LogUtil.log("onSuccess" + j_result.toString());

                    m.getData().putBoolean("update", true);
                } else {
                    m.getData().putBoolean("update", false);
                    if (!TextUtils.isEmpty(sp.getApiErrorLog()))
                        sp.setApiErrorLog(sp.getApiErrorLog() + ",");
                    sp.setApiErrorLog(sp.getApiErrorLog() + Common.RESULT_TREND_SET_ERR_LOG);
                }
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void sendTrendReport(final TrendItem item, String type) {
        Message m = newMessage(Common.API_CODE_SEND_TREND_REPORT, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_DATE_TYPE, "w");
            params.put(Common.PARAM_SEARCH_URL, URLEncoder.encode(item.getTrendUrl(), "utf-8"));
            params.put(Common.PARAM_CONFIRM_TYPE, "" + type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_TREND_REPORT, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);

                if (res) {
                    LogUtil.log("onSuccess" + j_result.toString());

                    m.getData().putBoolean("update", true);
                } else {
                    m.getData().putBoolean("update", false);
                    if (!TextUtils.isEmpty(sp.getApiErrorLog()))
                        sp.setApiErrorLog(sp.getApiErrorLog() + ",");
                    sp.setApiErrorLog(sp.getApiErrorLog() + Common.RESULT_TREND_SET_ERR_LOG);
                }
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    @SuppressLint("NewAPi")
    public void getTrendWorldWide(String type, boolean showProgress, boolean showErrorMessage) {
        Message m = newMessage(Common.API_CODE_GET_TREND_LIST_WORLDWIDE, null, showErrorMessage);

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + type, null, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
            }

            @Override
            public void onSuccess(String result) {
                LogUtil.log("Api getTrendWorldWide onSuccess" + result);
                ArrayList<TrendItem> dbItems = new ArrayList<TrendItem>();
                dbItems.addAll(db.getTrendItems());
                db.deleteAllTrendItems();
//                ArrayList<TrendItem> items = new ArrayList<TrendItem>();
                try {
                    JSONArray arr = new JSONArray(result);
                    if (arr.length() <= 0) {
                        if (!TextUtils.isEmpty(sp.getApiErrorLog()))
                            sp.setApiErrorLog(sp.getApiErrorLog() + ",");
                        sp.setApiErrorLog(sp.getApiErrorLog() + Common.RESULT_TREND_JSON_ERR_LOG);
                        m.getData().putBoolean("update", false);
                        return;
                    }

//                    JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        TrendItem item = new TrendItem(json_c);

                        for (TrendItem temp : dbItems) {
                            if (temp.getTrendUrl().equalsIgnoreCase(item.getTrendUrl())) {
                                item.setTrendReported(temp.isTrendReported());
                                break;
                            }
                        }
                        db.insertOrUpdateTrendItem(item);
                    }

                    m.getData().putBoolean("update", true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getTrendList(String type, int count, boolean showProgress, boolean showErrorMessage) {
        Message m = newMessage(Common.API_CODE_GET_TREND_LIST, null, showErrorMessage);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_DATE_TYPE, type);
            params.put(Common.PARAM_LIST_COUNT, String.valueOf(count));
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_GET_TREND_LIST, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<TrendItem> dbItems = new ArrayList<TrendItem>();
                dbItems.addAll(db.getTrendItems());

                db.deleteAllTrendItems();
                ArrayList<TrendItem> items = new ArrayList<TrendItem>();
                try {
                    JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        TrendItem item = new TrendItem(json_c);

                        if (dbItems.size() > 0) {
                            for (TrendItem temp : dbItems) {
                                if (temp.getTrendUrl().equalsIgnoreCase(item.getTrendUrl())) {
                                    item.setTrendFileSize(temp.getTrenFileSize());
                                    item.setTrendFileTime(temp.getTrendFIleTime());
                                    item.setTrendTitle(temp.getTrendTitle());
                                    item.setTrendThumbnail(temp.getTrendThumbnail());

                                    dbItems.remove(temp);
                                    break;
                                }
                            }
                        }

                        if (item != null)
                            db.insertOrUpdateTrendItem(item);
                    }

                    m.getData().putBoolean("update", true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String result) {
            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getTrendLearnMoreData() {
        Message m = newMessage(Common.API_CODE_GET_TREND_LEARNMORE, null, false);

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_GET_TREND_LEARNMORE, null, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
            }

            @Override
            public void onSuccess(String result) {
                LogUtil.log("Api getTrendLearnMoreData onSuccess" + result);
                String terms = null;
                try {
                    JSONArray arr = new JSONArray(result);

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        terms = Util.optString(json_c, Common.TAG_TEXT);
                        String region = Util.optString(json_c, Common.TAG_REGION);
                        if (!TextUtils.isEmpty(region) && region.equalsIgnoreCase(app.getContry().toLowerCase()))
                            break;
                    }

                    m.getData().putString("terms", terms);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getReportLearnMoreData() {
        Message m = newMessage(Common.API_CODE_GET_REPORT_LEARNMORE, null, false);

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_GET_REPORT_LEARNMORE, null, false) {

            @Override
            public void onSuccess(JSONObject j_result) {
            }

            @Override
            public void onSuccess(String result) {
                LogUtil.log("Api getReportLearnMoreData onSuccess" + result);
                String terms = null;
                try {
                    JSONArray arr = new JSONArray(result);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        terms = Util.optString(json_c, Common.TAG_TEXT);
                        String region = Util.optString(json_c, Common.TAG_REGION);
                        if (!TextUtils.isEmpty(region) && region.equalsIgnoreCase(app.getContry().toLowerCase()))
                            break;
                    }

                    m.getData().putString("terms", terms);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void sendErrorLog() {
        Message m = newMessage(Common.API_CODE_SEND_ERROR_LOG, null, false);

        if (TextUtils.isEmpty(sp.getApiErrorLog()))
            return;

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            String region = app.getSimContry();
            if (TextUtils.isEmpty(region))
                region = "WW";
            params.put(Common.PARAM_REGION, region);
            params.put(Common.PARAM_ERROR_CODE, sp.getApiErrorLog());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_ERROR, params, false) {

            @Override
            public void onSuccess(JSONObject j_result) {

                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                m.getData().putBoolean("send_error_log", res);
                if (res) {
                    sp.setApiErrorLog("");
                }
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    private Message newMessage(int apiCode, String progressMessage, boolean showErrorMessage) {
        Message m = Message.obtain(this, apiCode);
        Bundle b = new Bundle();
        b.putString(Common.PARAM_PROGRESS_MESSAGE, progressMessage);
        b.putBoolean(Common.PARAM_SHOW_ERROR_MESSAGE, showErrorMessage);
        m.setData(b);

        return m;
    }

    @Override
    public void handleMessage(Message msg) {
        if (ref == null || ref.get() == null)
            return;

        ref.get().handleApiMessage(msg);
    }

    public static interface ApiListener {
        public void handleApiMessage(Message m);

        public BaseActivity getApiActivity();

        public FragmentManager getApiFragmentManager();
    }

    private abstract static class ApiRequest implements Response.Listener<String>, Response.ErrorListener {
        protected Api api;
        protected Message m;
        protected boolean addAuthorization;
        protected StringRequest request;
        protected String paramString;

        public ApiRequest(Api _api, Message _m, int method, String _url, Map<String, Object> params, final boolean addAuthorizationToken) {
            this.api = _api;
            this.m = _m;
            this.addAuthorization = addAuthorizationToken;

            JSONObject j_params = null;
            if (method == Request.Method.GET) {
                if (params != null && params.size() > 0) {
                    Object[] temp = new Object[params.size()];
                    Set<String> keySet = params.keySet();
                    int i = 0;
                    for (String key : keySet) {
                        try {
                            temp[i] = key + "=" + URLEncoder.encode(String.valueOf(params.get(key)), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            temp[i] = key + "=" + String.valueOf(params.get(key));
                        }
                        i++;
                    }

                    _url += "?" + TextUtils.join("&", temp);
                }
            } else if (params != null) {
                j_params = new JSONObject(params);
                paramString = j_params.toString();
            }

            request = new StringRequest(method, _url, this, this) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();
                    if (headers == null || headers.equals(Collections.emptyMap()))
                        headers = new Hashtable<String, String>();

                    headers.put(Common.HEADER_CACHE_CONTROL, Common.HEADER_NO_CACHE);
                    headers.put(Common.HEADER_APPLICATION_VERSION, api.app.getVersion());
                    headers.put(Common.HEADER_OS, api.app.getOS());
//                    headers.put(Common.HEADER_DEVICE_IDENTIFIER, api.app.getDeviceIdentifier());
//                    if (addAuthorizationToken)
//                        headers.put(Common.HEADER_ACCESS_TOKEN, api.sp.getAccessToken());

                    if (LogUtil.DEBUG_MODE) {
                        StringBuilder sb = new StringBuilder();
                        for (String key : headers.keySet()) {
                            sb.append(key)
                                    .append(": ")
                                    .append(headers.get(key))
                                    .append("\n");
                        }
                        LogUtil.log("API_REQUEST", "HEADER===\n" + sb.toString());
                    }
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (TextUtils.isEmpty(paramString))
                        return super.getParams();
                    else {
                        Map<String, String> params = new HashMap<>();
                        params.put("param", paramString);
                        return params;
                    }
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(Common.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (LogUtil.DEBUG_MODE) {
                LogUtil.log("API_REQUEST", "URL : " + request.getUrl());
                try {
                    if (request.getBody() != null)
                        LogUtil.log("API_REQUEST", "BODY : " + new String(request.getBody()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        abstract public void onSuccess(String result);

        abstract public void onSuccess(JSONObject j_result);

        abstract public void onError();

        @Override
        public void onResponse(String response) {
            if (response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }

            try {
                response = new String(response.getBytes("8859_1"), "utf-8");
                LogUtil.log("UTF-8 " + response);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (LogUtil.DEBUG_MODE && response != null)
                LogUtil.log("API_REQUEST", "RESPONSE : " + response.toString());

            if (m.what == Common.API_CODE_GET_TREND_LIST_WORLDWIDE
                    || m.what == Common.API_CODE_GET_TREND_LEARNMORE
                    || m.what == Common.API_CODE_GET_REPORT_LEARNMORE) {
                onReceiveResponse(null, response, null);
            } else {
                JSONObject j_response = null;
                try {
                    j_response = new JSONObject(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (j_response == null) {
                    m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                    onReceiveResponse(null, null, "NO_RESPONSE");
                    return;
                }
                onReceiveResponse(j_response, null, null);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;

            if (error.networkResponse == null)
                m.arg2 = Common.RESULT_CODE_ERROR_UNKNOWN;
            else
                m.arg2 = error.networkResponse.statusCode;

            onReceiveResponse(null, null, String.valueOf(m.arg2));
        }

        public void ajax() {
            Api.requestQueue.add(request);
            synchronized (messageQueue) {
                messageQueue.add(m);
            }
        }

        private void onReceiveResponse(JSONObject j_result, String result, String errorCode) {
            synchronized (messageQueue) {
                messageQueue.remove(m);
            }

            new ResponseTask(this, j_result, result, errorCode).run();
        }
    }

    private static class ResponseTask extends AsyncTask<Void, Void, Void> {
        protected ApiRequest request;
        protected JSONObject j_result;
        protected String result;
        protected String errorCode;

        ResponseTask(ApiRequest request, JSONObject j_result, String result, String errorCode) {
            this.request = request;
            this.j_result = j_result;
            this.result = result;
            this.errorCode = errorCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (j_result != null)
                request.onSuccess(j_result);
            else if (result != null)
                request.onSuccess(result);
            else
                request.onError();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            request.api.sendMessage(request.m);
        }

        public void run() {
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        }
    }
}
