package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;


public class BookmarkItem extends TableObject {
    public static final String TABLE_NAME = "bookmarks";

    public static final String ID_HOW_TO_USE = "HowToUse";
    public static final String ID_DO_IT_YOURSELF = "DoItYourself";
    public static final String ID_AD_INSTANT = "AdInstant";
    public static final String ID_AD_FILEMANAGMER = "AdFilemanager";
    public static final String ID_AD_JM_BROWSER = "AdJMBrowser";
    public static final String ID_AD_EGG_STORY = "AdEggStory";
    public static final String ID_AD_QR_SCANNER = "QRScanner";

    public static final String ROW_ID = "_id";
    public static final String BOOKMARK_ID = "bookmark_id";
    public static final String BOOKMARK_URL = "bookmark_url";
    public static final String BOOKMARK_NAME = "bookmark_name";
    public static final String BOOKMARK_ICON = "bookmark_icon";
    public static final String BOOKMARK_AT = "bookmark_at";
    public static final String BOOKMARK_SHOW_MAIN = "bookmark_show_main";
    public static final String BOOKMARK_ORDER = "bookmark_order";
    public static final Creator<BookmarkItem> CREATOR = new Creator<BookmarkItem>() {
        @Override
        public BookmarkItem createFromParcel(Parcel source) {
            return new BookmarkItem(source);
        }

        @Override
        public BookmarkItem[] newArray(int size) {
            return new BookmarkItem[size];
        }
    };
    private long id;
    private String bookmarkId;
    private String url;
    private String name;
    private byte[] icon;
    private long bookmarkAt;
    private long bookmarkOrder;
    private boolean show = false;

    public BookmarkItem() {
        super();
    }

    public BookmarkItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.bookmarkId = in.readString();
        this.url = in.readString();
        this.name = in.readString();
        in.readByteArray(this.icon);
        this.show = in.readInt() == 1;
        this.bookmarkAt = in.readLong();
        this.bookmarkOrder = in.readLong();
    }

    public static void createTableBookmarks(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(BOOKMARK_ID + " text not null unique, ")
                .append(BOOKMARK_URL + " text not null, ")
                .append(BOOKMARK_NAME + " text, ")
                .append(BOOKMARK_ICON + " BLOB, ")
                .append(BOOKMARK_SHOW_MAIN + " integer not null, ")
                .append(BOOKMARK_AT + " integer not null, ")
                .append(BOOKMARK_ORDER + " integer not null) ")
                .toString());
    }

    public static BookmarkItem getBookmarkItem(SQLiteDatabase db, String url) {
        List<BookmarkItem> list = getBookmarkItems(db, null, BOOKMARK_URL + " = ?", new String[]{url}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static BookmarkItem getBookmarkItem(SQLiteDatabase db, BookmarkItem item) {
        List<BookmarkItem> list = getBookmarkItems(db, null, BOOKMARK_ID + " = ?", new String[]{item.getBookmarkId()}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<BookmarkItem> getBookmarkItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<BookmarkItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                BookmarkItem item = new BookmarkItem()
                        .setRowId(c)
                        .setBookmarkId(c)
                        .setBookmarkUrl(c)
                        .setBookmarkName(c)
                        .setBookmarkIcon(c)
                        .setBookmarkShowing(c)
                        .setBookmarkAt(c)
                        .setBookmarkOrder(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateBookmarkItem(SQLiteDatabase db, BookmarkItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookmarkId(v)
                    .putBookmarkUrl(v)
                    .putBookmarkName(v)
                    .putBookmarkIcon(v)
                    .putBookmarkShowing(v)
                    .putBookmarkAt(v)
                    .putBookmarkOrder(v);

            int rowAffected = db.update(TABLE_NAME, v, BOOKMARK_ID + " =? ", new String[]{item.getBookmarkId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBookmarkOrder(SQLiteDatabase db, BookmarkItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookmarkOrder(v);

            db.update(TABLE_NAME, v, BOOKMARK_ID + " = ?", new String[]{item.getBookmarkId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBookmarkShowing(SQLiteDatabase db, BookmarkItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookmarkShowing(v);

            db.update(TABLE_NAME, v, BOOKMARK_ID + " = ?", new String[]{item.getBookmarkId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateBookmark(SQLiteDatabase db, BookmarkItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putBookmarkName(v);
            item.putBookmarkUrl(v);
            item.putBookmarkOrder(v);

            db.update(TABLE_NAME, v, BOOKMARK_ID + " = ?", new String[]{item.getBookmarkId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteBookmarkItem(SQLiteDatabase db, BookmarkItem item) {
        try {
            db.delete(TABLE_NAME, BOOKMARK_ID + " = ? ", new String[]{item.getBookmarkId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteBookmarkItem(SQLiteDatabase db, String url) {
        try {
            db.delete(TABLE_NAME, BOOKMARK_URL + " = ? ", new String[]{url});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllBookmarkItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(bookmarkId);
        out.writeString(url);
        out.writeString(name);
        out.writeByteArray(icon);
        out.writeInt(show ? 1 : 0);
        out.writeLong(bookmarkAt);
        out.writeLong(bookmarkOrder);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BookmarkItem && bookmarkId == ((BookmarkItem) obj).bookmarkId;
    }

    public long getRowId() {
        return this.id;
    }

    public BookmarkItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public BookmarkItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public BookmarkItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getBookmarkId() {
        return this.bookmarkId;
    }

    public BookmarkItem setBookmarkId(String id) {
        this.bookmarkId = id;

        return this;
    }

    public BookmarkItem setBookmarkId(Cursor c) {
        this.bookmarkId = s(c, BOOKMARK_ID);

        return this;
    }

    public BookmarkItem putBookmarkId(ContentValues v) {
        v.put(BOOKMARK_ID, bookmarkId);

        return this;
    }

    public String getBookmarkUrl() {
        return this.url;
    }

    public BookmarkItem setBookmarkUrl(String url) {
        this.url = url;

        return this;
    }

    public BookmarkItem setBookmarkUrl(Cursor c) {
        this.url = s(c, BOOKMARK_URL);

        return this;
    }

    public BookmarkItem putBookmarkUrl(ContentValues v) {
        v.put(BOOKMARK_URL, url);

        return this;
    }

    public String getBookmarkName() {
        return this.name;
    }

    public BookmarkItem setBookmarkName(String name) {
        this.name = name;

        return this;
    }

    public BookmarkItem setBookmarkName(Cursor c) {
        this.name = s(c, BOOKMARK_NAME);

        return this;
    }

    public BookmarkItem putBookmarkName(ContentValues v) {
        v.put(BOOKMARK_NAME, name);

        return this;
    }

    public boolean isBookmarkShowing() {
        return this.show;
    }

    public BookmarkItem setBookmarkShowing(boolean show) {
        this.show = show;

        return this;
    }

    public BookmarkItem setBookmarkShowing(Cursor c) {
        this.show = i(c, BOOKMARK_SHOW_MAIN) == 1;

        return this;
    }

    public BookmarkItem putBookmarkShowing(ContentValues v) {
        v.put(BOOKMARK_SHOW_MAIN, show ? 1 : 0);

        return this;
    }

    public byte[] getBookmarkIcon() {
        return this.icon;
    }

    public BookmarkItem setBookmarkIcon(byte[] icon) {
        this.icon = icon;
        return this;
    }

    public BookmarkItem setBookmarkIcon(Cursor c) {
        this.icon = blob(c, BOOKMARK_ICON);

        return this;
    }

    public BookmarkItem putBookmarkIcon(ContentValues v) {
        v.put(BOOKMARK_ICON, icon);

        return this;
    }

    public long getBookmarkAt() {
        return this.bookmarkAt;
    }

    public BookmarkItem setBookmarkAt(long at) {
        this.bookmarkAt = at;

        return this;
    }

    public BookmarkItem setBookmarkAt(Cursor c) {
        this.bookmarkAt = l(c, BOOKMARK_AT);

        return this;
    }

    public BookmarkItem putBookmarkAt(ContentValues v) {
        v.put(BOOKMARK_AT, bookmarkAt);

        return this;
    }

    public long getBookmarkOrder() {
        return this.bookmarkOrder;
    }

    public BookmarkItem setBookmarkOrder(int order) {
        this.bookmarkOrder = order;

        return this;
    }

    public BookmarkItem setBookmarkOrder(Cursor c) {
        this.bookmarkOrder = l(c, BOOKMARK_ORDER);

        return this;
    }

    public BookmarkItem putBookmarkOrder(ContentValues v) {
        v.put(BOOKMARK_ORDER, bookmarkOrder);

        return this;
    }


}
