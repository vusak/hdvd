package com.ne.hdv.common;


import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

public class DownloadGuideDialog extends BaseDialogFragment {

    public static final String TAG = DownloadGuideDialog.class.getSimpleName();

    Button okButton, reportButton;
    TextView message, title;
    boolean isYoutube = false;

    public static DownloadGuideDialog newInstance(String tag, boolean isYoutube) {
        DownloadGuideDialog d = new DownloadGuideDialog();

        d.isYoutube = isYoutube;
        d.createArguments(tag);

        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_guide_download;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

        initializeViews();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() == null)
            return;

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (getDialog() == null || getDialog().getWindow() == null)
                return;

            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.85f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initializeViews() {
        title = fv(R.id.text_title);
        title.setText(isYoutube ? r.s(R.string.title_no_download_youtube) : r.s(R.string.title_no_video_resource));

        message = fv(R.id.text_message);
        SpannableStringBuilder sb = new SpannableStringBuilder();
        if (isYoutube) {
            sb.append(r.s(R.string.message_no_download_youtube_1));
            sb.append(" <font color='#e5533b'>");
            sb.append(r.s(R.string.message_no_download_youtube_2));
            sb.append("</font>");
            sb.append("<br/>");
            sb.append("<br/>");

            int index = sb.toString().indexOf("YouTube");
            sb.insert(index + 7, "</b>");
            sb.insert(index, "<b>");
        }
        sb.append(r.s(R.string.message_no_video_resource));
        message.setText(Html.fromHtml(sb.toString()));

        okButton = fv(R.id.button_got_it);
        okButton.setOnClickListener(v -> {
            dismiss();
        });

        reportButton = fv(R.id.button_report);
        reportButton.setOnClickListener(v -> {
            if (positiveListener != null) {
                positiveListener.onDialogPositive(DownloadGuideDialog.this, DownloadGuideDialog.TAG);
            }
            dismiss();
        });
        reportButton.setVisibility(isYoutube ? View.GONE : View.VISIBLE);
    }
}
