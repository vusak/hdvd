package com.ne.hdv.common;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Cecilia on 2017-05-17.
 */

public class ImageUtil {
    public static void recycleImageView(ImageView iv) {
        if (iv == null)
            return;

//        if (iv instanceof ScaledImageView) {
//            ((ScaledImageView) iv).recycle();
//
//            return ;
//        }

        BitmapDrawable bitmapDrawable = null;

        if (iv.getDrawable() != null)
            bitmapDrawable = ((BitmapDrawable) iv.getDrawable());

        iv.setImageDrawable(null);

        if (bitmapDrawable != null && bitmapDrawable.getBitmap() != null)
            bitmapDrawable.getBitmap().recycle();

        bitmapDrawable = null;
    }

    public static Bitmap resizeBitmap(String path) {
        return resizeBitmap(path, -1, -1);
    }

    public static Bitmap resizeBitmap(String path, int maxWidth, int maxHeight) {
        if (maxWidth <= 0 || maxHeight <= 0)
            return BitmapFactory.decodeFile(path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inSampleSize = getInSampleSize((int) ((float) options.outWidth / (float) maxWidth));

        return resizeBitmap(BitmapFactory.decodeFile(path, options1), maxWidth, maxHeight);
    }

    public static Bitmap resizeBitmap(Resources r, int resId) {
        return resizeBitmap(r, resId, -1, -1);
    }

    public static Bitmap resizeBitmap(Resources r, int resId, int maxWidth, int maxHeight) {
        try {
            if (maxWidth <= 0 || maxHeight <= 0)
                return BitmapFactory.decodeResource(r, resId);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(r, resId, options);

            BitmapFactory.Options options1 = new BitmapFactory.Options();
            options1.inSampleSize = getInSampleSize((int) ((float) options.outWidth / (float) maxWidth));

            return resizeBitmap(BitmapFactory.decodeResource(r, resId, options1), maxWidth, maxHeight);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Bitmap resizeBitmap(Bitmap orgBitmap, int maxWidth, int maxHeight) {
        if (orgBitmap == null)
            return null;

        int orgWidth = orgBitmap.getWidth();
        int orgHeight = orgBitmap.getHeight();

        float orgRatio = (float) orgWidth / (float) orgHeight;
        float viewRatio = (float) maxWidth / (float) maxHeight;

        int scaledWidth;
        int scaledHeight;
        float scale;

        if (orgRatio >= viewRatio) {
            scale = (float) maxWidth / (float) orgWidth;
            scaledWidth = maxWidth;
            scaledHeight = (int) ((float) orgHeight * scale);
        } else {
            scale = (float) maxHeight / (float) orgHeight;
            scaledHeight = maxHeight;
            scaledWidth = (int) ((float) orgWidth * scale);
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(orgBitmap, scaledWidth, scaledHeight, true);
        if (scaledBitmap != orgBitmap)
            orgBitmap.recycle();

        return scaledBitmap;
    }

    private static int getInSampleSize(int ratio) {
        int i = 1;

        while (i <= ratio)
            i *= 2;

        return Math.max(i / 2, i);
    }

    public static Bitmap getThumbnailFromFilepath(String path) {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;

        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(path);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mediaMetadataRetriever != null)
                mediaMetadataRetriever.release();
        }

        return bitmap;
    }

    public static Bitmap saveThumbnailFromURL(String url, String path) {
        Bitmap bitmap = null;
        try {
            bitmap = new ThumbnailImageLoader().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (bitmap != null) {

            File file = new File(path);
            OutputStream os;
            try {
                os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    public static Bitmap getThumbanilFromURL(String url) {
        try {
            return new ThumbnailImageLoader().execute(url).get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getImageFromURLNonTask(String strUrl) {
        Bitmap bitmap = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(strUrl);
            connection = (HttpURLConnection) url.openConnection();
            InputStream is = connection.getInputStream();

            bitmap = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }

        return bitmap;
    }

    public static byte[] getImageBytes(Bitmap bitmap) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getImage(byte[] image, int width, int height) {
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(image, 0, image.length, options);
            options.inSampleSize = calculateInSampleSize(options, width, height);

            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(image, 0, image.length, options);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth)
                inSampleSize *= 2;
        }

        return inSampleSize;
    }

    private static class ThumbnailImageLoader extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            String path = params[0];
            MediaMetadataRetriever mediaMetadataRetriever = null;

            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(path, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(path);
                bitmap = mediaMetadataRetriever.getFrameAtTime();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }
}
