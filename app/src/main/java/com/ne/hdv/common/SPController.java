package com.ne.hdv.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

/**
 * Created by Cecil on 2017-05-12.
 * SharedPreference Controller
 */

public class SPController {
    public static final String FIRST_SET_HOW_TO_USE = "first_set_how_to_use";
    public static final boolean DEFAULT_FIRST_SET_HOW_TO_USE = true;
    public static final String JOINED_TRENDS = "joined_trend";
    public static final boolean DEFAULT_JOINED_TRENDS = false;
    public static final String DELAY_JOIN_TREND = "delay_join_trend";
    public static final long DEFAULT_DELAY_JOIN_TREND = -1;
    public static final String DELAY_SHOW_AD_INSTANT = "delay_show_ad_instant";
    public static final long DEFAULT_DELAY_SHOW_AD_INSTANT = -1;
    public static final String DELAY_SHOW_AD_FILEMANAGER = "delay_show_ad_filemanager";
    public static final long DEFAULT_DELAY_SHOW_AD_FILEMANAGER = -1;
    public static final String TRENDS_UPDATE_DATE = "trends_update_date";
    public static final long DEFAULT_TRENDS_UPDATE_DATE = System.currentTimeMillis();
    public static final String USE_REPORT = "use_report";
    public static final boolean DEFAULT_USE_REPORT = true;
    public static final String SHOW_DOWNLOAD_BAN = "show_download_ban";
    public static final boolean DEFAULT_SHOW_DOWNLOAD_BAN = true;
    public static final String API_ERROR_LOG = "api_error_log";
    public static final String DEFAULT_API_ERROR_LOG = "";
    public static final String DELAY_SHOW_GIFT_ADS = "delay_show_gift_ads";
    public static final long DEFAULT_DELAY_SHOW_GIFT_ADS = -1;
    private static final String DOWNLOAD_COUNT = "download_count";
    private static final int DEFAULT_DOWNLOAD_COUNT = 0;
    private static final String INTRO_WIDTH = "width";
    private static final int DEFAULT_INTRO_WIDTH = 0;
    private static final String TOTAL_PROGRESS_SHOWING = "total_progress_showing";
    private static final boolean DEFAULT_TOTAL_PROGRESS_SHOWING = true;
    private static final String CONFIRM_FIRST_NOTI = "confirm_first_noti_how_to_use";
    private static final boolean DEFAULT_CONFIRM_FIRST_NOTI = false;
    private static final String CONFIRM_MAIN_TIP = "confirm_main_tip";
    private static final boolean DEFAULT_CONFIRM_MAIN_TIP = false;
    private static final String USE_CELLULAR_DATA = "use_cellular_data";
    private static final boolean DEFAULT_USE_CELLULAR_DATA = false;
    private static final String SET_SEARCH_ENGINE = "set_search_engine";
    private static final int DEFAULT_SEARCH_ENGINE = 0;
    private static final String SET_MAX_DOWNLOADS = "set_max_download";
    private static final int DEFAULT_MAX_DOWNLOADS = 2;
    private static final String SET_USE_TIME_FILTERING = "set_use_time_filtering";
    private static final int DEFAULT_USE_TIME_FILTERING = 0;
    private static final String SET_USE_NOTIFICATION = "set_use_notification";
    private static final boolean DEFAULT_USE_NOTIFICATION = true;
    private static final String SET_USE_SAVE_PASSWORDS = "set_use_save_passwords";
    private static final boolean DEFAULT_USE_SAVE_PASSWORDS = true;
    private static final String DOWNLOAD_LIST_MODE = "download_list_mode";
    private static final boolean DEFAULT_DOWNLOAD_LIST_MODE = false;
    private static final String NOTI_COMPLETE_DOWNLOAD_COUNT = "noti_complete_download_count";
    private static final int DEFAULT_NOTI_COMPLETE_DOWNLOAD_COUNT = 0;

    // premium
    private static final String PREMIUM_MODE = "premium_mode";
    private static final boolean DEFAULT_PREMIUM_MODE = false;
    private static final String SHOW_PREMIUM_BANNER = "show_premium_banner";
    private static final boolean DEFAULT_SHOW_PREMIUM_BANNER = true;
    private static final String DELETE_PREMIUM_BANNER_TIME = "delete_premium_banner_time";
    private static final String SHOW_PREMIUM_BANNER_COUNT = "show_premium_banner_count";
    private static final int DEFAULT_SHOW_PREMIUM_BANNER_COUNT = 0;
    private static final int MAX_SHOW_PREMIUM_BANNER_COUNT = 5;
    private static final long DEFAULT_DELETE_PREMIUM_BANNER_TIME = 0;
    private static final String SHOW_PREMIUM_DIALOG_COUNT = "show_premium_dialog_count";
    private static final int DEFAULT_SHOW_PREMIUM_DIALOG_COUNT = 0;
    private static final String SHOW_PREMIUM_DIALOG_TIME = "show_premium_time";
    private static final long DEFAULT_SHOW_PREMIUM_DIALOG_TIME = 0;
    private static final String PREMIUM_DOWNLOAD_COUNT = "premium_download_count";
    private static final int DEFAULT_PREMIUM_DOWNLOAD_COUNT = 0;

    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getIntroWidth() {
        return sp.getInt(INTRO_WIDTH, DEFAULT_INTRO_WIDTH);
    }

    public void setIntroWidth(int val) {
        put(INTRO_WIDTH, val);
    }

    public boolean isTotalProgressShowing() {
        return sp.getBoolean(TOTAL_PROGRESS_SHOWING, DEFAULT_TOTAL_PROGRESS_SHOWING);
    }

    public void setTotalProgressShowing(boolean val) {
        put(TOTAL_PROGRESS_SHOWING, val);
    }

    public boolean isConfirmFirstNoti() {
        return sp.getBoolean(CONFIRM_FIRST_NOTI, DEFAULT_CONFIRM_FIRST_NOTI);

    }

    public void setConfirmFirstNoti(boolean val) {
        put(CONFIRM_FIRST_NOTI, val);
    }

    public boolean isConfirmMainTip() {
        return sp.getBoolean(CONFIRM_MAIN_TIP, DEFAULT_CONFIRM_MAIN_TIP);
    }

    public void setConfirmMainTip(boolean val) {
        put(CONFIRM_MAIN_TIP, val);
    }

    public boolean isUseCellularData() {
        return sp.getBoolean(USE_CELLULAR_DATA, DEFAULT_USE_CELLULAR_DATA);
    }

    public void setUseCellularData(boolean val) {
        put(USE_CELLULAR_DATA, val);
    }

    public int getSearchEngine() {
        return sp.getInt(SET_SEARCH_ENGINE, DEFAULT_SEARCH_ENGINE);
    }

    public void setSearchEngine(int val) {
        put(SET_SEARCH_ENGINE, val);
    }

    public int getMaxDownloads() {
        return sp.getInt(SET_MAX_DOWNLOADS, DEFAULT_MAX_DOWNLOADS);
    }

    public void setMaxDownloads(int val) {
        put(SET_MAX_DOWNLOADS, val);
    }

    public int getTimeFiltering() {
        return sp.getInt(SET_USE_TIME_FILTERING, DEFAULT_USE_TIME_FILTERING);
    }

    public void setTimeFiltering(int val) {
        put(SET_USE_TIME_FILTERING, val);
    }

    public boolean isUseNotification() {
        return sp.getBoolean(SET_USE_NOTIFICATION, DEFAULT_USE_NOTIFICATION);
    }

    public void setUseNotification(boolean val) {
        put(SET_USE_NOTIFICATION, val);
    }

    public boolean isSavePasswords() {
        return sp.getBoolean(SET_USE_SAVE_PASSWORDS, DEFAULT_USE_SAVE_PASSWORDS);
    }

    public void setSetUseSavePasswords(boolean val) {
        put(SET_USE_SAVE_PASSWORDS, val);
    }

    public boolean isDownloadListMode() {
        return sp.getBoolean(DOWNLOAD_LIST_MODE, DEFAULT_DOWNLOAD_LIST_MODE);
    }

    public void setDownloadListMode(boolean val) {
        put(DOWNLOAD_LIST_MODE, val);
    }

    public boolean isFirstSetHowToUse() {
        return sp.getBoolean(FIRST_SET_HOW_TO_USE, DEFAULT_FIRST_SET_HOW_TO_USE);
    }

    public void setFirstSetHowToUse(boolean val) {
        put(FIRST_SET_HOW_TO_USE, val);
    }

    public boolean isJoinedTrends() {
        return sp.getBoolean(JOINED_TRENDS, DEFAULT_JOINED_TRENDS);
    }

    public void setJoinedTrends(boolean val) {
        put(JOINED_TRENDS, val);
    }

    public boolean isDelayedJoinTrends() {
        long delay = sp.getLong(DELAY_JOIN_TREND, DEFAULT_DELAY_JOIN_TREND);
        long curr = System.currentTimeMillis();

        return Math.abs(curr - delay) > (1000 * 60 * 60 * 24);

//        return delay == -1 || Math.abs(curr - delay) > (1000 * 30);
    }

    public void setDelayJoinTrends(long val) {
        put(DELAY_JOIN_TREND, val);
    }

    public boolean isDelayedShowADInstant() {
        long delay = sp.getLong(DELAY_SHOW_AD_INSTANT, DEFAULT_DELAY_SHOW_AD_INSTANT);
        long curr = System.currentTimeMillis();

        return delay == -1 || Math.abs(curr - delay) > (1000 * 60 * 60 * 24);

//        return delay == -1 || Math.abs(curr - delay) > (1000 * 30);
    }

    public void setDelayShowAdInstant(long val) {
        put(DELAY_SHOW_AD_INSTANT, val);
    }

    public boolean isDelayedShowADFilemanager() {
        long delay = sp.getLong(DELAY_SHOW_AD_FILEMANAGER, DEFAULT_DELAY_SHOW_AD_FILEMANAGER);
        long curr = System.currentTimeMillis();

        return delay == -1 || Math.abs(curr - delay) > (1000 * 60 * 60 * 24);

//        return delay == -1 || Math.abs(curr - delay) > (1000 * 30);
    }

    public void setDelayShowAdFilemanager(long val) {
        put(DELAY_SHOW_AD_FILEMANAGER, val);
    }

    public void setDelayShowGiftAds(long val) {
        put(DELAY_SHOW_GIFT_ADS, val);
    }

    public boolean isDelayedShowGiftAds() {
        long delay = sp.getLong(DELAY_SHOW_GIFT_ADS, DEFAULT_DELAY_SHOW_GIFT_ADS);
        long curr = System.currentTimeMillis();

        return delay == -1 || Math.abs(curr - delay) > (1000 * 60 * 60 * 24);
    }

    public long getTrendsUpdateDate() {
        return sp.getLong(TRENDS_UPDATE_DATE, DEFAULT_TRENDS_UPDATE_DATE);
    }

    public void setTrendsUpdateDate(long val) {
        put(TRENDS_UPDATE_DATE, val);
    }

    public boolean isUseReport() {
        return sp.getBoolean(USE_REPORT, DEFAULT_USE_REPORT);
    }

    public void setUseReport(boolean val) {
        put(USE_REPORT, val);
    }

    public boolean isShowDownloadBan() {
        return sp.getBoolean(SHOW_DOWNLOAD_BAN, DEFAULT_SHOW_DOWNLOAD_BAN);
    }

    public void setShowDownloadBan(boolean val) {
        put(SHOW_DOWNLOAD_BAN, val);
    }

    public String getApiErrorLog() {
        return sp.getString(API_ERROR_LOG, DEFAULT_API_ERROR_LOG);
    }

    public void setApiErrorLog(String val) {
        put(API_ERROR_LOG, val);
    }

    public int getNotiCompleteDownloadCount() {
        return sp.getInt(NOTI_COMPLETE_DOWNLOAD_COUNT, DEFAULT_NOTI_COMPLETE_DOWNLOAD_COUNT);
    }

    public void setNotiCompleteDownloadCount(int val) {
        put(NOTI_COMPLETE_DOWNLOAD_COUNT, val);
    }

    public void addNotiCompleteDownloadCount() {
        put(NOTI_COMPLETE_DOWNLOAD_COUNT, getNotiCompleteDownloadCount() + 1);
    }

    // premium
    public boolean isPremiumMode() {
        return sp.getBoolean(PREMIUM_MODE, DEFAULT_PREMIUM_MODE);
    }

    public void setPremiumMode(boolean val) {
        put(PREMIUM_MODE, val);
    }

    public boolean isShowPremiumBanner() {
        long delay = getDeletePremiumBannerTime();
        long curr = System.currentTimeMillis();
        return !isPremiumMode()
                && (getShowPremiumBannerCount() < MAX_SHOW_PREMIUM_BANNER_COUNT)
                && Math.abs(curr - delay) > (1000 * 60 * 60 * 48);
//        return sp.getBoolean(SHOW_PREMIUM_BANNER, DEFAULT_SHOW_PREMIUM_BANNER);
    }

    public void setShowPremiumBanner(boolean val) {
        put(SHOW_PREMIUM_BANNER, val);
    }

    public long getDeletePremiumBannerTime() {
        return sp.getLong(DELETE_PREMIUM_BANNER_TIME, DEFAULT_DELETE_PREMIUM_BANNER_TIME);
    }

    public void setDeletePremiumBannerTime(long val) {
        put(DELETE_PREMIUM_BANNER_TIME, val);
        setShowPremiumBannerCount(getShowPremiumBannerCount() + 1);
    }

    public int getShowPremiumBannerCount() {
        return sp.getInt(SHOW_PREMIUM_BANNER_COUNT, DEFAULT_SHOW_PREMIUM_BANNER_COUNT);
    }

    public void setShowPremiumBannerCount(int val) {
        put(SHOW_PREMIUM_BANNER_COUNT, val);
    }

    public int getShowPremiumDialogCount() {
        return sp.getInt(SHOW_PREMIUM_DIALOG_COUNT, DEFAULT_SHOW_PREMIUM_DIALOG_COUNT);
    }

    public void setShowPremiumDialogCount(int val) {
        put(SHOW_PREMIUM_DIALOG_COUNT, val);
    }

    public long getShowPremiumDialogTime() {
        return sp.getLong(SHOW_PREMIUM_DIALOG_TIME, DEFAULT_SHOW_PREMIUM_DIALOG_TIME);
    }

    public void setShowPremiumDialogTime(long val) {
        put(SHOW_PREMIUM_DIALOG_TIME, val);
    }

    public int getPremiumDownloadCount() {
        return sp.getInt(PREMIUM_DOWNLOAD_COUNT, DEFAULT_PREMIUM_DOWNLOAD_COUNT);
    }

    public void setPremiumDownloadCount(int val) {
        put(PREMIUM_DOWNLOAD_COUNT, val);
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    public int getDownloadCount() {
        return sp.getInt(DOWNLOAD_COUNT, DEFAULT_DOWNLOAD_COUNT);
    }

    public void setDownloadCount(int val) {
        put(DOWNLOAD_COUNT, val);
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}
