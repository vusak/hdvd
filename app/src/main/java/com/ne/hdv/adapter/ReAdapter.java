package com.ne.hdv.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.ne.hdv.common.ImageUtil;

import java.util.List;

/**
 * Created by Cecilia on 2017-05-17.
 */

public abstract class ReAdapter<T extends Object, VH extends ReAbstractViewHolder> extends RecyclerView.Adapter<VH> {
    protected ReOnItemClickListener<T> listener;

    protected List<T> items;
    protected int layoutId;

    protected Context context;

    public ReAdapter(int layoutId, List<T> items, Context context) {
        this.layoutId = layoutId;
        this.items = items;
        this.context = context;
    }

    @Override
    public void onViewRecycled(VH holder) {
        for (ImageView iv : holder.getRecyclableImageViews()) {
            ImageUtil.recycleImageView(iv);
        }
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getLayoutId() {
        return this.layoutId;
    }

    public List<T> getItems() {
        return this.items;
    }

    public interface ReOnItemClickListener<T> {
        void OnItemClick(int position, T item);

        void OnItemLongClick(int position, T item);
    }
}
