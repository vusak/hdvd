package com.ne.hdv.common;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class Util {
    private static final String HEXES = "0123456789ABCDEF";
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault());
    //    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM, dd, yyyy", Locale.US);
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);

    public static Activity getActivity(Object object) {
        return object instanceof Activity ? (Activity) object : ((Fragment) object).getActivity();
    }

    public static boolean isTablet(Context context) {
        Configuration config = context.getResources().getConfiguration();

        if (Build.VERSION.SDK_INT >= 13)
            return config.smallestScreenWidthDp >= 600;
        else
            return false;
    }

    public static String getMACAddress() {
        String macaddress = "";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase("wlan0"))
                    continue;
                byte[] mac = intf.getHardwareAddress();
                if (mac != null) {
                    StringBuilder buf = new StringBuilder();
                    for (int idx = 0; idx < mac.length; idx++)
                        buf.append(String.format("%02X:", mac[idx]));
                    if (buf.length() > 0)
                        buf.deleteCharAt(buf.length() - 1);
                    macaddress = buf.toString();
                }
            }
        } catch (Exception ex) {
        }

        return TextUtils.isEmpty(macaddress) ? "00:00:00:00:00:00" : macaddress;
    }

    public static String getVersion(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "0.0.0";
        }
    }

    public static boolean isNetworkAbailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            return true;

        } else
            return false;
    }

    public static String getNetworkConnectionType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null ? null : activeNetworkInfo.getTypeName();
    }

    public static void lockRotatation(Activity activity) {
        switch (activity.getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                break;

            case Configuration.ORIENTATION_LANDSCAPE:
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                break;
        }
    }

    public static void unlockRotation(Activity activity) {
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null)
            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    public static void hideKeyBoard(View v) {
        v.clearFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void showKeyBoard(View v) {
        v.requestFocus();
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
    }

    public static <T extends BaseFragment> T addf(FragmentManager fm, String tag, BaseFragment.BaseFragmentCreator<T> creator) {
        T t = null;
        BaseFragment f = ff(fm, tag);
        if (f != null)
            t = (T) f;
        else {
            t = creator.create();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(creator.getFrameId(), t, tag);
            ft.commit();
        }

        return t;
    }

    public static <T extends BaseFragment> T ff(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            return null;
        }
    }

    // find dialog fragment
    @SuppressWarnings("unchecked")
    public static <T extends BaseDialogFragment> T fdf(FragmentManager fm, String tag) {
        if (fm == null || tag == null)
            return null;

        Fragment f = fm.findFragmentByTag(tag);

        try {
            return (T) f;
        } catch (Exception e) {
            return null;
        }
    }

    // hide dialog fragment
    public static void hdf(FragmentManager fm, String tag) {
        DialogFragment prev = fdf(fm, tag);
        if (prev != null)
            prev.dismissAllowingStateLoss();
    }

    // show dialog fragment
    public static void sdf(FragmentManager fm, BaseDialogFragment d) {
        if (fm == null || d == null || d.getDialogTag() == null)
            return;

        hdf(fm, d.getDialogTag());
        try {
            d.show(fm, d.getDialogTag());
        } catch (Exception e) {
        }
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String toHexString(byte[] bytes) {
        final StringBuilder builder = new StringBuilder(2 * bytes.length);
        for (final byte b : bytes) {
            builder.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
        }
        return builder.toString();
    }

    public static String sha256(String s) {
        byte[] bytes = s.getBytes();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            algorithm.reset();
            algorithm.update(bytes);
            byte result[] = algorithm.digest();

            return toHexString(result);
        } catch (NoSuchAlgorithmException e) {
            return "00000000000000000000000000000000";
        }
    }

    public static String optString(JSONObject o, String name) {
        return o.isNull(name) ? null : o.optString(name);
    }


    public static String optString(JSONArray a, int index) {
        return a.isNull(index) ? null : a.optString(index);
    }

    public static String getTimeString(long date) {
        return timeFormat.format(new Date(date));
    }

    public static String getDateString(long date) {
        return dateFormat.format(new Date(date));
    }

    public static String getFullDateString(long date) {
        return format.format(new Date(date));
    }

    public static void showToast(Activity context, String message, boolean center) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_toast,
                (ViewGroup) context.findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.text_message);
        text.setText(message);

        Toast toast = new Toast(context.getApplicationContext());
        if (center)
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }


}
