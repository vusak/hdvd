package com.ne.hdv.common;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;

import java.util.ArrayList;


public class SettingsDialog extends BaseDialogFragment {

    public static final String TAG = SettingsDialog.class.getSimpleName();

    TextView titleText;
    Button cancelButton, okButton;
    RadioGroup radioGroup;
    RadioButton radioButton1, radioButton2, radioButton3, radioButton4, radioButton5, radioButton6;

    String title;
    ArrayList<String> list;
    int curr;

    public static SettingsDialog newInstance(String tag, String title, ArrayList<String> list, int curr) {
        SettingsDialog d = new SettingsDialog();

        d.createArguments(tag);
        d.title = title;
        d.list = list;
        d.curr = curr;
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_settings;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        titleText = fv(R.id.text_dlg_title);
        setTitle();

        cancelButton = fv(R.id.btn_negative);
        okButton = fv(R.id.btn_positive);

        radioGroup = fv(R.id.radio_group);

        radioButton1 = fv(R.id.radio_1);
        radioButton2 = fv(R.id.radio_2);
        radioButton3 = fv(R.id.radio_3);
        radioButton4 = fv(R.id.radio_4);
        radioButton5 = fv(R.id.radio_5);
        radioButton6 = fv(R.id.radio_6);
        setContents();

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (positiveListener != null)
                    positiveListener.onDialogPositive(SettingsDialog.this, list.get(getIndexById(radioGroup.getCheckedRadioButtonId())));
                dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (negativeListener != null)
                    negativeListener.onDialogNegative(SettingsDialog.this, tag);

                dismiss();
            }
        });
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void setTitle() {
        if (titleText != null)
            titleText.setText(title);
    }

    public void setContents() {
        if (list == null)
            return;

        if (list.size() >= 1 && radioButton1 != null) {
            radioButton1.setVisibility(View.VISIBLE);
            radioButton1.setText(list.get(0));
        }
        if (list.size() >= 2 && radioButton2 != null) {
            radioButton2.setVisibility(View.VISIBLE);
            radioButton2.setText(list.get(1));
        }
        if (list.size() >= 3 && radioButton3 != null) {
            radioButton3.setVisibility(View.VISIBLE);
            radioButton3.setText(list.get(2));
        }
        if (list.size() >= 4 && radioButton4 != null) {
            radioButton4.setVisibility(View.VISIBLE);
            radioButton4.setText(list.get(3));
        }
        if (list.size() >= 5 && radioButton5 != null) {
            radioButton5.setVisibility(View.VISIBLE);
            radioButton5.setText(list.get(4));
        }
        if (list.size() >= 6 && radioButton6 != null) {
            radioButton6.setVisibility(View.VISIBLE);
            radioButton6.setText(list.get(5));
        }

        int index = 0;
        if (list.size() > curr)
            index = curr;
        radioGroup.check(getIdByIndex(index));
    }

    private int getIndexById(int id) {
        switch (id) {
            case R.id.radio_1:
                return 0;

            case R.id.radio_2:
                return 1;

            case R.id.radio_3:
                return 2;

            case R.id.radio_4:
                return 3;

            case R.id.radio_5:
                return 4;

            case R.id.radio_6:
                return 5;
        }

        return 0;
    }

    private int getIdByIndex(int index) {
        switch (index) {
            case 0:
                return R.id.radio_1;

            case 1:
                return R.id.radio_2;

            case 2:
                return R.id.radio_3;

            case 3:
                return R.id.radio_4;

            case 4:
                return R.id.radio_5;

            case 5:
                return R.id.radio_6;
        }

        return R.id.radio_1;
    }
}
