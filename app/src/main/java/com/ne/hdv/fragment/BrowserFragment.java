package com.ne.hdv.fragment;


import android.animation.Animator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.ClientCertRequest;
import android.webkit.CookieSyncManager;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.ne.hdv.R;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.base.ExpandLayout;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.DownloadGuideDialog;
import com.ne.hdv.common.ImageUtil;
import com.ne.hdv.common.LogUtil;
import com.ne.hdv.common.ReportCompleteDialog;
import com.ne.hdv.common.ReportDialog;
import com.ne.hdv.data.BookmarkItem;
import com.ne.hdv.data.FavoriteItem;
import com.ne.hdv.data.ReportItem;
import com.ne.hdv.download.DDownloadLinkProvider;
import com.ne.hdv.download.TDownloadLinkProvider;
import com.ne.hdv.download.VDownloadLinkProvider;

import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class BrowserFragment extends BaseFragment implements GestureDetector.OnGestureListener {

    public WebView webView;
    TextView urlEdit, reportStatusText, reportStatusSubText;
    LinearLayout reportStatusLayout;
    ImageButton refreshButton, backButton; //, reportButton;
    CheckBox bookmarkButton;
    Button reportStatusButton;

    ProgressBar loadingProgress;
    ExpandLayout urlExpand, adExpand;

    ImageButton adCloseButton, downloadButton;
    LottieAnimationView downloadLottie;
    Button adInstantButton;
    ImageView adInstantImage;
    TextView adInstantText;

    String currentTitle, currentSite, addHistoryUrl;
    Bitmap currentIcon;
    GestureDetector detector;

    float initialY = -1;
    String checkUrl = "";
    boolean CLEAR_HISTORY = false;
    boolean SHOW_REPORT = false;

    private BrowserFragmentListsner listener;
    private Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1000) {
                Animation hideAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_left);
                hideAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        reportStatusLayout.setVisibility(View.GONE);
//                        reportButton.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                reportStatusLayout.startAnimation(hideAnim);
            }
        }
    };
    private Toast toast = null;

    @Override
    public void onCreateView(Bundle savedInstanceState) {


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        detector = new GestureDetector(getActivity(), this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_browser;
    }

    @Override
    public void onResume() {
        if (downloadButton != null)
            downloadButton.post(() -> downloadButton.setVisibility(sp.isUseNotification() ? View.VISIBLE : View.GONE));
        super.onResume();

        if (webView != null) {
            webView.resumeTimers();
            webView.onResume();
            webView.reload();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {
            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (webView != null) {
            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }
    }

    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
            webView.destroy();
        }

        super.onDestroy();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (distanceY > 0 && Math.abs(initialY - distanceY) > 100) {
            if (urlExpand.isExpanded())
                urlExpand.collapse(true);
            initialY = distanceY;
//            reportButton.clearAnimation();
//            reportButton.setVisibility(View.GONE);
            return true;
        } else if (!urlExpand.isExpanded() && distanceY < 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.expand(true);
            initialY = distanceY;
//            reportButton.setVisibility(SHOW_REPORT ? View.VISIBLE : View.GONE);
//            if (SHOW_REPORT) {
//                Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
//                reportButton.startAnimation(anim);
//                reportButton.setEnabled(true);
//            }
            return true;
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    public boolean onPrevButtonClicked() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return false;
    }

    public void onNextButtonClicked() {
        if (webView != null)
            webView.goForward();
    }

    public void setURLString(String url) {
        urlEdit.setText(url);
    }

    public void setListener(BrowserFragmentListsner listener) {
        this.listener = listener;
    }

    public void goUrl(final String url) {
        if (urlExpand != null && !urlExpand.isExpanded()) {
            urlExpand.collapse(false);
            adExpand.collapse(false);
//            reportButton.setVisibility(View.GONE);
        }

        if (urlEdit == null || webView == null)
            return;

        urlEdit.post(() -> urlEdit.setText(url));

        webView.post(() -> {
            webView.onResume();
            webView.loadUrl(url);
        });
    }

    public void startPremiumMode() {
        if (adExpand != null && adExpand.isExpanded())
            adExpand.collapse(false);
    }

    private void initView() {
        if (LogUtil.DEBUG_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        urlExpand = fv(R.id.layout_expend_url);
        urlExpand.collapse(false);

        adExpand = fv(R.id.layout_expend_ad);
        adExpand.collapse(false);

        adCloseButton = fv(R.id.button_ad_close);
        adCloseButton.setOnClickListener(v -> {
            sp.setDelayShowAdInstant(System.currentTimeMillis());
            adExpand.collapse(true);
        });

        adInstantButton = fv(R.id.button_ad_instant);
        adInstantButton.setOnClickListener(v -> {
            sp.setDelayShowAdInstant(System.currentTimeMillis());
            adExpand.collapse(true);
            launchApp(Common.PACKAGE_JM_INSTANT);
        });

        adInstantImage = fv(R.id.image_ad_instant);
        adInstantImage.setOnClickListener(v -> {
            sp.setDelayShowAdInstant(System.currentTimeMillis());
            adExpand.collapse(true);
            launchApp(Common.PACKAGE_JM_INSTANT);
        });

        adInstantText = fv(R.id.text_ad_instant);
        adInstantText.setOnClickListener(v -> {
            sp.setDelayShowAdInstant(System.currentTimeMillis());
            adExpand.collapse(true);
            launchApp(Common.PACKAGE_JM_INSTANT);
        });

        webView = fv(R.id.webview);

        if (!sp.isSavePasswords()) {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(webView.getContext());
            android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeSessionCookies(null);
                cookieManager.removeAllCookies(null);
            } else {
                cookieManager.removeSessionCookie();
                cookieManager.removeAllCookie();
            }
            cookieSyncManager.sync();
        }

        reportStatusText = fv(R.id.text_status_report);
        reportStatusSubText = fv(R.id.text_status_report_sub);

        reportStatusLayout = fv(R.id.layout_status_report);
        reportStatusLayout.setVisibility(View.GONE);

        reportStatusButton = fv(R.id.button_status_report);
        reportStatusButton.setVisibility(View.GONE);
        reportStatusButton.setOnClickListener(v -> {
            SHOW_REPORT = false;
            sp.setShowDownloadBan(false);
            reportStatusLayout.setVisibility(View.GONE);
            toastHandler.removeMessages(1000);
        });

        downloadButton = fv(R.id.button_download);
        downloadButton.setOnClickListener(v -> {
            if (downloadLottie.getVisibility() == View.VISIBLE) {
                if (listener != null)
                    listener.onReadyButtonClicked();
            } else {
                if (fdf(DownloadGuideDialog.TAG) == null) {
                    boolean youtube = urlEdit.getText().toString().matches("(?i).*youtube.*");
                    DownloadGuideDialog dlg = DownloadGuideDialog.newInstance(DownloadGuideDialog.TAG, youtube);
                    dlg.setPositiveListener((dialog, tag) -> {

                        ReportDialog dlg1 = ReportDialog.newInstance(ReportDialog.TAG, urlEdit.getText().toString());
                        dlg1.setPositiveListener((dialog1, tag1) -> {
                            ReportItem item = new ReportItem();
                            item.setReportUrl(webView.getUrl());
                            String site = "";
                            if (!TextUtils.isEmpty(webView.getUrl())) {
                                Uri uri = Uri.parse(webView.getUrl());
                                if (uri != null)
                                    site = uri.getHost();
                            }
                            if (TextUtils.isEmpty(site))
                                site = currentSite;
                            item.setReportSite(site);
                            item.setReportAt(System.currentTimeMillis());
                            String region = app.getSimContry();
                            if (TextUtils.isEmpty(region))
                                region = "WW";
                            item.setReportRegion(region);
                            db.insertOrUpdateReportItem(item);
                            if (listener != null)
                                listener.onSendReport(item);
                            ReportCompleteDialog dlg2 = ReportCompleteDialog.newInstance(ReportCompleteDialog.TAG, urlEdit.getText().toString());
                            sdf(dlg2);
                        });
                        sdf(dlg1);
                    });
                    sdf(dlg);
                }
            }
        });
        downloadButton.setVisibility(sp.isUseNotification() ? View.VISIBLE : View.GONE);

        downloadLottie = fv(R.id.lottie_download);
        downloadLottie.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        downloadLottie.setVisibility(View.GONE);

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDatabaseEnabled(sp.isSavePasswords());
        webView.getSettings().setAppCacheEnabled(sp.isSavePasswords());
        webView.getSettings().setDomStorageEnabled(sp.isSavePasswords());
        webView.getSettings().setSavePassword(sp.isSavePasswords());
        webView.getSettings().setSaveFormData(sp.isSavePasswords());
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        // 20190.03.05 Uncaught (in promise) NotAllowedError: play() can only be initiated by a user gesture.
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }

        webView.setWebViewClient(new MyWebViewClient());
        webView.setOnTouchListener((v, event) -> detector.onTouchEvent(event));
        webView.requestFocus();
        webView.setWebChromeClient(new MyWebChromeClient());

        urlEdit = fv(R.id.text_br_url);
        urlEdit.setOnTouchListener((v, event) -> {
            if (listener != null)
                listener.onUrlEditClick(webView.getUrl());
            return false;
        });

        backButton = fv(R.id.button_br_back);
        backButton.setVisibility(View.GONE);
        backButton.setOnClickListener(v -> onPrevButtonClicked());
        refreshButton = fv(R.id.button_br_refresh);
        refreshButton.setOnClickListener(v -> webView.reload());

        bookmarkButton = fv(R.id.button_bookmark);
        bookmarkButton.setOnClickListener(v -> {
            if (!bookmarkButton.isChecked()) {
                db.deleteBookmarkItem(Common.SITE);
                if (listener != null)
                    listener.onShowToast(r.s(R.string.msg_toast_bookmarked));
            } else {
                if (!checkBookmarkLimit()) {
                    bookmarkButton.setChecked(false);
                    if (listener != null)
                        listener.onShowToast(r.s(R.string.msg_toast_no_more));
                    return;
                }

                if (!TextUtils.isEmpty(urlEdit.getText())) {
                    addBookmarkItem();
                }
            }
//                reportAnimation();
        });

        loadingProgress = fv(R.id.prg);

    }

    private boolean checkBookmarkLimit() {
        return db.getBookmarkItems() == null || db.getBookmarkItems().size() < (Common.MAX_BOOKMARK_COUNT - 2);
    }

    private int getDisplayHeight() {
        return this.getResources().getDisplayMetrics().heightPixels;
    }

    private void addFavoriteItem(String url) {
        if (TextUtils.isEmpty(url))
            return;

        String temp = url.replace("http://", "");
        temp = temp.replace("https://", "");
        FavoriteItem item = db.getFavoriteItem(temp);
        if (item != null) {
            item.setFavoriteCount(item.getFavoriteCount() + 1);
            db.insertOrUpdateFavoriteItem(item);
        } else
            item = new FavoriteItem();
        item.setFavoriteUrl(temp);
        item.setFavoriteSite(url);
        if (TextUtils.isEmpty(currentTitle))
            item.setFavoriteTitle(Uri.parse(temp).getHost());
        else
            item.setFavoriteTitle(currentTitle);
        item.setFavoriteAt(System.currentTimeMillis());
        new AddFavoriteItem().execute(item);
    }

    private void addBookmarkItem() {
        BookmarkItem item = new BookmarkItem();
        item.setBookmarkId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setBookmarkUrl(Common.SITE);
        item.setBookmarkName(currentTitle);
        item.setBookmarkShowing(false);
        item.setBookmarkOrder((int) db.getNextDBId(BookmarkItem.TABLE_NAME));
        item.setBookmarkAt(System.currentTimeMillis());

        new AddBookmarkItem().execute(item);
    }

    private void setBookmarkButton(String url) {
        bookmarkButton.setVisibility(View.VISIBLE);
        boolean check = db.getBookmarkItem(url) != null;
        bookmarkButton.setChecked(check);

        String site = "";
        if (!TextUtils.isEmpty(url)) {
            Uri uri = Uri.parse(url);
            if (uri != null)
                site = uri.getHost();
        }

        if (url.matches("(?i).*instagram.*")
                && !getPackageList(Common.PACKAGE_JM_INSTANT) && sp.isDelayedShowADInstant() && !sp.isPremiumMode()) {
            adExpand.expand(true);
        }
    }

    private void showToastMessage(String message) {
        if (toast == null)
            toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);

        toast.setText(message);
        toast.show();
    }

    public void clearCurrData() {
        currentIcon = null;
        currentTitle = "";
        currentSite = "";
        addHistoryUrl = "";

        if (downloadLottie != null)
            downloadLottie.setVisibility(View.GONE);
        if (listener != null)
            listener.onClearReadyList();
    }

    public void stopWebViewLoading() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
        }
    }

    private void launchApp(String packageName) {
        try {
            if (getPackageList(packageName)) {
                Intent intent = getActivity().getPackageManager()
                        .getLaunchIntentForPackage(packageName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    private boolean getPackageList(String packageName) {
        boolean isExist = false;

        PackageManager pkgMgr = getActivity().getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith(packageName)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    public void readyDownload(boolean show) {
        LogUtil.log("readyDownload()");
        if (show) {
            downloadLottie.post(() -> {
                downloadLottie.setVisibility(View.VISIBLE);
                downloadLottie.setRepeatCount(2);
                downloadLottie.playAnimation();
            });
        } else
            downloadLottie.post(() -> downloadLottie.setVisibility(View.GONE));
    }

    public static interface BrowserFragmentListsner {
        public void onUrlEditClick(String url);

        public void onAccessYoutebe();

        public void onAccessDownloadUrl(String site, String url);

        public void onWebviewScrolled();

        public void onSendReport(ReportItem item);

        public void onShowToast(String message);

        public void onReadyButtonClicked();

        public void onClearReadyList();
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            if (url.equals("about:blank")) {
                backButton.setVisibility(View.GONE);
                urlEdit.setText("");
                clearCurrData();
            }
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            loadingProgress.setVisibility(View.VISIBLE);
            refreshButton.setVisibility(View.GONE);

            urlExpand.collapse(false);
            adExpand.collapse(false);
            clearCurrData();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (CLEAR_HISTORY) {
                view.clearHistory();
                CLEAR_HISTORY = false;
            }

            compareText = url;
            loadingProgress.setVisibility(View.INVISIBLE);
            refreshButton.setVisibility(View.VISIBLE);
            backButton.setVisibility(webView.canGoBack() ? View.VISIBLE : View.GONE);
            if (url.equals("about:blank") || Common.SITE.equals(compareText)) {
                return;
            }

            Common.SITE = url;
            urlEdit.setText(url);
            currentSite = url;
            urlExpand.collapse(false);
            adExpand.collapse(false);
            setBookmarkButton(url);

            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
        }


        @Override
        public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
            super.onReceivedClientCertRequest(view, request);
        }

        @Override
        public void onLoadResource(WebView view, final String url) {
            if (!Common.checkAvailableDownload(url).equals("novideo")) { // url에 비디오 관련 확장자 있음
                if (!checkUrl.equals(url)) {
                    checkUrl = url;
                    Thread t = new Thread(() -> {
                        try {
                            URLConnection u = new URL(url).openConnection();
                            String type = u.getHeaderField("Content-Type");

                            if (url.matches("(?i).*youtube.*")) {

                            } else if (type.matches("video.*")) {

                                if (url.contains("dailymotion.com/sec(")) {
                                    /** Daily Motion
                                     if (url.contains("frag(2)")) {
                                     String videoLink = currentSite;
                                     int index = currentSite.indexOf("?playlist");
                                     if (index > 0)
                                     videoLink = currentSite.substring(0, index);
                                     LogUtil.log("DAILYMOTION VIDEO LINK :: " + videoLink);
                                     if (!TextUtils.isEmpty(videoLink)) {
                                     new generateDDownloadLink().execute(videoLink);
                                     }
                                     }
                                     **/
                                } else if (listener != null) {
                                    listener.onAccessDownloadUrl(currentSite, url);

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });

                    t.start();
                }
            } else if (url.contains("skyfiregce-vimeo") && url.contains("master.json")) {
                LogUtil.log("onLoadResource :: " + url);
                String[] temp = url.split("/");
                Pattern p = Pattern.compile("(^[0-9]*$)");

                String videoId = "";
                if (temp != null && temp.length > 0) {
                    for (String v : temp) {
                        LogUtil.log("check url :: " + v);
                        if (!TextUtils.isEmpty(v) && p.matcher(v).find()) {
                            videoId = v;
                            break;
                        }
                    }
                }
                if (!TextUtils.isEmpty(videoId)) {
                    new generateVDownloadLink().execute(videoId);

                }
            } else if (url.contains("clips.twitch.tv") && url.contains("/view")) {
                LogUtil.log("HDVD URL (onLoadResource) :: " + url);
                String source = url.replace("view", "status");
                if (!TextUtils.isEmpty(source))
                    new generateTDownloadLink().execute(source);
            }
            super.onLoadResource(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            addFavoriteItem(currentSite);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
            if (Common.checkAvailableDownload(url).equals("novideo") && url.matches("http.*")) { // url에 비디오 관련 확장자 없음
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else
//                    view.loadUrl(url);
                    return false;
//
            } else if (url.matches("intent.*")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                    view.loadUrl(fallbackUrl);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                if (!checkUrl.equals(url)) {
                    checkUrl = url;
                    Thread t = new Thread(() -> {
                        try {
                            URLConnection u = new URL(url.toString()).openConnection();
                            String type = u.getHeaderField("Content-Type");
                            if (url.matches("(?i).*youtube.*")) {

                            } else if (type.matches("video.*")) {
//                                    downloadHandler.sendEmptyMessage(0);
                                LogUtil.log("HDVD URL (shouldOverrideUrlLoading) :: " + url);
                                if (listener != null) {
                                    listener.onAccessDownloadUrl(currentSite, url);
                                }
                            } else {
                                view.loadUrl(url);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });

                    t.start();
                }
            }

            return true;
        }
    }

    private class MyWebChromeClient extends WebChromeClient implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            currentTitle = title;
            currentSite = view.getOriginalUrl();
        }


        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
            currentIcon = icon;
            setBookmarkButton(Common.SITE);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            loadingProgress.setProgress(newProgress);
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            super.onShowCustomView(view, callback);
        }

        @Override
        public void onCompletion(MediaPlayer mp) {

        }

        @Override
        public void onPrepared(MediaPlayer mp) {

        }
    }

    class AddFavoriteItem extends AsyncTask<FavoriteItem, Void, FavoriteItem> {
        @Override
        protected FavoriteItem doInBackground(FavoriteItem... params) {
            FavoriteItem item = params[0];
            String path = item.getFavoriteUrl();
            Bitmap bitmap = null;

            if (!TextUtils.isEmpty(path)) {
                String host = Uri.parse(path).getHost();
                String iconUrl = host + "/favicon.ico";
                if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                    iconUrl = "http://" + iconUrl;
                bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);
            }

            if (bitmap != null)
                item.setFavoriteIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setFavoriteIcon(ImageUtil.getImageBytes(currentIcon));

            return item;

        }

        @Override
        protected void onPostExecute(FavoriteItem item) {
            db.insertOrUpdateFavoriteItem(item);
        }
    }

    class AddBookmarkItem extends AsyncTask<BookmarkItem, Void, BookmarkItem> {
        @Override
        protected BookmarkItem doInBackground(BookmarkItem... params) {
            BookmarkItem item = params[0];
            String path = item.getBookmarkUrl();
            Bitmap bitmap = null;
            if (!TextUtils.isEmpty((path))) {
                String host = Uri.parse(path).getHost();
                String iconUrl = host + "/favicon.ico";
                if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                    iconUrl = "http://" + iconUrl;

                bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);
                if (TextUtils.isEmpty(item.getBookmarkName()))
                    item.setBookmarkName(host);
            }
            if (bitmap != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setBookmarkIcon(ImageUtil.getImageBytes(currentIcon));


            return item;
        }

        @Override
        protected void onPostExecute(BookmarkItem item) {
            db.insertOrUpdateBookmarkItem(item);
            if (listener != null)
                listener.onShowToast(r.s(R.string.msg_toast_bookmarked));
        }
    }

    /**
     * Twitch Clip
     */
    class generateTDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                TDownloadLinkProvider provider = new TDownloadLinkProvider(videoId);
                downloadUrl = provider.getVideoLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

    /**
     * DailyMotion
     **/
    class generateDDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                DDownloadLinkProvider provider = new DDownloadLinkProvider(videoId);
                downloadUrl = provider.getVideoLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

    /**
     * Vimeo
     **/
    class generateVDownloadLink extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String videoId = strings[0];
            String downloadUrl = "";
            try {
                VDownloadLinkProvider provider = new VDownloadLinkProvider(videoId);
                downloadUrl = provider.generateDownloadLink();
            } catch (Exception e) {
                LogUtil.log(e.getMessage());
            }
            return downloadUrl;
        }

        @Override
        protected void onPostExecute(String downloadUrl) {
            super.onPostExecute(downloadUrl);
            if (listener != null)
                listener.onAccessDownloadUrl(currentSite, downloadUrl);
        }
    }

}
