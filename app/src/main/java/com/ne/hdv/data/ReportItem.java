package com.ne.hdv.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import com.ne.hdv.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class ReportItem extends TableObject {
    public static final String TABLE_NAME = "report";

    public static final String ROW_ID = "id";

    public static final String REPORT_SITE = "report_site";
    public static final String REPORT_URL = "report_url";
    public static final String REPORT_REGION = "report_region";
    public static final String REPORT_AT = "report_at";
    public static final Creator<ReportItem> CREATOR = new Creator<ReportItem>() {
        @Override
        public ReportItem createFromParcel(Parcel source) {
            return new ReportItem(source);
        }

        @Override
        public ReportItem[] newArray(int size) {
            return new ReportItem[size];
        }
    };
    private long id;
    private String url;
    private String site;
    private String region;
    private long reportAt;

    public ReportItem() {
        super();
    }


    public ReportItem(Parcel in) {
        super(in);
        this.id = in.readLong();
        this.url = in.readString();
        this.site = in.readString();
        this.region = in.readString();
        this.reportAt = in.readLong();
    }

    public static void createReportTable(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(REPORT_SITE + " text not null unique, ")
                .append(REPORT_URL + " text, ")
                .append(REPORT_REGION + " text, ")
                .append(REPORT_AT + " integer) ")
                .toString());
    }

    public static ReportItem getReportItem(SQLiteDatabase db, String site) {
        List<ReportItem> list = getReportItems(db, null, REPORT_SITE + " = ?", new String[]{site}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ReportItem> getReportItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ReportItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ReportItem item = new ReportItem()
                        .setRowId(c)
                        .setReportUrl(c)
                        .setReportSite(c)
                        .setReportRegion(c)
                        .setReportAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateReportItem(SQLiteDatabase db, ReportItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putReportSite(v)
                    .putReportUrl(v)
                    .putReportRegion(v)
                    .putReportAt(v);

            int rowAffected = db.update(TABLE_NAME, v, REPORT_SITE + " =? ", new String[]{item.getReportSite()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getReportSite());
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteReportItem(SQLiteDatabase db, ReportItem item) {
        try {
            db.delete(TABLE_NAME, REPORT_SITE + " = ? ", new String[]{item.getReportSite()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllReportItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(url);
        out.writeString(site);
        out.writeString(region);
        out.writeLong(reportAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ReportItem && site.equalsIgnoreCase(((ReportItem) obj).site);
    }

    public long getRowId() {
        return this.id;
    }

    public ReportItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ReportItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ReportItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getReportUrl() {
        return this.url;
    }

    public ReportItem setReportUrl(String url) {
        this.url = url;

        return this;
    }

    public ReportItem setReportUrl(Cursor c) {
        this.url = s(c, REPORT_URL);

        return this;
    }

    public ReportItem putReportUrl(ContentValues v) {
        v.put(REPORT_URL, url);

        return this;
    }

    public String getReportSite() {
        return this.site;
    }

    public ReportItem setReportSite(String site) {
        this.site = site;

        return this;
    }

    public ReportItem setReportSite(Cursor c) {
        this.site = s(c, REPORT_SITE);

        return this;
    }

    public ReportItem putReportSite(ContentValues v) {
        v.put(REPORT_SITE, site);

        return this;
    }

    public String getReportRegion() {
        return this.region;
    }

    public ReportItem setReportRegion(String region) {
        this.region = region;

        return this;
    }

    public ReportItem setReportRegion(Cursor c) {
        this.region = s(c, REPORT_REGION);

        return this;
    }

    public ReportItem putReportRegion(ContentValues v) {
        v.put(REPORT_REGION, region);

        return this;
    }

    public long getReportAt() {
        return this.reportAt;
    }

    public ReportItem setReportAt(long at) {
        this.reportAt = at;

        return this;
    }

    public ReportItem setReportAt(Cursor c) {
        this.reportAt = l(c, REPORT_AT);


        return this;
    }

    public ReportItem putReportAt(ContentValues v) {
        v.put(REPORT_AT, reportAt);

        return this;
    }
}
