package com.ne.hdv.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ne.hdv.R;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.data.DownItem;

import java.io.File;

public class EditDownloadDialog extends BaseDialogFragment {
    public static final String TAG = EditDownloadDialog.class.getSimpleName();

    Context context;
    DownItem item;

    TextView dlgTitleText;
    EditText titleEdit;
    TextView titleText;
    ImageView titleImage;
    ImageButton titleClearButton;
    View titleLine;

    Button cancelButton, saveButton;
    String extension, oTitle, hint;

    private EditDownloadDialogListener listener;

    public static EditDownloadDialog newInstance(String tag, Context context, DownItem item) {
        EditDownloadDialog d = new EditDownloadDialog();

        d.context = context;
        d.item = item;
        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setCancelable(false, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_edit_title;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        dlgTitleText = fv(R.id.text_dlg_title);

        titleEdit = fv(R.id.edit_title);
        titleEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = TextUtils.isEmpty(s.toString());
                titleText.setVisibility(empty ? View.VISIBLE : View.GONE);
                titleClearButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });

        titleEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                titleLine.setBackgroundColor(r.c(context, hasFocus ? R.color.url_keyword : R.color.bookmarks_empty_sub));
//                titleText.setText(r.s(R.string.dlg_edit_download_title_hint));
                titleEdit.setHint(hint);
                titleText.setText("");
                titleText.setTextColor(r.c(context, R.color.bookmarks_empty_sub));
                titleText.setVisibility(TextUtils.isEmpty(titleEdit.getText().toString()) ? View.VISIBLE : View.GONE);
                titleImage.setVisibility(View.GONE);
                titleClearButton.setVisibility(TextUtils.isEmpty(titleEdit.getText().toString()) ? View.GONE : View.VISIBLE);
            }
        });

        titleText = fv(R.id.text_warning_title);
        titleImage = fv(R.id.image_warning_title);
        titleLine = fv(R.id.line_title);
        titleClearButton = fv(R.id.button_clear_title);
        titleClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleEdit.setText("");
            }
        });


        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCancelButtonClick();

                Util.hideKeyBoard(titleEdit);
                dismiss();
            }
        });

        saveButton = fv(R.id.button_positive);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(titleEdit);

                if (TextUtils.isEmpty(titleEdit.getText().toString())) {
                    setWarning();
                    return;
                }

                String newName = titleEdit.getText().toString().trim() + "." + extension;
                if (newName.equalsIgnoreCase(oTitle)) {
                    if (listener != null)
                        listener.onCancelButtonClick();

                    Util.hideKeyBoard(titleEdit);
                    dismiss();
                }

                if (db.isDownloadFileName(newName)) {
                    setDuplicatedWarning();
                    return;
                }

                String path = item.getDownloadFilePath();
                File file = new File(new File(path).getParent(), newName);
                if (file.exists()) {
                    setDuplicatedWarning();
                    return;
                }

                File tempFile = new File(new File(path).getParent(), newName + FileUtil.TEMP_FILE_EXTENSION);
                if (tempFile.exists()) {
                    setDuplicatedWarning();
                    return;
                }

                item.setDownloadFileName(newName);
                if (listener != null)
                    listener.onSaveButtonClick(item);

                Util.hideKeyBoard(titleEdit);
                dismiss();
            }
        });


        if (item != null) {
            extension = FileUtil.getFileExtention(item.getDownloadFileName());
            oTitle = item.getDownloadFileName();
            titleEdit.setText(item.getDownloadFileName().substring(0, item.getDownloadFileName().lastIndexOf(".")));
            hint = titleEdit.getText().toString();
            titleEdit.setSelection(0, titleEdit.getText().length());
        }
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.8f;
            params.width = (int) (dm.widthPixels * rate);
            getDialog().getWindow().setAttributes(params);
        }

        if (titleEdit != null)
            titleEdit.post(new Runnable() {
                @Override
                public void run() {
                    Util.showKeyBoard(titleEdit);
                }
            });
    }

    public EditDownloadDialog setListener(EditDownloadDialogListener listener) {
        this.listener = listener;

        return this;
    }

    private void setWarning() {
        boolean tEmpty = TextUtils.isEmpty(titleEdit.getText().toString());

        if (tEmpty) {
            titleEdit.setHint("");
            titleText.setText(r.s(R.string.dlg_edit_download_title_required));
            titleText.setTextColor(r.c(context, R.color.bookmark_requred_text));
            titleLine.setBackgroundColor(r.c(context, R.color.bookmark_requred_text));
            titleImage.setVisibility(View.VISIBLE);
            titleEdit.clearFocus();
        }
    }

    private void setDuplicatedWarning() {
        hint = titleEdit.getText().toString();
        titleEdit.setText("");
        titleEdit.setHint("");
        titleText.setText(r.s(R.string.dlg_edit_download_title_duplicated));
        titleText.setTextColor(r.c(context, R.color.bookmark_requred_text));
        titleLine.setBackgroundColor(r.c(context, R.color.bookmark_requred_text));
        titleImage.setVisibility(View.VISIBLE);
        titleEdit.clearFocus();
    }

    public static interface EditDownloadDialogListener {
        public void onSaveButtonClick(DownItem item);

        public void onCancelButtonClick();
    }
}
