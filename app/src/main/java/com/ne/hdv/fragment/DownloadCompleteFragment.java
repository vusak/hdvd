package com.ne.hdv.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.ne.hdv.PlayerActivity;
import com.ne.hdv.R;
import com.ne.hdv.adapter.DownloadAdapter;
import com.ne.hdv.adapter.ReSelectableAdapter;
import com.ne.hdv.base.BaseDialogFragment;
import com.ne.hdv.base.BaseFragment;
import com.ne.hdv.base.ExpandLayout;
import com.ne.hdv.common.Common;
import com.ne.hdv.common.EditDownloadDialog;
import com.ne.hdv.common.FileUtil;
import com.ne.hdv.common.MessageDialog;
import com.ne.hdv.common.Util;
import com.ne.hdv.data.DownItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownloadCompleteFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = DownloadCompleteFragment.class.getSimpleName();

    ImageButton delButton, closeButton, allCheckButton, allUncheckButton;
    LinearLayout delLayout;
    TextView delCountText;

    FloatingActionButton selDelButton, allDelButton, listModeButton, gridModeButton;
    FloatingActionMenu floatingActionMenu;

    RecyclerView lv;
    LinearLayoutManager manager, gridManager;
    DownloadAdapter adapter, gridAdapter;
    ArrayList<DownItem> list;
    SwipeRefreshLayout refreshLayout;
    ExpandLayout adExpandLayout;
    ImageView adFMImage;
    TextView adFMText;
    ImageButton adCloseButton;
    Button adFMButton;

    boolean isGridMode = true;
    //    private DownloadCompleteListener listener;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_download_files;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adExpandLayout = fv(R.id.layout_expend_ad);
        adExpandLayout.collapse(false);

        adCloseButton = fv(R.id.button_ad_close);
        adCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setDelayShowAdFilemanager(System.currentTimeMillis());
                adExpandLayout.collapse(true);
            }
        });

        adFMButton = fv(R.id.button_ad_fm);
        adFMButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setDelayShowAdFilemanager(System.currentTimeMillis());
                adExpandLayout.collapse(true);
                launchApp(Common.PACKAGE_TM_FILEMANAGER);
            }
        });

        adFMImage = fv(R.id.image_ad_fm);
        adFMImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setDelayShowAdFilemanager(System.currentTimeMillis());
                adExpandLayout.collapse(true);
                launchApp(Common.PACKAGE_TM_FILEMANAGER);
            }
        });

        adFMText = fv(R.id.text_ad_fm);
        adFMText.setOnClickListener(v -> {
            sp.setDelayShowAdFilemanager(System.currentTimeMillis());
            adExpandLayout.collapse(true);
            launchApp(Common.PACKAGE_TM_FILEMANAGER);
        });

        floatingActionMenu = fv(R.id.button_floating_menu);

        listModeButton = fv(R.id.sub_floating_menu_list_view);
        listModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listModeButton.setVisibility(View.GONE);
                gridModeButton.setVisibility(View.VISIBLE);
                floatingActionMenu.close(false);
                setViewMode(false);
            }
        });
        listModeButton.setVisibility(View.GONE);

        gridModeButton = fv(R.id.sub_floating_menu_grid_view);
        gridModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridModeButton.setVisibility(View.GONE);
                listModeButton.setVisibility(View.VISIBLE);
                floatingActionMenu.close(false);
                setViewMode(true);
            }
        });
        gridModeButton.setVisibility(View.GONE);
        selDelButton = fv(R.id.sub_floating_menu_sel_delete);
        selDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeleteMode(true);
            }
        });

        allDelButton = fv(R.id.sub_floating_menu_delete_all);
        allDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibleMenu(false);
                if (fdf(Common.TAG_DIALOG_DELETE_ALL_ITEMS) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(Common.TAG_DIALOG_DELETE_ALL_ITEMS);
                    dlg.setTitle(r.s(R.string.dlg_delete_title));
                    dlg.setMessage(r.s(R.string.dlg_msg_delete_all_downloaded_list));
                    dlg.setNegativeLabel(r.s(R.string.str_cancel));
                    dlg.setPositiveLabel(r.s(R.string.str_delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            deleteItems(true);
                            refreshLIst();
                        }
                    });
                    sdf(dlg);
                }

            }
        });

        closeButton = fv(R.id.btn_top_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delButton.getVisibility() == View.VISIBLE)
                    setDeleteMode(false);
            }
        });

        delCountText = fv(R.id.text_top_del_count);

        delButton = fv(R.id.btn_top_delete);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItems();
            }
        });

        allCheckButton = fv(R.id.btn_top_all_check);
        allCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allCheckButton.setVisibility(View.GONE);
                allUncheckButton.setVisibility(View.VISIBLE);
                setDeleteCheckAll(true);
            }
        });

        allUncheckButton = fv(R.id.btn_top_all_uncheck);
        allUncheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allCheckButton.setVisibility(View.VISIBLE);
                allUncheckButton.setVisibility(View.GONE);
                setDeleteCheckAll(false);
            }
        });

        delLayout = fv(R.id.layout_delete);

        refreshLayout = fv(R.id.layout_refresh);
        refreshLayout.setColorSchemeResources(R.color.url_keyword);
        refreshLayout.setOnRefreshListener(this);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        gridManager = new GridLayoutManager(getActivity(), 2);
        lv.setLayoutManager(manager);
        lv.getItemAnimator().setRemoveDuration(100);
        adapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownItem item) {
            }

            @Override
            public void onDeleteButtonClick(int position, DownItem item) {

            }

            @Override
            public void onEditButtonClick(int position, DownItem item) {

            }

            @Override
            public void onClearButtonClick(int position, DownItem item) {

            }

            @Override
            public void OnItemClick(int position, DownItem item) {
                if (adapter.isCheckMode()) {
                    adapter.select(position);
                    checkChanged(adapter.getSelectedList().size(), adapter.isSelectedAll());
                } else
                    downloadedItemClicked(item);

            }

            @Override
            public void OnItemLongClick(int position, DownItem item) {
                if (!adapter.isCheckMode())
                    setDeleteMode(true);
            }
        });
        adapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);
        lv.setAdapter(adapter);

        gridAdapter = new DownloadAdapter(getActivity(), R.layout.item_downlaod_grid, list, new DownloadAdapter.DownloadAdapterListener() {
            @Override
            public void onDownloadButtonClick(int position, DownItem item) {
            }

            @Override
            public void onDeleteButtonClick(int position, DownItem item) {
                deleteItem(position);
            }

            @Override
            public void onEditButtonClick(int position, DownItem item) {
                editItem(position);
            }

            @Override
            public void onClearButtonClick(int position, DownItem item) {
                if (gridAdapter != null)
                    gridAdapter.setEditIndex(-1);
            }

            @Override
            public void OnItemClick(int position, DownItem item) {
                if (gridAdapter.isCheckMode()) {
                    gridAdapter.select(position);

                    checkChanged(gridAdapter.getSelectedList().size(), gridAdapter.isSelectedAll());
                } else if (gridAdapter.getEcitIndex() == position) {

                } else
                    downloadedItemClicked(item);
            }

            @Override
            public void OnItemLongClick(int position, DownItem item) {
                if (gridAdapter != null)
                    gridAdapter.setEditIndex(position);

            }
        });
        gridAdapter.setSelectMode(ReSelectableAdapter.CHOICE_MULTIPLE);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        isGridMode = !sp.isDownloadListMode();
        listModeButton.setVisibility(isGridMode ? View.VISIBLE : View.GONE);
        gridModeButton.setVisibility(isGridMode ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        refreshLIst();
        super.onResume();
    }

    @Override
    public void onRefresh() {
        refreshLIst();
        refreshLayout.setRefreshing(false);
    }

    public boolean setBackPress() {
        if (floatingActionMenu != null && floatingActionMenu.isOpened()) {
            visibleMenu(false);
            return true;
        }

        if (delLayout != null && delLayout.getVisibility() == View.VISIBLE) {
            setDeleteMode(false);
            return true;
        }
        return false;
    }

    public ArrayList<DownItem> getCompleteList() {
        return list;
    }

    private void deleteItems() {
        if (delCountText.getText().toString().isEmpty()) {
            if (fdf("no_select") == null) {
                MessageDialog dlg = MessageDialog.newInstance("no_select");
                dlg.setTitle(r.s(R.string.dlg_delete_title));
                dlg.setMessage(r.s(R.string.dlg_msg_no_selected_item));
                dlg.setPositiveLabel(r.s(R.string.ok));
                sdf(dlg);
            }

            return;
        }

        if (fdf("delete_selected_items") == null) {
            MessageDialog dlg = MessageDialog.newInstance("delete_selected_items");
            dlg.setTitle(r.s(R.string.dlg_delete_title));
            dlg.setMessage((r.s(R.string.dlg_msg_delete_item)));
            dlg.setNegativeLabel(r.s(R.string.str_cancel));
            dlg.setPositiveLabel(r.s(R.string.str_delete));
            dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                @Override
                public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                    deleteItems(false);
                }
            });
            sdf(dlg);
        }
    }

    public void deleteItems(boolean isAll) {
        if (adapter == null)
            return;

        ArrayList<DownItem> deleteList = new ArrayList<>();
        if (isGridMode)
            deleteList.addAll(gridAdapter.getSelectedList());
        else
            deleteList.addAll(adapter.getSelectedList());

        if (isAll)
            db.deleteAllDownloadedItems();
        else {
            for (DownItem item : deleteList)
                db.deleteDownloadItem(item);
            adapter.clearSelection();
            gridAdapter.clearSelection();
            checkChanged(0, false);
        }
        refreshLIst();
    }

    private void checkChanged(int count, boolean isSelectAll) {
        if (count <= 0)
            delCountText.setText("");
        else
            delCountText.setText(count + " " + r.s(R.string.del_selected_title));

        allCheckButton.setVisibility(isSelectAll ? View.GONE : View.VISIBLE);
        allUncheckButton.setVisibility(isSelectAll ? View.VISIBLE : View.GONE);
    }

    private void downloadedItemClicked(DownItem item) {
        String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
        Uri uri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            uri = FileProvider.getUriForFile(getActivity(), "com.ne.hdv.fileprovider", new File(path));
        else
            uri = Uri.fromFile(new File(path));

        Intent intent = new Intent(getActivity(), PlayerActivity.class);
        intent.putExtra(Common.INTENT_VIDEO_ID, item.getDownloadId());
        intent.putExtra(Common.INTENT_VIDEO_TITLE, item.getDownloadFileName());
        intent.putExtra(Common.INTENT_VIDEO_URL, uri.toString());
        startActivity(intent);

//        String path = item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, "");
//        MimeTypeMap myMime = MimeTypeMap.getSingleton();
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
//
//        Uri uri = null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            uri = FileProvider.getUriForFile(getActivity(), "com.ne.hdv.fileprovider", new File(path));
//        else
//            uri = Uri.fromFile(new File(path));
//        intent.setDataAndType(uri, "video/mp4");
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//
//        try {
//            startActivity(intent);
//        } catch (android.content.ActivityNotFoundException e) {
//            Toast.makeText(getActivity(), "Failed...", Toast.LENGTH_SHORT).show();
//        }
    }

    public void refreshLIst() {
        if (list == null || adapter == null || lv == null)
            return;

//        if (!getPackageList(Common.PACKAGE_JM_FILEMANAGER) && sp.isDelayedShowADFilemanager() && !sp.isPremiumMode())
        if (!getPackageList(Common.PACKAGE_TM_FILEMANAGER) && sp.isDelayedShowADFilemanager() && !sp.isPremiumMode())
            adExpandLayout.expand(true);

        list.clear();
        ArrayList<DownItem> temp = new ArrayList<>();
        temp.addAll(db.getAllCDownloadItems());

        for (DownItem item : temp) {
            if (item.getDownloadState() == DownItem.DOWNLOAD_STATE_DONE) {
                File file = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                if (!file.exists())
                    db.deleteDownloadItem(item);
                else {
                    if (item.getDownloadFileTotalSize() <= 0) {
                        item.setDownloadFileTotalSize(file.length());
                        db.insertOrUpdateDownloadItem(item);
                    }
                    list.add(item);
                }
            }
        }
        adapter.setTrend(sp.isJoinedTrends());
        adapter.notifyDataSetChanged();
        gridAdapter.setTrend(sp.isJoinedTrends());
        gridAdapter.notifyDataSetChanged();
        gridAdapter.setEditIndex(-1);

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);
        setViewMode(isGridMode);

        if (empty)
            setDeleteMode(false);
        floatingActionMenu.setVisibility(empty ? View.GONE : View.VISIBLE);
    }

    public void setViewMode(boolean _isGridMode) {
        if (gridAdapter == null || lv == null)
            return;

        gridAdapter.setEditIndex(-1);
        sp.setDownloadListMode(!_isGridMode);
        isGridMode = _isGridMode;
        lv.setLayoutManager(isGridMode ? gridManager : manager);
        lv.setAdapter(isGridMode ? gridAdapter : adapter);
        int padding = (int) Util.convertDpToPixel(10, app.getApplicationContext());
        if (isGridMode)
            lv.setPadding(padding, padding, padding, padding);
        else
            lv.setPadding(0, padding, 0, padding);
    }

    public void setDeleteCheckAll(boolean isCheck) {
        if (adapter != null) {
            if (isCheck)
                adapter.setAllSelection();
            else
                adapter.clearSelection();
        }

        if (gridAdapter != null) {
            if (isCheck)
                gridAdapter.setAllSelection();
            else
                gridAdapter.clearSelection();
        }

        if (gridAdapter != null)
            checkChanged(gridAdapter.getSelectedList().size(), gridAdapter.isSelectedAll());
    }

//    public boolean isListMode() {
//        return !this.isGridMode;
//    }

    public void selectModeClick(boolean mode) {
        if (adapter != null) {
            adapter.setCheckMode(mode);
            if (!mode)
                adapter.clearSelection();
        }

        if (gridAdapter != null) {
            gridAdapter.setCheckMode(mode);
            if (mode)
                gridAdapter.setEditIndex(-1);
            else
                gridAdapter.clearSelection();
        }
    }

//    public void setListener(DownloadCompleteListener listener) {
//        this.listener = listener;
//    }

    public void startPremiumMode() {
        if (adExpandLayout != null && adExpandLayout.isExpanded())
            adExpandLayout.collapse(false);
    }

    private void visibleMenu(boolean show) {
        if (show) ;
        else
            floatingActionMenu.close(true);
    }

    private void setDeleteMode(boolean isMode) {
        delLayout.setVisibility(isMode ? View.VISIBLE : View.GONE);
        delCountText.setText("");
        allCheckButton.setVisibility(View.VISIBLE);
        allUncheckButton.setVisibility(View.GONE);
        delButton.setVisibility(View.VISIBLE);

        if (isMode) {
            floatingActionMenu.close(true);
            selectModeClick(true);
        } else {
            selectModeClick(false);
        }
    }

    private void deleteItem(final int position) {
        final DownItem item = list.get(position);
        if (item != null) {
            if (fdf(MessageDialog.TAG) == null) {
                MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                dlg.setTitle(r.s(R.string.dlg_delete_title))
                        .setMessage(r.s(R.string.dlg_msg_delete_item))
                        .setNegativeLabel(r.s(R.string.str_cancel))
                        .setPositiveLabel(r.s(R.string.str_delete));
                dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                    @Override
                    public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.removeDownloadItem(item.getDownloadId());

                        if (gridAdapter != null)
                            gridAdapter.removeDownloadItem(item.getDownloadId());
                        db.deleteDownloadItem(item);
                        refreshLIst();
                    }
                });
                dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
                    @Override
                    public void onDialogNegative(BaseDialogFragment dialog, String tag) {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }
                    }
                });
                sdf(dlg);
            }
        }
    }

    private void editItem(final int position) {
        DownItem item = list.get(position);
        if (item != null) {
            if (fdf(EditDownloadDialog.TAG) == null) {
                EditDownloadDialog dlg = EditDownloadDialog.newInstance(EditDownloadDialog.TAG, getActivity(), item);
                dlg.setListener(new EditDownloadDialog.EditDownloadDialogListener() {
                    @Override
                    public void onSaveButtonClick(DownItem item) {
                        String newPath = new File(item.getDownloadFilePath()).getParent() + "/" + item.getDownloadFileName();

                        try {
                            File oldF = new File(item.getDownloadFilePath().replace(FileUtil.TEMP_FILE_EXTENSION, ""));
                            File newF = new File(newPath);
                            oldF.renameTo(newF);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        item.setDownloadFilePath(newPath);
                        db.insertOrUpdateDownloadItem(item);
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }
                    }

                    @Override
                    public void onCancelButtonClick() {
                        if (adapter != null)
                            adapter.notifyItemChanged(position);
                        if (gridAdapter != null) {
                            gridAdapter.notifyItemChanged(position);
                            gridAdapter.setEditIndex(-1);
                        }
                    }
                });

                sdf(dlg);
            }
        }
    }

    private void launchApp(String packageName) {
        try {
            if (getPackageList(packageName)) {
                Intent intent = getActivity().getPackageManager()
                        .getLaunchIntentForPackage(packageName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(Common.PLAYSTORE_URL + packageName);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        }
    }

    private boolean getPackageList(String packageName) {
        boolean isExist = false;

        if (getActivity() == null || getActivity().getPackageManager() == null)
            return false;

        PackageManager pkgMgr = getActivity().getPackageManager();
        List<ResolveInfo> mApps;
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = pkgMgr.queryIntentActivities(mainIntent, 0);

        try {
            for (int i = 0; i < mApps.size(); i++) {
                if (mApps.get(i).activityInfo.packageName.startsWith(packageName)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            isExist = false;
        }
        return isExist;
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    deleteItem(viewHolder.getAdapterPosition());
                } else {
                    editItem(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.primary1_a100));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mode_edit_white_24);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.str_edit), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.red1_a100));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white_24);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(14, getActivity()));
                        c.drawText(r.s(R.string.str_delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return !isGridMode && !adapter.isCheckMode();
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        };

        return simpleCallback;
    }

//    public static interface DownloadCompleteListener {
//        public void onCheckChanged(int count, boolean isSelectAll);
//
//        public void onDownloadedItemClicked(DownItem item);
//
//        public void notiEmptyCompleteList(boolean empty);
//
//        public void onDeleteMode(boolean on);
//    }
}
